% plik zera_bieguny.m

% IIR filter design; metoda "zer i biegun�w"
clear all; clf; subplot(1,1,1)
fs=1000; % czestotliwosc probkowania [Hz]

% zera i bieguny funkcji transmitancji H(z)
fz=[50]; % czestotliwosc zera
fp=[10]; % czestotliwosc bieguna
Rz=[1]; % modul dla zera
Rp=[0.96]; % modul dla bieguna
fmax=100; df=0.1; % parametry dla transformaty Fouriera

% zespolone wartosci zer i biegunow
fi_z=2*pi*fz/fs; % katy dla zer (pasmo zaporowe)
fi_p=2*pi*fp/fs; % katy dla biegunow (pasmo porzepustowe)
z=Rz.*exp(j*fi_z); % zero w gornej polplaszczyznie z
p=Rp.*exp(j*fi_p); % biegun w gornej polplaszczyznie z
z=[z conj(z)]; % zero + zero sprzezone
p=[p conj(p)]; % biegun + biegun sprzezony

% zera i bieguny na plaszczyznie z (wykres)
NP=1000;
fi=2*pi*(0:NP-1)/NP;
s=sin(fi); c=cos(fi);
% zplane(z',p');

subplot(3,2,1)
plot(s,c,'-k',real(z),imag(z),'or',real(p),imag(p),'xb');
title('Zeros and poles of Frequency Responce');

% obliczanie wspolczynnikow {a} i {b}
wzm=1;
[b,a]=zp2tf(z',p',wzm)
%disp('Pause 1');pause

% obliczanie funkcji transmitancji ({b,a}-->H(f))
f=0:df:fmax;
w=2*pi*f;
wn=2*pi*f/fs; % pulsacja znormalizowana (omega_n=omega/omega_s)

H=freqz(b,a,wn);
Habs=abs(H); % modul |H(f)| - skala liniowa
HdB=20*log10(Habs); % |H(z)| - skala logarytmiczna
Hfa=unwrap(angle(H)); % "odwijanie" fazy
subplot(3,2,3)
plot(f,Habs);grid on;xlabel('f[Hz]');
title('Charakterystyka amplitudowa |H(f)|')
% disp('Pause 2');pause
subplot(3,2,5)
plot(f,Hfa);grid on;xlabel('f[Hz]');ylabel('[rad]');
title('Charakterystyka fazowa H(f)');
% disp('Pause 3');pause