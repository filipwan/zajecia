% skrypt tworzy 2 sygna�y okresowe (sin i prostokatny)
% i oblicza ich sume
clear all; clf
% Dane wyjsciowe:
Tk=1;           % czas trwania sygna�u w [s]
fs=1000;        % czestotliwosc probkowania w [Hz]
f1=2;           % czestotliwosc 1. sygnalu
f2=10;          % czestotliwosc 2. sygnalu
A1=1;A2=1;      % amplitudy sygnalow
%==================================
dt=1/fs;        % okres probkowania
t=0:dt:Tk-dt;   % wektor czasu dyskretnego

s1=A1*sin(2*pi*f1*t);   % sygnal 1.

p=ones(1,((fs/f2)/2));  % polowa okresu sygnalu 2.
okres=[p,-p] * A2;     	% okres sygnalu 2.
s2 = okres;
for i=1:((f2*Tk)-1)
    s2=[s2,okres];      % prawidlowa ilosc okresow w zadanym czasie
end

sygnal=s1+s2; % suma sygnalow

subplot(3,1,1)
plot(t,s1); grid on
xlabel('Czas [s]')
ylabel('[V]')
title('Sygnal 1.')

subplot(3,1,2)
plot(t,s2); grid on
xlabel('Czas [s]')
ylabel('[V]')
title('Sygnal 2.')

subplot(3,1,3)
plot(t,sygnal); grid on
xlabel('Czas [s]')
ylabel('[V]')
title('Sumas sygnalow 1 i 2')