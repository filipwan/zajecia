;Lab5_Zad2_PW_GS_TC3
$NOMOD51			; wylaczenie predefinicji rejestrow
$INCLUDE(reg517.inc)	; wlaczenie predefinicji rej. procesora 80537

CSEG AT 0000h

TH0_SET EQU 256-180	; obliczenie wartosci poczatkowych
TL0_SET EQU 0
ON1 EQU 130		; czas trwania zapalenia diody P1.0
OFF1 EQU 15			; czas trwania zgaszenia diody P1.0
ON2 EQU 27			; czas trwania zapalenia diody P1.1
OFF2 EQU 43			; czas trwania zgaszenia diody P1.1
ON3 EQU 65			; czas trwania zapalenia diody P1.2
OFF3 EQU 91			; czas trwania zgaszenia diody P1.2

LJMP START

ORG 0BH			; adres procedury obslugi T0
MOV TH0, #TH0_SET	; wpisanie warto�ci poczatkowek TH
DEC R0            
MOV A, R0         
LCALL LED1		; wezwanie procedury obslugujacej diode P1.0    
DEC R1  
MOV A, R1
LCALL LED2		; wezwanie procedury obslugujacej diode P1.1    
DEC R2
MOV A, R2
LCALL LED3		; wezwanie procedury obslugujacej diode P1.2
RETI				; powrot z przerwania

LED1:				; procedury obslugujaca diode P1.0
JNZ K       
CPL P1.0			; zanegowanie stanu diody P1.0   
JB P1.0, LED_ON1  
MOV R0, #ON1       	 	; ustawienie czasu zapalenia diody P1.0
LJMP K
LED_ON1:
MOV R0, #OFF1		; ustawienie czasu zgaszenia diody P1.0
K:
RET

LED2:				; procedury obslugujaca diode P1.1
JNZ K2
CPL P1.1			; zanegowanie stanu diody P1.1
JB P1.1, LED_ON2
MOV R1, #ON2		; ustawienie czasu zapalenia diody P1.1
LJMP K2
LED_ON2:
MOV R1, #OFF2		; ustawienie czasu zgaszenia diody P1.1
K2:
RET

LED3:				; procedury obslugujaca diode P1.2
JNZ K3
CPL P1.2			; zanegowanie stanu diody P1.2
JB P1.2, LED_ON3
MOV R2, #ON3		; ustawienie czasu zapalenia diody P1.2
LJMP K3
LED_ON3:
MOV R2, #OFF3		; ustawienie czasu zgaszenia diody P1.2
K3:
RET

START:
CPL P1.0			; zanegowanie stanu diody P1.0
CPL P1.1			; zanegowanie stanu diody P1.1
CPL P1.2			; zanegowanie stanu diody P1.2
MOV R0, #ON1		; przesuniecie do rejestrow okresow 
MOV R1, #ON2		; zapalenia kolejnych diod LED
MOV R2, #ON3
MOV TMOD, #00000001b	; ustawienie rejestru TMOD
MOV TH0, #TH0_SET	; wpisanie warto�ci poczatkowych
MOV TL0, #TL0_SET
SETB TR0			; start T0
SETB EAL			; odblokowanie systemu przerwa�
SETB ET0			; zezwolenie na przerwanie od T0
SJMP $

END
