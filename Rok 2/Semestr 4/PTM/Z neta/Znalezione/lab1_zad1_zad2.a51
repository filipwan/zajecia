;Lab1_Zad1_PW_GS_TC3
$NOMOD51                    	 ;Wyl. predef. rej.
$INCLUDE(reg517.inc)

CSEG AT 0000h 			;Adres poczatku programu

	MOV DPTR,#TAB1
	MOV R0,#21h
	MOV R1, #06h			
;kopiowanie do XDATA
ETYK:			
	MOVC A,@A+DPTR
	MOV @R0,A	
	MOV A,#0		;Zerowanie akumulatora
	INC DPTR
	INC R0
	DJNZ R1,ETYK


;kopiowanie do IDATA
	;MOV R0,#20h
	;MOV R1,#40h
	;MOV R2,#06h
;ETYK2:
	;MOV A,@R0
	;MOV @R1,A
	;MOV A,#0
	;INC R0
	;INC R1
	;DJNZ R2,ETYK2



;	TAB2 wczytanie
	MOV DPTR,#TAB2
	MOV R1,#06h
	MOV R0,#2Ch
;Kopiowanie do XDATA
ETYK3:
	MOVC A,@A+DPTR
	MOV @R0,A
	MOV A,#0		;Zerowanie akumulatora
	INC DPTR
	INC R0
	DJNZ R1,ETYK3


;Dodawanie
;DODAJ:
	MOV R0,#21h		; TAB1
	MOV R1,#2Ch		; TAB2 oraz tablica wynikowa
	MOV R2,#06h		; Licznik
DODAJ:
	MOV A,@R0
	ADD A,@R1
	MOV @R1,A

;ZAPIS:				;Zapisuje wynik w pamieci

	INC R0
	INC R1
	MOV A,#0
	DJNZ R2,DODAJ

JMP $

TAB1: DB 1,3,2,9,0,2
TAB2: DB 0,0,0,-1,3,1

END

