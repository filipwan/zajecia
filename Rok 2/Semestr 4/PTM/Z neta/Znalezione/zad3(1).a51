;Lab4_Zad3_PW_GS_TC3
$nomod51
$include(reg517.inc)
$include (LCD.inc)

CSEG AT 0000h

ORG 0
LJMP PROG	; skok do procedury PROG	
ORG 0Bh
LJMP PRZER	; skok do procedury PRZER
ORG 30h

PRZER:
INC B
RETI

PROG:
CALL initLCD	; wezwanie procedury inicjujacej LCD
CALL LCDclear	; wezwanie procedury czyszczacej LCD
ORL TMOD,#09 	; ustawienie trybu pracy timera T0

POM:
MOV R0,#20h

CLR A		; czyszczenie zawartosci akumulatora
MOV TL0,A
MOV TH0,A
MOV B,A
JB P3.3,$	; obsluga przycisku uruchamiajacego stoper
SETB TR0	; start pracy timera
JB P3.2,$	; obsluga przycisku wylaczajacego stoper
CLR TR0		; koniec pracy timera
MOV @R0,TL0
INC R0
MOV @R0,TH0
INC R0
MOV @R0,B	; przeksztalcenia wyniku w celu
MOV A,B		; poprawnego wyswietlenia go
MOV B,#15	; na LCD
DIV AB

CALL putInt	; wezwanie procedury wyprowadzajacej czas na LCD
LJMP POM	; skok do procedury POM

END