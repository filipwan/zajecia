;Lab2_Zad2_PW_GS_TC3
$NOMOD51
$INCLUDE (REG517.INC)

CSEG AT 0

PETLA:
	MOV R3,#4		; licznik wierszy
	MOV R1,#07Fh   		; wiersz poczatkowy
	JZ PETLA1
PETLA1:
	MOV P5,R1
	MOV A,P7		; wprowadzenie stanu portu P7 do akumulatora
	XRL A,#0FFh
	JZ PETLA2
	MOV A,P7		; wprowadzenie stanu portu P7 do akumulatora
	ANL A,R1		; wyzerowanie 4 starszych bit�w akumulatora
	MOV P1,A		; wyprowadzenia zawartosci akumulatora na diody
PETLA2:
	MOV A,R1
	RR A
	MOV R1,A
	DJNZ R3,PETLA1	
	SJMP PETLA		; powrot na poczatek programu
	
END






