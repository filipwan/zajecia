;Lab3_Zad3_PW_GS_TC3
$NOMOD51                    		; wylaczenie predefinicji rejestrow
$INCLUDE(reg517.inc)
$INCLUDE(lab3_zad2.inc)

CSEG AT 0000h 				; adres poczatku programu

LCD_Stat equ 0ff2eh  			; rejestr Statu ukladu LCD
LCD_Ctrl equ 0ff2ch 			; rejestr sterujacy
LCD_DataRD equ 0ff2fh  			; rejestr zapisu danych
LCD_DataWR equ 0ff2dh 			; rejestr odczytu danych
LCD_On equ 0eh  			; kod wlaczajacy wyswietlacz
ASCII_A equ 41h     			; kod ascii litery A

START:
	MOV R5,#ASCII_A  		; kod znaku do rejestru pomocniczego
	MOV DPTR,#LCD_Ctrl 		; adres rejestru sterujacego do DPTR
	CALL LCD_Reset  		; kod funkcji resetujacej uklad LCD
	CALL TEST  			; kontrola dostepnosci
	CALL LCD_Clr			; kod funkcji czyszczacej do akumulatora 
	CALL LCD_CursorRight		
	CALL LCD_CursorRight		
	MOV R4,#16  			; ustawienie licznika petli
	CALL PRINT
PRINT:
	CALL TEST
	MOV DPTR,#LCD_DataWR 		; adres rejestru danych do zapisu
	MOV A,R5     			; zapisanie kodu znaku do akumulatora
	MOVX @DPTR,A  			; wyslanie danej na wyswietlacz
	INC R5   			; inkrementacja kodu ascii znaku
LOOP:
	NOP
	DJNZ R4,PRINT 

JMP $
END
