;Lab3_Zad2_PW_GS_TC3
LCDCODE SEGMENT CODE
RSEG LCDCODE			; relokowalne

LCD_Reset:                 
	MOV DPTR,#0FF2CH
	MOV A,#38h
	LCALL TEST
	MOVX @DPTR,A
	RET

LCD_Clr:                  
	MOV A,#01h
	MOV DPTR,#0FF2CH
	LCALL TEST
	MOVX @DPTR,A
	RET

LCD_CursorRight:                
	MOV A,#06h
	MOV DPTR,#0FF2CH
	LCALL TEST
	MOVX @DPTR,A
	RET

LCD_CursorLeft:                 
	MOV A,#00h
	MOV DPTR,#0FF2CH
	LCALL TEST
	MOVX @DPTR,A
	RET

TEST:                 
	PUSH ACC
	PUSH DPL
	PUSH DPH
	MOV DPTR,#0FF2EH

LOOP_NOT:
	MOVX A,@DPTR
	JB ACC.7,LOOP_NOT
	POP DPH
	POP DPL
	POP ACC
	RET

;PRINT:
;	CALL TEST
;	MOV R4,@R0
;	MOV DPTR,#LCD_DataWR 		; adres rejestru danych do zapisu
;	MOV A,R5     			; zapisanie kodu znaku do akumulatora
;	MOVX @DPTR,A  			; wyslanie danej na wyswietlacz
;	
;	JMP LOOP
;	RET
;
;LOOP:
;	NOP
;	DJNZ R4,PRINT
