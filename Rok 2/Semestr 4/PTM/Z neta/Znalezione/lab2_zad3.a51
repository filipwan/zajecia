;Lab2_Zad3_PW_GS_TC3
$NOMOD51
$INCLUDE(REG517.INC)

CSEG AT 0

	MOV R2,#0	; numer sprawdzanego klawisza
	MOV R3,#4	; licznik petli 
	MOV P5,#0EFH	; wysterowanie portu P5 
	MOV A,P7	; przeslanie zawartosci portu P7 do akumulatora   
PETLA1:
	RRC A		; przesuniecie bitowe w prawo 
	JNC WYJSCIE	; skok na koniec, gdy C=0
	INC R2		 
	DJNZ R3,PETLA1	; R3=R3-1, jesli R3!=0 to skok do PETLA1

	MOV R3,#4
	MOV P5,#0DFH
	MOV A,P7
	SETB C		; ustawienie C=1
	CPL C		; zmiana stanu C (wyzerowanie)

; PETLA2, PETLA3, PETLA4 sa wykonywane analogicznie jak PETLA1, stad brak komentarzy w celu unikniecia powtorzen

PETLA2:
	RRC A		
	JNC WYJSCIE
	INC R2
	DJNZ R3,PETLA2

	MOV R3,#4
	MOV P5,#0BFH
	MOV A,P7
	SETB C
	CPL C
PETLA3:
	RRC A
	JNC WYJSCIE
	INC R2
	DJNZ R3,PETLA3

	MOV R3,#4
	MOV P5,#07FH
	MOV A,P7
	SETB C
	CPL C
PETLA4:
	RRC A
	JNC WYJSCIE
	INC R2
	DJNZ R3, PETLA4

WYJSCIE:
	MOV A,R2 	; przepisanie numeru klawisza do akumulatora
	CPL A 		
	MOV P1,A	; wyswietlenie na diodach LED numeru w kodzie NKB

JMP $

END 

