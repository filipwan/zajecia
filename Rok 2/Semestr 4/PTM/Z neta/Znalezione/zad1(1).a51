$nomod51
$include(reg517.inc)

CSEG AT 0000h

JMP START

ORG 0bh			; adres przerwania od Timer0
MOV TH0, #03Ch		; ustawienie warto�ci poczatkowych
MOV TL0, #0AFh 
DJNZ ACC, LOOP
CPL P1.0
MOV A, #10

LOOP:
RETI			;powr�t z przerwania

ORG 100h

START:
MOV TMOD,#00000001b
MOV TH0, #03Ch		;wpisanie warto�ci poczatkowych
MOV TL0, #0AFh
MOV A,#10		;czas=10*50ms=0,5[s]

SETB TR0		;T0 => ON
SETB EAL		;odblokowanie systemu przerwa�
SETB ET0		;zezwolenie na przerwanie od T0

SJMP $

END
