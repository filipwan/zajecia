;Lab3_Zad1_PW_GS_TC3
$NOMOD51                    		; wylaczenie predefinicji rejestrow
$INCLUDE(reg517.inc)

CSEG AT 0000h 				; adres poczatku programu

LCD_Stat equ 0ff2eh  			; rejestr Statu ukladu LCD
LCD_Ctrl equ 0ff2ch 			; rejestr sterujacy
LCD_DataRD equ 0ff2fh  			; rejestr zapisu danych
LCD_DataWR equ 0ff2dh 			; rejestr odczytu danych

LCD_On equ 0eh  			; kod wlaczajacy wyswietlacz
LCD_Reset equ 38h 			; kod resetujacy wyswietlacz
LCD_CursorRight equ 06h    		; kod przesuwajacy kursor w prawo
LCD_CursorLeft equ 00h    		; kod przesuwajacy kursor w lewo
ASCII_A equ 41h     			; kod ascii litery A
LCD_Clr equ 01h    			; czyszczenie i kursor na poczatek

START:
	MOV R5,#ASCII_A  		; kod znaku do rejestru pomocniczego
	MOV DPTR,#LCD_Ctrl 		; adres rejestru sterujacego do DPTR
	MOV A,#LCD_Reset  		; kod funkcji resetujacej uklad LCD
	MOVX @DPTR,A 			; wyslanie kodu funkcji do rejestru sterujacego
	CALL TEST  			; kontrola dostepnosci
	MOV A,#LCD_Clr			; kod funkcji czyszczacej do akumulatora 
	MOVX @DPTR,A   

PRINT:
	CALL TEST
	MOV DPTR,#LCD_DataWR 		; adres rejestru danych do zapisu
	MOV A,R5     			; zapisanie kodu znaku do akumulatora
	MOVX @DPTR,A  			; wyslanie danej na wyswietlacz

TEST:
	PUSH DPH
	PUSH DPL
	PUSH ACC
	MOV DPTR,#LCD_Stat

LOOP_NOT:
	MOVX A, @DPTR
	JB ACC.7,LOOP_NOT
	POP ACC
	POP DPL
	POP DPH
	RET

END
