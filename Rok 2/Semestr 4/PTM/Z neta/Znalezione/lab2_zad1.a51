;Lab2_Zad1_PW_GS_TC3
$NOMOD51                    	 ; wylaczenie predefinicji rejestrow
$INCLUDE(reg517.inc)

CSEG AT 0000h 			; Adres poczatku programu

	MOV P5,#07FH ; ustawienie P5.4=0, detekcja pierwszego wiersza
PETLA:
	MOV A,P3   ; zapamietanie stanu portu P3 w akumulatorze
 	MOV P1,A    ; wyswietlenie kolumny na diodach portu P1 
	SJMP PETLA  
JMP $

END
