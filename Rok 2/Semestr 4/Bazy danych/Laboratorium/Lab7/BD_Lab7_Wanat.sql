-- zadanie 1a
create or replace view zad1a as
select e.first_name, e.last_name, e.salary 
from employees e left join jobs j on e.job_id = j.job_id 
where e.department_id = 60 and j.job_id like 'IT_PROG';  -- w dziale 50 nie ma programist�w, dlatego zostal u�yty dzialu 60
select * from zad1a;

-- zadanie 1b
-- nie ma mo�liwo�ci u�ywania polece� DML ze wzgl�du na polecenie group by
create or replace view zad1b as
select d.department_name, max(e.salary) as max_salary
from departments d join employees e on d.department_id = e.department_id
group by d.department_name
order by max_salary desc;
select * from zad1b;

-- zadanie 1c
create or replace view zad1c as
select r.region_name, count(e.employee_id) count_employees
from regions r  
left join countries c on r.region_id = c.region_id 
left join locations l on c.country_id = l.country_id 
left join departments d on l.location_id = d.location_id 
left join employees e on d.department_id = e.department_id 
group by r.region_name;
select * from zad1c;

-- zadanie 1d
create or replace view zad1d as
select first_name, last_name, salary, hire_date
from employees
where hire_date > (
    select hire_date from employees 
    where salary = (
        select min(salary) from employees
    )
);
select * from zad1d;

-- zadanie 1e
-- wy�wietlam list� wszystkich stanowisk, a nie liczb�
create or replace view zad1e as
select j.job_title, min(e.salary) min_salary
from departments d
left join employees e on d.department_id = e.department_id
left join jobs j on e.job_id = j.job_id
where d.department_id = 100
group by j.job_title
order by min_salary desc;
select * from zad1e;

-- zadanie 2
create sequence zad2 
minvalue 1 
start with 1 
increment by 1 
nocycle;

create table Studenci
( student_id number(10) not null,
  indeks number(10) not null,
  imie varchar2(20 byte) not null,
  nazwisko varchar2(20 byte) not null,
  data_urodzenia date,
  srednia_ocen number(3,2),
  plec varchar2(20 byte) not null,
  rok_studiow date,
  kierunek varchar2(20 byte),
  constraint Studenci_pk primary key (student_id)
);

insert into studenci (student_id, indeks, imie, nazwisko, plec)
values (zad2.nextval, 246523, 'Adam', 'Niewiemtego', 'M�czyzna');
insert into studenci (student_id, indeks, imie, nazwisko, plec)
values (zad2.nextval, 243223, 'Janusz', 'Januszowy', 'M�czyzna');
insert into studenci (student_id, indeks, imie, nazwisko, plec)
values (zad2.nextval, 236523, 'Roberta', 'Benzal', 'Kobieta');
insert into studenci (student_id, indeks, imie, nazwisko, plec)
values (zad2.nextval, 246523, 'Martyna', 'Soners', 'Kobieta');

select * from studenci;

-- zadanie 3
create index zad3 on studenci (imie);













