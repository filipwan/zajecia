-- zadanie 1
select e.first_name, e.last_name, e.salary from employees e
where e.salary > (
select avg(e.salary) from employees e
where (e.department_id = 20 or e.department_id = 80 or e.department_id = 90)  and e.last_name LIKE '%e%');

-- zadanie 2
select e.first_name, e.last_name, e.employee_id, e.salary, d.department_name, l.city, emp.averSalar
from (employees e left join departments d on e.department_id = d.department_id left join locations l on d.location_id = l.location_id)
right join (
select avg(e.salary) averSalar, e.department_id from employees e left join departments d on e.department_id = d.department_id
where d.department_name <> 'Sales'
group by e.department_id
) emp on e.department_id = emp.department_id
where e.salary > emp.averSalar;

-- zadanie 3
select e.first_name, e.last_name, e.hire_date, e.salary from employees e
where e.hire_date > (
select e.hire_date from employees e
where e.salary = (select max(e.salary) from employees e)
);

-- zadanie 4
select d.department_name from departments d right join (
select avg(e.salary) averSalar, e.department_id from employees e left join departments d on e.department_id = d.department_id
group by e.department_id
) dep on d.department_id = dep.department_id
where dep.averSalar = (select max(averSalar) from 
(
select avg(e.salary) averSalar, e.department_id from employees e left join departments d on e.department_id = d.department_id
group by e.department_id
)
);

-- zadanie 5
select d.department_name from departments d right join (
select count(e.employee_id) countEmplo, e.department_id from employees e left join departments d on e.department_id = d.department_id
group by e.department_id
) dep on d.department_id = dep.department_id
where dep.countEmplo = (select max(countEmplo) from 
(
select count(e.employee_id) countEmplo, e.department_id from employees e left join departments d on e.department_id = d.department_id
group by e.department_id
));

-- zadanie 6
select d.department_name from departments d right join (
select count(e.employee_id) countEmplo, e.department_id from employees e left join departments d on e.department_id = d.department_id
where extract(year from e.hire_date) = 2005 
group by e.department_id
) dep on d.department_id = dep.department_id
where dep.countEmplo = (select max(countEmplo) from 
(
select count(e.employee_id) countEmplo, e.department_id from employees e left join departments d on e.department_id = d.department_id
where extract(year from e.hire_date) = 2005 
group by e.department_id
));

-- zadanie 7
select men.first_name, men.last_name, men.employee_id, men.salary, count(e.employee_id) from employees e
right join
(select em.employee_id, em.first_name, em.last_name, em.salary from employees em 
inner join 
(select manager_id from employees union select manager_id from departments
) m on em.employee_id = m.manager_id) men on e.manager_id = men.employee_id
group by men.employee_id, men.first_name, men.last_name, men.salary;

-- zadanie 8
select e.employee_id, e.first_name, e.last_name, e.salary "Employee Salary", m.salary "Manager Salary" from employees e
inner join employees m on e.manager_id = m.employee_id
where e.salary > m.salary