-- zadanie 1
insert into employees (employee_id, last_name, job_id, department_id, manager_id, email, hire_date)
values (207, '&last_name', 'SA_REP', 80,
    (select employees.manager_id from employees where last_name = 'Austin'), 'info@gmail.com', to_date(sysdate));

-- zadanie 2
update employees
set salary = ((select avg(salary) from employees where department_id = 80)+2000)
where employee_id = 207;

-- zadanie 3
update employees
set manager_id = 
(select e.manager_id from employees e
where e.salary = (select max(salary) from employees where manager_id is not null) and e.manager_id is not null
group by e.manager_id)
where employee_id = 207;

-- zadanie 4
delete from employees where employee_id = 207;

-- zadanie 5
update employees
set department_id = (select department_id from employees where salary = (select min(salary) from employees))
where last_name like 'C%';

-- zadanie 6
rollback