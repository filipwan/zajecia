select first_name, last_name, hire_date from employees
    where hire_date > TO_DATE('2007-01-01', 'YYYY-MM-DD')
    order by hire_date asc