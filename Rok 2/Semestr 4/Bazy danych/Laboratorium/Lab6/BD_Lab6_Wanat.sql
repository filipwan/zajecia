-- zadanie 1
create table Studenci
( student_id number(10) not null,
  indeks number(10) not null,
  imie varchar2(20 byte) not null,
  nazwisko varchar2(20 byte) not null,
  data_urodzenia date,
  srednia_ocen number(3,2),
  plec varchar2(20 byte) not null,
  rok_studiow date,
  kierunek varchar2(20 byte),
  constraint Studenci_pk primary key (student_id)
);

-- zadanie 2
insert into studenci (student_id, indeks, imie, nazwisko, plec)
values (1, 246523, 'Adam', 'Niewiemtego', 'M�czyzna');
insert into studenci (student_id, indeks, imie, nazwisko, plec)
values (2, 243223, 'Janusz', 'Januszowy', 'M�czyzna');
insert into studenci (student_id, indeks, imie, nazwisko, plec)
values (3, 236523, 'Roberta', 'Benzal', 'Kobieta');
insert into studenci (student_id, indeks, imie, nazwisko, plec)
values (4, 246523, 'Martyna', 'Soners', 'Kobieta');

-- zadanie 3
update studenci
set srednia_ocen = 4.0;

-- zadanie 4
create table Gry
( gra_id number(10) not null,
  tytul varchar2(30 byte) not null,
  data_wydania date not null,
  gatunek varchar2(20 byte),
  ocena number(3,1),
  studio varchar2(20 byte) not null,
  liczba_graczy varchar2(20 byte) not null,
  platforma varchar2(20 byte),
  constraint Gry_pk primary key (gra_id),
  constraint check_liczba_graczy
  check ( liczba_graczy in ('single-player','multi-player','local multi-player', 'co-op')),
  constraint check_platforma
  check( platforma in ('pc','xbox','playstation'))
);
insert into gry (gra_id, tytul, data_wydania, studio, liczba_graczy, platforma)
values (1, 'Trover Saves the Universe', to_date('04-06-2019', 'DD-MM-YYYY'), 'Squanch Games', 'single-player', 'pc');
insert into gry (gra_id, tytul, data_wydania, studio, liczba_graczy, platforma)
values (2, 'Minecraft', to_date('17-05-2009', 'DD-MM-YYYY'), 'Mojang', 'single-player', 'pc');
insert into gry (gra_id, tytul, data_wydania, studio, liczba_graczy, platforma)
values (3, 'Gears of War', to_date('07-11-2009', 'DD-MM-YYYY'), 'Epic Games', 'single-player', 'xbox');
insert into gry (gra_id, tytul, data_wydania, studio, liczba_graczy, platforma)
values (4, 'Grand Theft Auto V', to_date('17-09-2013', 'DD-MM-YYYY'), 'Rockstar Games', 'single-player', 'xbox');
insert into gry (gra_id, tytul, data_wydania, studio, liczba_graczy, platforma)
values (5, 'Spider-Man', to_date('07-09-2018', 'DD-MM-YYYY'), 'Insomniac Games', 'single-player', 'playstation');

-- zadanie 5
update gry
set ocena = 5.0
where platforma like 'xbox';

-- zadanie 6
create table Ulubione_gry
( ulubione_gry_id number(10) not null,
  student_id number(10) not null references studenci (student_id),
  gra_id number(10) not null references gry (gra_id),
  constraint Ulubione_gry_pk primary key (ulubione_gry_id)
);
insert into ulubione_gry(ulubione_gry_id, student_id, gra_id)
values (1, 1, 2);

-- zadanie 7
drop table studenci CASCADE CONSTRAINTS;
drop table gry CASCADE CONSTRAINTS;
drop table ulubione_gry CASCADE CONSTRAINTS;