 -- zadanie 1
select * from employees where department_id = 50;

 -- zadanie 2
select department_id, count(employees.employee_id) AS "Number of employees" 
    from employees 
    group by department_id;

 -- zadanie 3
select department_id, job_id, round(avg(salary)) AS "Average salary" 
    from employees 
    group by department_id, job_id;

 -- zadanie 4
select first_name, last_name 
    from employees 
    where last_name not like 'S%'
    order by last_name  desc;

 -- zadanie 5
select job_id, count(employee_id) as "Liczba pracowników"
    from employees 
    where job_id='&job_id'-- or job_id='IT_PROG'
    group by job_id;

 -- zadanie 6
select (MAX(salary) - MIN(salary)) as "Różnica max-min salary"
    from employees;

 -- zadanie 7
select count(distinct manager_id) as "Liczba managerów"
    from employees;

 -- zadanie 8
select count(*) as "Liczba pracowników", 
    count(case when (hire_date between TO_DATE('2002-12-31', 'YYYY-MM-DD') and TO_DATE('2006-01-01', 'YYYY-MM-DD')) then 1 else null end) as "Liczba zatrudnionych 03-06"
    from employees;

 -- zadanie 9
select job_id, 
    SUM(case when department_id = 20 then salary else 0 end) as "Total Salary dep 20",
    SUM(case when department_id = 80 then salary else 0 end) as "Total Salary dep 80",
    SUM(case when department_id = 90 then salary else 0 end) as "Total Salary dep 90",
    sum(salary) as "Total_salary"
    from employees
    group by job_id;