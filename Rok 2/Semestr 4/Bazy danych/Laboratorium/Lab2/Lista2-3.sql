select department_id, job_id, round(avg(salary)) AS "Average salary" 
    from employees 
    group by department_id, job_id;