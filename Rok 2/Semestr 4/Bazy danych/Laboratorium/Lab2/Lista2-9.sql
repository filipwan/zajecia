select job_id, 
    SUM(case when department_id = 20 then salary else 0 end) as "Total Salary dep 20",
    SUM(case when department_id = 80 then salary else 0 end) as "Total Salary dep 80",
    SUM(case when department_id = 90 then salary else 0 end) as "Total Salary dep 90",
    sum(salary) as "Total_salary"
    from employees
    group by job_id