select e.first_name, e.last_name, e.hire_date from employees e
    cross join employees em
    where em.last_name = 'King' and e.hire_date > em.hire_date
    group by e.first_name, e.last_name, e.hire_date