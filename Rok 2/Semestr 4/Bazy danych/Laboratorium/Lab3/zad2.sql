select e.first_name, e.last_name, d.department_name, l.city from employees e
    left join departments d on e.department_id=d.department_id
    left join locations l on d.location_id=l.location_id
    where e.salary > 5000
    order by e.employee_id