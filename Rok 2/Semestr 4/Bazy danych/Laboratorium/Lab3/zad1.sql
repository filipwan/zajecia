select d.department_name, l.street_address, l.postal_code, l.city, l.state_province, l.country_id
    from departments d 
    JOIN locations l on d.location_id=l.location_id 
    order by d.department_id