select c.country_name, count(e.employee_id) as "employee ammount" from employees e
    full outer join departments d on e.department_id=d.department_id
    full outer join locations l on d.location_id=l.location_id
    right join countries c on l.country_id=c.country_id
    group by c.country_name