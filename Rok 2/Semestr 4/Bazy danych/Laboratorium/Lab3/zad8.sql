select e.first_name, e.last_name, d.department_name, l.city from employees e
    left join departments d on e.department_id=d.department_id
    left join locations l on d.location_id=l.location_id
    where e.hire_date < to_date('01-01-2005', 'DD-MM-YYYY')