select e.first_name, e.last_name from employees e
    inner join jobs j on e.job_id=j.job_id
    inner join employees em on j.job_id=em.job_id
    group by e.first_name, e.last_name, e.salary, j.job_title
    having e.salary > avg(em.salary)