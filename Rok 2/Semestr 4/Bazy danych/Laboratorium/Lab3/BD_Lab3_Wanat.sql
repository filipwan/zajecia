-- zadanie 1
select d.department_name, l.street_address, l.postal_code, l.city, l.state_province, l.country_id
    from departments d 
    JOIN locations l on d.location_id=l.location_id 
    order by d.department_id;

-- zadanie 2
select e.first_name, e.last_name, d.department_name, l.city from employees e
    left join departments d on e.department_id=d.department_id
    left join locations l on d.location_id=l.location_id
    where e.salary > 5000
    order by e.employee_id;

-- zadanie 3
select e.first_name, e.last_name from employees e
    inner join jobs j on e.job_id=j.job_id
    inner join employees em on j.job_id=em.job_id
    group by e.first_name, e.last_name, e.salary, j.job_title
    having e.salary > avg(em.salary);

-- zadanie 4
select e.first_name, e.last_name, d.manager_id, l.city from employees e
    join departments d on e.department_id=d.department_id
    join locations l on d.location_id=l.location_id
    where d.manager_id = '&manager_id' -- np 145
    and l.city = '&city'; -- np Oxford

-- zadanie 5
select e.first_name, e.last_name, e.employee_id, em.first_name, em.last_name, em.employee_id from employees e
    left join employees em on e.manager_id=em.employee_id
    where e.employee_id = '&employee_id'
    order by e.last_name;

-- zadanie 6
select c.country_name, count(e.employee_id) EMPLOYEE_COUNT from employees e
    full outer join departments d on e.department_id=d.department_id
    full outer join locations l on d.location_id=l.location_id
    right join countries c on l.country_id=c.country_id
    group by c.country_name
    order by EMPLOYEE_COUNT desc;

-- zadanie 7
select e.first_name, e.last_name, d.department_name from employees e
    inner join departments d on e.department_id=d.department_id
    inner join employees em on d.department_id=em.department_id
    where em.employee_id = '&employee_id'; -- np 145

-- zadanie 8
select e.first_name, e.last_name, d.department_name, l.city from employees e
    left join departments d on e.department_id=d.department_id
    left join locations l on d.location_id=l.location_id
    where e.hire_date < to_date('01-01-2005', 'DD-MM-YYYY');

-- zadanie 9
select e.first_name, e.last_name, e.hire_date from employees e
    cross join employees em
    where em.last_name = 'King' and e.hire_date > em.hire_date
    group by e.first_name, e.last_name, e.hire_date;

-- zadanie 10
select e.first_name, e.last_name, e.employee_id, e.hire_date,
    em.first_name as "MANAGER_FIRST_NAME", em.last_name as "MANAGER_LAST_NAME", em.employee_id as "MANAGER_EMPLOYEE_ID", em.hire_date as "MANAGER_HIRE_DATE"
    from employees e
    inner join employees em on e.manager_id=em.employee_id
    where e.hire_date < em.hire_date
    order by e.hire_date, em.hire_date;
