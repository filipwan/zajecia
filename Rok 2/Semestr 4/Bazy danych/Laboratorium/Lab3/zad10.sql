select e.first_name, e.last_name, e.employee_id, e.hire_date, em.first_name, em.last_name, em.employee_id, em.hire_date from employees e
    inner join employees em on e.manager_id=em.employee_id
    where e.hire_date < em.hire_date
    order by e.hire_date, em.hire_date