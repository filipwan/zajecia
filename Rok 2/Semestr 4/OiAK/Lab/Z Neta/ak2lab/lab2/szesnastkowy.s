.section .data
STDIN=0
SYSEXIT=60
STDOUT=1
SYSWRITE=1
SYSREAD=0
EXIT_SUCCESS=0
DBUF=1 #max długość tekstu 
.section .bss
.comm textin, 1
.comm liczba, 8
.comm licznik, 4
.comm temp, 1
.section .text
.align 32
.global _start
_start:
movl $0, %eax
movl $0, licznik(,%eax,4)
pobierzznak: 

movl $SYSREAD, %eax #załadowanie polecenia odczytu dla systemu 
movl $STDIN, %ebx #pierwszy parametr - wskazanie wejścia z którego ma być pobrany tekst 
movl $textin, %esi  #drugi parametr - bufor do którwgo tekst zostanie wczytany 
movl $DBUF, %edx #trzeci parametr- długość buforu
syscall #przerwanie ststemowe 

movl $0, %eax # za pomcą tego dostaje się do 0-rowego indeksu 

movb textin(,%eax,1), %bl #kopiuje wczytany znak do rejestru 

#sprawdzam czy to liczba (zasada analogiczna do poprzedniego programu) jeśli tak odejmije od znaku wartość
# kodu '0' i szkacze aby dodać te liczbe d buforu, jeśli nie to skacze do miejsca w którym sprawdzam czy to duża
# litera z zakresu A-F

CMP $'0', %bl
JB zlyznak
CMP $'9' ,%bl
JA duzalitera
SUB $'0', %bl 
JMP dodajdobuforu

# Sprawdzam czy kod mieści się w zakresie dużych liter A-F jeśli nie, to skacze do miejsca w którym
# sprawdzam czy to mała litera a-f, jeśli tak, to zmniejszam kod o kod 'A'-10 tak aby A oznaczało 10 itd

duzalitera:
CMP $'A', %bl
JB zlyznak
CMP $'F', %bl
JA malalitera
SUB $'A'-10, %bl
JMP dodajdobuforu

# Sprawdzam czy znak mieści się w zakresie a-f, jeśli tak traktuje go analogicznie do dużych liter,
# jeśli nie uznaje za znak z poza zakresu i skaczę do etykiety obsługującej ten przypadek 

malalitera:
CMP $'a', %bl 
JB zlyznak
CMP $'f', %bl
JA zlyznak
SUB $'a'-10, %bl
JMP dodajdobuforu

zlyznak:
JMP pobierzznak #traktuej znak jakby go nie było więc skacze do początku pęli bez zmiany licznika 

# Dodawanie do bufou odbywa się w 2 przypadkach. Słowo ma 1 bajt, a znak w kodzie szesnastkowym 4 bity, więc na
# jedno miejsce w tablicy przypadają 2 znaki. Gdy licznik pętli jest parzysty (nieparzyste przejście) mnoże daną
# liczbę przez 16, tak aby zajęła pierwsze, bardziej znaczące 4 bajty i zachowuje ją w tymczasowym buforze.
# W karzdym parzystym przejściu pętli dodaje do liczby z tymczasowego buforu wczytany znak i tak przygotowany
# bajt mogę skopiować do buforu z wynikiem 

dodajdobuforu:
movl $0, %eax
movl licznik(,%eax,4), %eax
movl $2, %ecx
DIV %ecx
CMP $0, %edx
JE zachowajwbuf
movl $0, %eax
movl licznik(,%eax,4), %eax
shr %eax # unikam problemów z dzieleniem
movl $0, %edx
add temp(,%edx,1), %bl
movb %bl, liczba(,%eax,1)
JMP koniecpetli

zachowajwbuf:
movb $16, %al
MUL %bl
movl $0, %ecx
movb %al, temp(,%ecx,1)

# Pobieram licznik, z buforu w którym go składuje, dodaje do niego 1 i sprawdzam czy pętla dalej ma się wykonywać
koniecpetli:
movl $0, %eax
movl licznik(,%eax,4), %ecx
add $1, %ecx
movl %ecx, licznik(,%eax,4)
CMP $16, %ecx
JB pobierzznak

#kopiowanie do rejestów, aby zaprezętować wynik w gdb

movl $0, %ecx
movl liczba(,%ecx,4), %eax 
movl $1, %ecx
movl liczba(,%ecx,4), %ebx 
wynik1:

#dodawanie liczby do siebie 

add %ebx, %ebx
adc %eax, %eax 
movl $0, %ecx
adc $0, %ecx
wynik2:

movl $SYSEXIT, %eax #polecenie zakończenia programu
movl $EXIT_SUCCESS, %ebx #parametr oznaczający poprawne zakończenie programu
syscall
