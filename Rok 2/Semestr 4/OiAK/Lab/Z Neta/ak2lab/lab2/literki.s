.section .data
STDIN=0
SYSEXIT=60
STDOUT=1
SYSWRITE=1
SYSREAD=0
EXIT_SUCCESS=0
DBUF=256 #max długość tekstu 
.section .bss
.comm text, 256 #deklaracja buforu o rozmiarze 256 bajtów 
.comm textout, 256
.section .text
.align 32
.global _start
_start:
movl $SYSREAD, %eax #załadowanie polecenia odczytu dla systemu 
movl $STDIN, %ebx #pierwszy parametr - wskazanie wejścia z którego ma być pobrany tekst 
movl $text, %esi  #drugi parametr - bufor do którwgo tekst zostanie wczytany 
movl $DBUF, %edx #trzeci parametr- długość buforu
syscall #przerwanie ststemowe 

movl $0, %ecx #zerowanie licznika pętli 

spr:
movb text(,%ecx,1), %bl #wczytuje kolejny znak do rejestru

# porównuje wczytany znak z literą A (a dokładniej ich kody ASCII). Jeśli jest mniejszy, to skaczę do
# końca pętli, gdyż taki znak na pewno nie będzie literą. Następnie porównuje wczytany znnak z kodem
# ascii litery Z. Jeśli jest większe skacze do etykiedy gdzie znak będzie sprawdzony pod kątem bycia
# dużą literą, a jeśli jest mniejszy bądź równy dodaje różnicę mniędzy dużymi a małymi literami 
# i skaczę do końca pętli

CMP $'A', %bl
JB koniecpetli 
CMP $'Z', %bl 
JA malelitery
ADD $32, %bl
JMP koniecpetli

# Porównuje wczytany znak do kodu a, jeśli jest mniejszy nie jest literą (duże już zostały
# wykluczone), więc skaczę do końca pętli. Następnie porównuje go do kodu litery z, jeśli jest
# większy, znak nie jest literą więc również skaczę do końca pętli. Jeśli mieści się w przedziale
# kodów od a do z odejmóje różnicę między małymi a dużymi literami. 

malelitery:
CMP $'a', %bl
JB koniecpetli 
CMP $'z', %bl 
JA koniecpetli
SUB $32, %bl

# Dodaje przetworzony (lub też nie jeśli znak nie był literą) znak do bufora przechowywującego tekst
# wyjściowy. Następenie zwiększam licznik pętli o 1 i porównuje go z maksymalną dłuością tekstu -1
# (indeksowanie i pętla jest od 0, a rozmiar od 1).

koniecpetli:
movb %bl, textout(,%ecx,1)
add $1, %ecx
cmp $255, %ecx
JBE spr

movl $SYSWRITE, %eax #załadowanie polecenia odczytu dla systemu 
movl $STDOUT, %ebx #pierwszy parametr - wskazanie wyjścia na które tekst ma zostać wyświetlony 
movl $textout, %esi #drugi parametr - źródło wyświetlanego r=tekstu
movl $DBUF, %edx #trzeci parametr- długość buforu
syscall # przerwanie systemowe 

movl $SYSEXIT, %eax #polecenie zakończenia programu
movl $EXIT_SUCCESS, %ebx #parametr oznaczający poprawne zakończenie programu
syscall



