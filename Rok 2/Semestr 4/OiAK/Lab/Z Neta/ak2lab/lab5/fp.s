.section .data
kontrola: .short 0
i: .double 0
.section .text
.global getstan
.global getstatus
.global setdouble
.global setfloat
.global dfx
.global dgx
.type getstan @function
.type getstatus @function
.type setdouble @function
.type setfloat @function
.type dfx @function
.type dgx @function

##pobranie słowa z koprocesora i wstawienie go do rejestru ax w przypadku pobrania słów statusu i kontroli
getstan:
movl $0, %eax
fstcw kontrola
fwait
mov kontrola, %ax
JMP koniec
getstatus:
mov $0, %eax
fstsw kontrola
fwait
mov kontrola, %ax
JMP koniec
setdouble:
mov $0, %eax
fstcw kontrola #pobranie słowa sterującego do zmiennej typu short 
fwait #czekanie aż koprocesor zakończy zadane operacje 
mov kontrola, %ax #skopiowanie słowa do rejestru eax 
and $0xFCFF, %ax  #wyzerowanie 9tego i 10 tego bitu odpowiadającego za tryb pracy (00-single)
xor $0x200, %ax #zamiana bitów 9tego i 10 teg na 10 (double) 
mov %ax, kontrola #skopiowanie zmodyfikowanego słowa do zmiennej 
fldcw kontrola #załadowanie słowa sterującego do rejestru w koprocesorze 
JMP koniec
setfloat:
#analogicznie do double
mov $0, %eax
fstcw kontrola
fwait
mov kontrola, %ax
and $0xFCFF, %ax #wyzerowanie 9tego i 10 tego bitu odpowiadającego za tryb pracy (00-single)
mov %ax, kontrola
fldcw kontrola
JMP koniec
dfx:
sub $8, %rsp
movsd %xmm0, (%rsp)

fldl (%rsp) #wkładanie do ST0 
fld %st #kopiowanie zawartosci st0 do st1 (w skrócie)
fmul %st, %st(1) #mnożenie liczby przez samą siebie (podnoszenie do potęgi)
fwait #czekanie, aż koprocesor zakończy operacje 

fstp %st# usuwanie niezmodyfikowanej liczby z st0 (st1 => st0)
fld1 #wstawnienie wartości 1.0 do st0 
fadd %st, %st(1) # dodawanie jedynki do kwadratu zadanej watrtości 
fwait #czekanie, aż koprocesor zakończy operacje 

fstp %st# usuwanie wartości 1 z st0 (st1 => st0)
fsqrt #liczenie pierwiastka aktualnie posiadanego wyniku znajdującego się w st0 
fwait #czekanie, aż koprocesor zakończy operacje 

fld1 #kopiowanie zawartosci st0 do st1 (w skrócie)
fxch %st(1) #zamiana miejscami st0 i st1 
fsub %st, %st(1) #odjęcie od st0 st1 i zapisanie w st1 
fwait #czekanie, aż koprocesor zakończy operacje 

fstp %st# usuwanie jedynki ze rejestru koprocesora 
fstpl (%rsp) #włożenie wartości 
movsd (%rsp), %xmm0 
add $8, %rsp
JMP koniec
dgx:
sub $8, %rsp
movsd %xmm0, (%rsp)
fldl (%rsp) #wkładanie do ST0 
fld %st #kopiowanie zawartosci st0 do st1 (w skrócie)
fmul %st, %st(1) #mnożenie liczby przez samą siebie (podnoszenie do potęgi)
fwait #czekanie, aż koprocesor zakończy operacje 

fstp %st# usuwanie niezmodyfikowanej liczby z st0 (st1 => st0)
fld %st #kopiowanie zawartosci st0 do st1 (w skrócie)
fld1 
fadd %st, %st(1) # dodawanie jedynki do kwadratu zadanej watrtości 
fwait #czekanie, aż koprocesor zakończy operacje 

fstp %st# usuwanie wartości 1 z st0 (st1 => st0)

fsqrt #policznie pierwiastka i zapisanie go w st0
fwait

fld1 
fadd %st, %st(1) # dodawanie jedynki do kwadratu zadanej watrtości 
fwait #czekanie, aż koprocesor zakończy operacje 

fstp %st# usuwanie wartości 1 z st0 (st1 => st0)
fxch %st(1) #zamiana miejscami st0 i st1 
fdiv %st, %st(1)
fwait

fstp %st# usuwanie wartości 1 z st0 (st1 => st0)
fstpl (%rsp) #włożenie wyniku na stos (nadpisując zmienną, którą przyjmowałem jako parametr)
movsd (%rsp), %xmm0  #przeniesienie wyniku ze stosu do rejestru xmm0, aby przekazać go do C 
add $8, %rsp #przesunięcie stack pointera  o osiem bajtów (usunięcie z wyniku ze stosu) 
JMP koniec

koniec:
ret


