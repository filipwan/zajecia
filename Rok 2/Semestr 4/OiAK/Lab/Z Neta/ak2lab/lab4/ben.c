#include <stdio.h>
int main (void)

{
int c=0;
unsigned czas1l, czas1h, czas2l, czas2h; //Deklaracja zmiennych do których przepisze wartości rejestrów po wykonaniu RDTSC
unsigned long long int czas1, czas2; //Zmienne do których przepisze połącząną wyższą i niższą część
printf("Stoper \nNacisnij enter aby roczac pomiar ");
char znak;
scanf("%c", &znak); //oczekiwanie na wystartowanie stopera
   asm volatile ( 
	"cpuid\n\t"
         "RDTSC\n\t"  //Wywołanie RDTSC, które przepsze do rejestów edx (wyższą część) i eax(niższą część) ilość taktów procesora od jego uruchomienia 
         "mov %%edx, %0\n\t"  //Przepisanie parametrów z rejestrów do zadanych zmiennych (%0 to czas1h %1 czas1l)
         "mov %%eax, %1\n\t": "=r" (czas1h), "=r" (czas1l) //Przypisanie zmiennych do rejestrów %0 itd 
); //Całość wczytuje ilość taktów w momęcie rozpoczęcia pomiaru do czas1l i czas1h 



printf("Nacisnij enter, aby zakonczyc pomiar \n");
scanf("%c", &znak); //Oczekiwanie na klawisz do zatrzymania stopera

   asm volatile ( 
	"cpuid\n\t"
         "RDTSC\n\t" //Analogicznie do wywołania ppowyżej
         "mov %%edx, %0\n\t" 
         "mov %%eax, %1\n\t": "=r" (czas2h), "=r" (czas2l)
);//Całość wczytuje ilość taktów w momęcie zakończenia pomiaru


czas1=(unsigned long long int)czas1h; //Przypisanie do 64bitowej zmiennej zrzutowaną wyższą część pomiaru 
czas1=czas1*4294967296;//przesunięcie wyższej części o 32 bity w lewo wykonane za pomocą pomnożenia tej liczy przez 2^32 (to ta duża liczba) 
czas1=czas1+czas1l; //dodanie niższej części do uprzedni przygotowanej wyższej części ilości taktów 
czas2=(unsigned long long int)czas2h; //czas 2 analogicznie do czas 1 
czas2=czas2*4294967296;
czas2=czas2+czas2l;
czas1=czas2-czas1;// odjęcie liczby taktów w chwili rozpoczęcia pomiaru od liczby taktów w chwili jego zakończenia, aby uzyskać liczbę taktów, która wykonała się podczas trwania pomiaru. 
double wynik;
wynik=(double)czas1; //rzutowanie tej różnicy na double, aby można uzyskać wynik w postaci zmiennoprzecinkowej 
wynik=wynik/3330000000; //podzielenie liczby taktów podczas trwania pomiaru, przez częstotliwość taktowania procesora, którą uzyskałem za pomocą cpuid (3,33GHz)
printf("Pomiar trwal %lfs \n", wynik); //wyswietlenie wyniku 


return 0;
}

