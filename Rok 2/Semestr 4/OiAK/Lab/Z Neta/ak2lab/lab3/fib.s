.section .data
STDIN=0
SYSEXIT=60
STDOUT=1
SYSWRITE=1
SYSREAD=0
EXIT_SUCCESS=0
DBUF=128
NR_WYRAZU=30
.section .bss
.comm liczba, 128
.comm liczbab, 128
.comm licznik, 4
.section .text
.align 32
.global _start
_start:
#ustawianie 1 jako pierwszej liczby 
movl $0, %ebx
movl $31, %ecx
ADD $1, %ebx
movl %ebx, liczbab(,%ecx,4)
#dodaje w linijce poniżej indeks pożądanej licznby i przygotowywuje liczniki pod pętle 
movl $NR_WYRAZU, %ebx #F(edx)
movl $0, %ecx
movl %ebx, licznik(,%ecx,4)
movl $1, %ecx #ustawiam licznik petli ogolnej na 1 bo zaczynam od tego wyrazu 
fib:
##zwiekszenie licznika petli i sprawdzenie czy wykonywana jest parzysty raz, jesli tak sume zapisuje w buworze liczba jesli nie liczbab
add $1, %ecx
movl %ecx, %eax
shr %eax
movl $0, %eax
adc $0, %eax
CMP $0, %eax
JNE pnp

## dodaje najnimniej znaczace oizycje bez uzwglednienia przeniesien, poczym przechowuje flagi z tej operacji na stosie, nastepnie zmniejszam licznik petli 
movl $31, %esi #licznik petli wewnetrznej 
movl liczba(,%esi,4), %eax 
movl liczbab(,%esi,4), %edx
add %edx, %eax
PUSHF
movl %eax, liczba(,%esi,4)
movl %eax, %edi ################## używam tego do sprawdzania w gdb , czy ciąg generowany jest prawidłowo 
SUB $1, %esi 

#dodawanie pozycji bardziej znaczacych z uwzglednieniem przeniesienia. Aby to zrobić przed dodaniem liczb pobieram flagi ze stosu , aby uwzglednic przniesienie z poprzedniego dodawania
dodawanie1:
movl liczba(,%esi,4), %eax 
movl liczbab(,%esi,4), %edx
POPF
adc %edx, %eax
PUSHF
movl %eax, liczba(,%esi,4)
SUB $1, %esi 
CMP $0, %esi 
JNE dodawanie1
movl liczba(,%esi,4), %eax 
movl liczbab(,%esi,4), %edx
POPF
adc %edx, %eax
movl %eax, liczba(,%esi,4)
SUB $1, %esi 
CMP $0, %esi 
JMP koniecpetli

pnp: #ten przypadek różni się od poprzedniego zapisaniem wyniku w innym buforze 
movl $31, %esi
movl liczba(,%esi,4), %eax 
movl liczbab(,%esi,4), %edx
add %edx, %eax
PUSHF
movl %eax, liczbab(,%esi,4)
movl %eax, %edi ##################
SUB $1, %esi 

dodawanie2:
movl liczba(,%esi,4), %eax 
movl liczbab(,%esi,4), %edx
POPF
adc %edx, %eax
PUSHF
movl %eax, liczbab(,%esi,4)
SUB $1, %esi 
CMP $0, %esi 
JNE dodawanie2
movl liczba(,%esi,4), %eax 
movl liczbab(,%esi,4), %edx
POPF
adc %edx, %eax
movl %eax, liczbab(,%esi,4)
SUB $1, %esi 
CMP $0, %esi 
JMP koniecpetli

koniecpetli:
movl $0, %edx
movl licznik(,%edx,4), %eax

CMP %eax, %ecx 
JNE fib

#####
#movl $30, %eax 
#movl liczba(,%eax,4), %ebx
####zatrzymujac sie w tym miejscu sprawdzalem czy kolejne 32 bity sa poprawne (w zapytaniu o parzysty wyraz) 

#sprawdzanie czy urzytkownik poprosil o parzysty wyraz ciagu i na podstawie tej decyzji wypisanie na standardowe wyjście odpowiedniego wyniku w konwencji little endian
wypisz:
movl $0, %eax 
movl licznik(,%eax,4), %ebx
shr %ebx
movl $0, %ebx
adc $0, %ebx
CMP $0, %ebx
JNE wnp 

movl $SYSWRITE, %eax #załadowanie polecenia odczytu dla systemu 
movl $STDOUT, %ebx #pierwszy parametr - wskazanie wyjścia na które tekst ma zostać wyświetlony 
movl $liczba, %esi #drugi parametr - źródło wyświetlanego r=tekstu
movl $DBUF, %edx #trzeci parametr- długość buforu
syscall # przerwanie systemowe 
JMP koniecprogramu

wnp:
movl $SYSWRITE, %eax #załadowanie polecenia odczytu dla systemu 
movl $STDOUT, %ebx #pierwszy parametr - wskazanie wyjścia na które tekst ma zostać wyświetlony 
movl $liczbab, %esi #drugi parametr - źródło wyświetlanego r=tekstu
movl $DBUF, %edx #trzeci parametr- długość buforu
syscall # przerwanie systemowe 

koniecprogramu: 
movl $SYSEXIT, %eax #polecenie zakończenia programu
movl $EXIT_SUCCESS, %ebx #parametr oznaczający poprawne zakończenie programu
syscall 


