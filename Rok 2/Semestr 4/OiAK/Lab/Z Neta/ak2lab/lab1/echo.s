.section .data
STDIN=0
SYSEXIT=60
STDOUT=1
SYSWRITE=1
SYSREAD=0
EXIT_SUCCESS=0
DBUF=256 #max długość tekstu 
.section .bss
.comm text, 256 #deklaracja buforu o rozmiarze 256 bajtów 
.section .text
.align 32
.global _start
_start:
movl $SYSREAD, %eax #załadowanie polecenia odczytu dla systemu 
movl $STDIN, %ebx #pierwszy parametr - wskazanie wejścia z którego ma być pobrany tekst 
movl $text, %esi  #drugi parametr - bufor do którwgo tekst zostanie wczytany 
movl $DBUF, %edx #trzeci parametr- długość buforu
syscall #przerwanie ststemowe 

movl $SYSWRITE, %eax #załadowanie polecenia odczytu dla systemu 
movl $STDOUT, %ebx #pierwszy parametr - wskazanie wyjścia na które tekst ma zostać wyświetlony 
movl $text, %esi #drugi parametr - źródło wyświetlanego r=tekstu
movl $DBUF, %edx #trzeci parametr- długość buforu


syscall # przerwanie systemowe 
movl $SYSEXIT, %eax #polecenie zakończenia programu
movl $EXIT_SUCCESS, %ebx #parametr oznaczający poprawne zakończenie programu
syscall



