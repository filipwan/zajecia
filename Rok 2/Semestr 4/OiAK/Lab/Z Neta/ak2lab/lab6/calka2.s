.bss
.comm begin, 4 #bufor dla wartosci poczatkowej
.comm end, 4 #bufor dla wartosci koncowej
.comm dxk, 4 #bufor dla skoku 
.comm dx, 16 #bufor dla wektora skokow 
.comm begins, 16 #bufor dla wektora wartosci poczatkowych 
.comm wynik, 16 #bufor dla wektora wynikow 
.data
kolejka: .float 0, 1, 2, 3 #wektor z kolejnymi krokami 
zera: .float 0, 0, 0, 0 #do zerowania wektorow
przeskoki: .float 4, 4, 4, 4 #do zwiekszania krokow
.text
.global calkaa
.type calkaa @function 
calkaa:
pushq %rbp 
movq %rsp, %rbp #ramka stosu 
##
movss %xmm0, begin #przenoszenie zmiennej z c do bufora 
movss %xmm1, end #-||-
cvtsi2ss %edi, %xmm0 #rzutowanie ilosci prostokatow  na float i zapisanie w xmm0 
##
subss begin, %xmm1 #odejmowanie od wartosci koncowej poczatowej 
divss %xmm0, %xmm1 #wyliczam dx poprzez podzielenie poprzedniego wyniku przez liczbe korokow, wynik jest w xmm1
mov $0, %eax #ustawiam licznik do pobierania z bufora 
movss %xmm1, dxk(,%eax,4) ##na ostatnie poprawki #przenosze wartosc dx, aby pozniej ja wykorzystac
########## A=A+dx/2- operacja między liniami #
movss %xmm1, %xmm4 
movl $2, %eax
cvtsi2ss %eax, %xmm5
divss %xmm5, %xmm4
movss begin, %xmm5
addss %xmm5, %xmm4
movss %xmm4, begin
#########
#twrzenie wektora zawierającego 4 dx-y 
movl $0, %esi
wyp_kol:
movss %xmm1, dx(,%esi,4)
inc %esi
cmp $4, %esi 
je wyj1
jmp wyp_kol
wyj1:
movups dx, %xmm1 # teraz w xmm1 jest wetor 4 dx a nie jeden 

#wypełnienaie wektora wartości początkowych  
movl $0, %esi 
movss begin, %xmm2
wyp_beg:
movss %xmm2, begins(,%esi,4)
inc %esi
cmp $4, %esi
je wyj2
jmp wyp_beg
wyj2: 

#wektory przenosze do rejestrow aby móc ich użyć w obliczeniach 
movups begins, %xmm2
movups kolejka, %xmm3
mov %edi, %ecx #zachowywanie oryginalnej wartosci, aby pozniej porownac ile krokow brakuje 
shrl $2, %edi #dziellenie ilosci krokow na 4, bo 4 obliczenia wykonuja się jednocześnie 
mov %edi, %edx 
shll $2, %edx #mnoze wartosc przez 4, aby pozniej porownac ilosc wykonanych obliczen z oryginalem 
movups zera, %xmm0 #zeruje miejsce na wyniki 

petla:
movups zera, %xmm4 #zeruje rejstr aby usunac poprzednie wysoksci  
addps %xmm3, %xmm4 #dodawanie numerow krokow do rejestru xmm4
mulps %xmm1, %xmm4 #mnzenie przez wektor z warosciami dx
addps %xmm2, %xmm4 #dodawanie wektora z wartoscia poczatkowa 
mulps %xmm4, %xmm4 #podnoszenie do kwadratu (teraz to wysokosc porostokata)
addps %xmm4, %xmm0 #dodanie do rejestru 
movups przeskoki, %xmm5 #przenoszenie wektora 4 do rejestru 
addps %xmm5, %xmm3 #zwiekszenie nr kazdego kroku o 4
dec %edi #zmniejszenie licznika petli 
cmp $0, %edi #sprawdzenie warunku petli 
je wyj3
jmp petla
wyj3: 
mulps %xmm1, %xmm0 #mozenie sumy przez dx, aby otrzymac pola
movl $0, %esi 
movups %xmm0, wynik  #przeniesienie sumy do bufora
movups zera, %xmm0 # wyzerowanie rejestru 
sumowanie:#sumowanie 4 pol z bufora
addss wynik(,%esi,4), %xmm0
inc %esi
cmp $4, %esi 
je koniec
jmp sumowanie
koniec:
####
mov $0, %eax
cmp %ecx, %edx #sprawdzenie czy wykonaly sie wszystkie sumy 
je dkoniec
korekta: #jesli nie do analogicznie sumujemy do 3 ktore zostaly 
inc %edx
cvtsi2ss %edx, %xmm3 
mulss dxk(,%eax,4), %xmm3
addss begin(,%eax,4), %xmm3
mulss dxk(,%eax,4), %xmm3
addss %xmm3, %xmm0
cmp %ecx, %edx
je dkoniec
jmp korekta
dkoniec:
#####
leave
ret


