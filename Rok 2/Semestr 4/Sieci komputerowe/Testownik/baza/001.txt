X0001
What is a characteristic of a fault tolerant network?
a network that protects confidential information from unauthorized access
a network that can expand quickly to support new users and applications without impacting the performance of the service delivered to existing users
a network that supports a mechanism for managing congestion and ensuring reliable delivery of content to all users
a network that recovers quickly when a failure occurs and depends on redundancy to limit the impact of a failure