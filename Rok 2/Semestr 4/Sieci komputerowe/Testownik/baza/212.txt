X0010
A technician is configuring a router to allow for all forms of management access. As part of each different type of access, the technician is trying to type the command login. Which configuration mode should be entered to do this task?
user executive mode
global configuration mode
any line configuration mode
privileged EXEC mode