X100110
A host is accessing an FTP server on a remote network. Which three functions are performed by intermediary network devices during this conversation? (Choose three.)
regenerating data signals
acting as a client or a server
providing a channel over which messages travel
applying security settings to control the flow of data
notifying other devices when errors occur
serving as the source or destination of the messages