X011100
What are three characteristics of multicast transmission? (Choose three.)
The source address of a multicast transmission is in the range of 224.0.0.0 to 224.0.0.255.
A single packet can be sent to a group of hosts. 
Multicast transmission can be used by routers to exchange routing information. 
Routers will not forward multicast addresses in the range of 224.0.0.0 to 224.0.0.255.
Computers use multicast transmission to request IPv4 addresses.
Multicast messages map lower layer addresses to upper layer addresses.