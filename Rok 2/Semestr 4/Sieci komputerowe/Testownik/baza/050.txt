X01010
In what two situations would UDP be the preferred transport protocol over TCP? (Choose two.)
when applications need to guarantee that a packet arrives intact, in sequence, and unduplicated
when a faster delivery mechanism is needed
when delivery overhead is not an issue
when applications do not need to guarantee delivery of the data
when destination port numbers are dynamic