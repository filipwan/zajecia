X0001
When applied to a router, which command would help mitigate brute-force password attacks against the router?
exec-timeout 30
service password-encryption
banner motd $Max failed logins = 5$
login block-for 60 attempts 5 within 60