X1000
Which statement is true about DHCP operation?
When a device that is configured to use DHCP boots, the client broadcasts a DHCPDISCOVER message to identify any available DHCP servers on the network.
A client must wait for lease expiration before it sends another DHCPREOUEST message.
The DHCPDISCOVER message contains the IP address and sub net masK to be assigned, the IP address of the DNS server, and the IP address of the default gateway.
If the client receives several DHCPOFFER messages from different servers, it sends a unicast DHCPREOUEST message to the server from which it chooses to obtain the IP information.