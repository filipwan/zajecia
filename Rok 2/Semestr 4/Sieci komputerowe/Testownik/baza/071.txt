X0010
How does quality of service help a network support a wide range of applications and services?
by limiting the impact of a network failure
by allowing quick recovery from network failures
by providing mechanisms to manage congested network traffic
by providing the ability for the network to grow to accommodate new users