X0100
What is the purpose of adjacency tables as used in Cisco Express Forwarding (CEF)?
to populate the forwarding information base (FIB)
to maintain Layer 2 next-hop addresses
to allow the separation of Layer 2 and Layer 3 decision making
to update the forwarding information base (FIB)