X1000
What is the purpose of the command ping ::1?
It tests the internal configuration of an IPv6 host.
It tests the broadcast capability of all hosts on the subnet.
It tests the multicast connectivity to all hosts on the subnet.
It tests the reachability of the default gateway for the network.