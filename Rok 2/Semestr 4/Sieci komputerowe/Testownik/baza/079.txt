X1000
Which procedure is used to reduce the effect of crosstalk in copper cables?
twisting opposing circuit wire pairs together
wrapping the bundle of wires with metallic shielding
designing a cable infrastructure to avoid crosstalk interference
avoiding sharp bends during installation