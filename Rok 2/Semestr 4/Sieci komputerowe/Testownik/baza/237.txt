X10100
How could a faulty network device create a source of hazard for a user? (Choose two.)
It could stop functioning.
It could apply dangerous voltage to other pieces of equipment.
It could explode.
It could produce an unsafe electromagnetic field.
It could apply dangerous voltage to itself.