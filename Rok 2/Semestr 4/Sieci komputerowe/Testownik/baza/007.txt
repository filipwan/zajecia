X0100
An administrator uses the Ctrl-Shift-6 key combination on a switch after issuing the ping command. What is the purpose of using these keystrokes?
to restart the ping process
to interrupt the ping process
to exit to a different configuration mode
to allow the user to complete the command