X0010
A network engineer is analyzing reports from a recently performed network baseline. Which situation would depict a possible latency issue?
a change in the bandwidth according to the show interfaces output
a next-hop timeout from a traceroute
an increase in host-to-host ping response times
a change in the amount of RAM according to the show version output