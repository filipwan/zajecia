X10100
What are two characteristics of Ethernet MAC addresses? (Choose two.)
They are globally unique.
They are routable on the Internet.
They are expressed as 12 hexadecimal digits.
MAC addresses use a flexible hierarchical structure.
MAC addresses must be unique for both Ethernet and serial interfaces on a device.