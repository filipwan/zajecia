X0010
Which statement accurately describes a TCP/IP encapsulation process when a PC is sending data to the network?
Data is sent from the internet layer to the network access layer.
Packets are sent from the network access layer to the transport layer.
Segments are sent from the transport layer to the internet layer.
Frames are sent from the network access layer to the internet layer.