X0100
What important information is added to the TCP/IP transport layer header to ensure communication and connectivity with a remote network device?
timing and synchronization
destination and source port numbers
destination and source physical addresses
destination and source logical network addresses