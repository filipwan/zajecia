X11000
Under which two circumstances will a switch flood a frame out of every port except the port that the frame was received on? (Choose two.)
The frame has the broadcast address as the destination address. 
The destination address is unknown to the switch.
The source address in the frame header is the broadcast address.
The source address in the frame is a multicast address.
The destination address in the frame is a known unicast address.