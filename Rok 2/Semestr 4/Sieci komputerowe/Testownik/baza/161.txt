X101100
A network technician is measuring the transfer of bits across the company backbone for a mission critical application. The technician notices that the network throughput appears lower than the bandwidth expected. Which three factors could influence the differences in throughput? (Choose three.)
the amount of traffic that is currently crossing the network
the sophistication of the encapsulation method applied to the data
the type of traffic that is crossing the network
the latency that is created by the number of network devices that the data is crossing
the bandwidth of the WAN connection to the Internet
the reliability of the gigabit Ethernet infrastructure of the backbone