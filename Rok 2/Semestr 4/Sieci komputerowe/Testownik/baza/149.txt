X0010
What is the purpose of the network security accounting function?
to require users to prove who they are
to determine which resources a user can access
to keep track of the actions of a user
to provide challenge and response questions