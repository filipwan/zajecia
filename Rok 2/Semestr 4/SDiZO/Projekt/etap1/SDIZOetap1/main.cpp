
#include <iostream>

#include "Testy.h"
#include "TestyAutomatyczne.h"

using namespace std;

int main() {
    //Inicjalizacja klasy testuj\245cej
    Testy testy;
    TestyAutomatyczne testyAutomatyczne;
    int wybor = 5;
    string tn = "n";

    cout << "SDiZO, Projekt 1, Filip Wanat, 241121" << endl << endl;


    //Pocz\245tkowe menu wyboru
    while (wybor != 0) {
        cout << "Wybierz test:" << endl;
        cout << "    1. Tablica Dynamiczna" << endl;
        cout << "    2. Lista" << endl;
        cout << "    3. Kopiec Binarny" << endl;
        cout << "    4. Drzewo Czerwono-Czarne" << endl;
        cout << "    0. Wyj\230cie" << endl << endl;
        cout << "Wyb\242r: ";
        cin >> wybor;

        //Zamknij, je\276eli wybrano 0
        if (wybor == 0) return 0;

        cout << "Automatyzowa\206 test? (t/n): ";
        cin >> tn;

        switch (wybor) {
            default:
                cout << "B\210\251dny wyb\242r!" << endl;
                break;

            case 0:
                break;

            case 1:
                cout << "Rozpoczynam test tablicy dynamicznej..." << endl;
                if (tn == "t") testyAutomatyczne.testTablicy();
                else testy.testTablicy();

                break;

            case 2:
                cout << "Rozpoczynam test listy..." << endl;
                if (tn == "t") testyAutomatyczne.testListy();
                else testy.testListy();
                break;

            case 3:
                cout << "Rozpoczynam test kopca binarnego..." << endl;
                if (tn == "t") testyAutomatyczne.testKopca();
                else testy.testKopca();
                break;

            case 4:
                cout << "Rozpoczynam test drzewa..." << endl;
                if (tn == "t") testyAutomatyczne.testDrzewa();
                else testy.testDrzewa();
                break;

        }
    }
    return 0;
}
