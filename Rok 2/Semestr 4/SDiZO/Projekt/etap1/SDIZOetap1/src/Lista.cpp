#include <iostream>
#include "Lista.h"

using namespace std;

Lista::Lista() {
    Lista::pierwszyElement = NULL;
    Lista::ostatniElement = NULL;
    Lista::rozmiar = 0;
}

Lista::~Lista() {
    for (int i = 0; i < Lista::rozmiar; i++) {
        ElementListy *Element;
        // Przypisz kolejny element jako aktualny
        Element = pierwszyElement->nastepny;
        // Usuń pierwszy element
        delete pierwszyElement;
        pierwszyElement = Element;
    }
}

void Lista::dodajNaPoczatek(int wartosc) {
    ElementListy *Element;
    // Sprawdź, czy lista nie jest pusta
    if (pierwszyElement != NULL) {
        // Ustaw poczatkowy element jako aktualny, zastąp pierwszy element nowym
        // Oraz ustaw jego wartosc i wskaznik na kolejny
        Element = pierwszyElement;
        pierwszyElement = new ElementListy(wartosc,Element, NULL);
        Element->poprzedni = pierwszyElement;
        rozmiar = rozmiar + 1;
    } else {
        // Utwórz element i dodaj go jako poczatkowy i koncowy
        pierwszyElement = new ElementListy(wartosc, NULL, NULL);
        ostatniElement = pierwszyElement;
        rozmiar = rozmiar + 1;
    }
}

void Lista::dodajNaKoniec(int wartosc) {
    ElementListy *Element;
    // Sprawdź, czy element początkowy istnieje
    if (pierwszyElement == NULL) {
        // Utwórz nowy element i ustaw go jako element początkowy i końcowy
        ostatniElement = new ElementListy(wartosc, NULL, NULL);
        pierwszyElement = ostatniElement;
    } else {
        // Ustaw element, który był ostatnim, jako aktualny
        Element = ostatniElement;
        // Zastąp ostatni element nowym
        // Ustaw jego wartość i element poprzedni na element aktualny
        ostatniElement = new ElementListy(wartosc, NULL, Element);
        Element->nastepny = ostatniElement;
    }
    // Powiększ rozmiar listy
    rozmiar++;
}

void Lista::dodajDowolnie(int wartosc, int pozycja) {
    ElementListy *Element;
    // Sprawdź, czy w liście istnieje podana pozycja
    if (pozycja < 0 || pozycja > rozmiar) {
        cout << "W li\230cie nie istnieje pozycja [" << pozycja << "]" << endl;
        return;
    }

    // Sprawdź, czy wybrana pozycja jest pierwsza
    if (pozycja == 0) {
        Lista::dodajNaPoczatek(wartosc);
        return;
    }

    // Sprawdź, czy wybrana pozycja jest ostatnia
    if (pozycja == rozmiar ) {
        dodajNaKoniec(wartosc);
        return;
    }

    // Przypisz element pierwszy za aktualny
    Element = pierwszyElement;

    // Przesuń wszyskie elementy o jeden
    for (int i = 1; i < pozycja ; ++i)
        Element = Element->nastepny;

    // Stwórz nowy element listy o podanych parametrach
    ElementListy *nowyElementListy = new ElementListy(wartosc, Element->nastepny, Element);

    // Przypisz nowy element w odpowiednim miejscu tablicy i zwieksz jej rozmiar
    Element->nastepny->poprzedni = nowyElementListy;
    Element->nastepny = nowyElementListy;
    rozmiar++;
}

void Lista::usunPierwszy() {
    ElementListy *Element;
    if (rozmiar == 0) {
        cout<<"Pusta lista"<<endl;
        return;
    }

    if(rozmiar > 1) {
        Element = pierwszyElement->nastepny;
        // Usuń pierwszy element
        delete pierwszyElement;
        Element->poprzedni = NULL;
        pierwszyElement = Element;
    }  else {
        Element->poprzedni = NULL;
        pierwszyElement = Element;
    }
    // Zmniejsz rozmiar listy
    rozmiar--;
}

void Lista::usunOstatni() {
    if(rozmiar == 0) {
        cout<<"Pusta lista"<<endl;
        return;
    }
    ElementListy *Element;
    // Przypisz przedostani element jako aktualny
    Element = ostatniElement->poprzedni;
    // Usuń ostatni element
    delete ostatniElement;

    // Sprawdź, czy w liście znajdują się jakieś elementy
    // Jeśli tak, ustaw aktualny element jako ostatni
    // W przeciwnym wypadku wyzeruj elementy listy
    if (rozmiar > 1) {
        Element->nastepny = NULL;
        ostatniElement = Element;
    } else {
        Element = NULL;
        pierwszyElement = NULL;
        ostatniElement = NULL;
    }
    // Zmniejsz rozmiar listy
    rozmiar--;
}

void Lista::usunWybrany(int pozycja) {
    ElementListy *Element;
    if(rozmiar == 0){
        cout<<"Pusta lista"<<endl;
        return;
    }
    // Sprawdź, czy w liście jest podana pozycja
    if (pozycja < 0 || pozycja > rozmiar) {
        cout << "W li\230cie nie istnieje pozycja [" << pozycja << "]" << endl;
        return;
    }

    // Sprawdź, czy wybrana pozycja jest pierwsza
    if (pozycja == 0) {
        usunPierwszy();
        return;
    }

    // Sprawdź, czy wybrana pozycja jest ostatnia
    if (pozycja == rozmiar - 1) {
        usunOstatni();
        return;
    }

    // Przypisz pierwszy element za aktualny
    Element = pierwszyElement;

    // Przesuń wszyskie elementy o jeden dalej
    for (int i = 0; i < pozycja ; ++i)
        Element = Element->nastepny;

    // Przypisz nowy element w odpowiednim miejscu tablicy
    Element->nastepny->poprzedni = Element->poprzedni;
    Element->poprzedni->nastepny = Element->nastepny;

    delete  Element;
    //Zmniejsz rozmiar listy
    rozmiar--;
}

bool Lista::sprawdzCzyIstnieje(int wartosc) {
    ElementListy *Element;
    //Jeżeli lista jest pusta, to zwróć false
    if (rozmiar == 0) {
        cout<<"Pusta lista"<<endl;
        return false;
    }

    //Przypisa pierwszy element jako aktualny
    Element = pierwszyElement;

    // Znajdź element w liście
    for (int i = 0; i < rozmiar; i++) {
        if (Element->wartosc == wartosc) {
            cout << "Szukana warto\230\206 znajduje sie na [" << i << "] pozycji" << endl;
            return true;
        }
        Element = Element->nastepny;
    }

    // Zwróć false w razie braku wartości
    cout << "Szukana warto\230\206 nie wyst\251puje w li\230cie" << endl;
    return false;
}

void Lista::wydrukujListe() {
    ElementListy *Element;
    // Ustaw sie na poczatku listy
    Element = pierwszyElement;

    for (int i = 0; i < rozmiar; i++) {
        cout << "[" << i << "] " << Element->wartosc << endl;
        // Przestaw się na kolejny element
        Element = Element->nastepny;
    }
    cout<<rozmiar;
}
