#ifndef KOPIEC_H
#define KOPIEC_H
#include <string>



class Kopiec {
public:
    int *tablica;
    unsigned int rozmiar;

    Kopiec();

    ~Kopiec();

    void dodaj(int);

    void usun(int);

    bool sprawdzCzyIstnieje(int);

    void wydrukujKopiec();

    void poprawStrukture();
    void wyrysuj();
    void wyswietl_ladnie(std::string, std::string, std::string, std::string, std::string, int);

    int iloscPoziomow();
};



#endif // KOPIEC_H
