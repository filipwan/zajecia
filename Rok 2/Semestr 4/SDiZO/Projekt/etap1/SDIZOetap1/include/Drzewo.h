#ifndef DRZEWO_H
#define DRZEWO_H


#include <iostream>
#include "ElementDrzewa.h"

using namespace std;


class Drzewo {

public:

    int rozmiar;

    Drzewo();

    ~Drzewo();

    void usunWszystko();

    void dodaj(int);

    void usun(int);

    void obrotWLewo(ElementDrzewa *);

    void obrotWPrawo(ElementDrzewa *);

    void sprawdzCzyIstnieje(int);

    void wydrukuj();

private:

    ElementDrzewa *korzen;

    ElementDrzewa wezel;

    string cr,cl,cp;       // �a�cuchy do znak�w ramek

    void usunElement(ElementDrzewa *);

    void znajdzElement(int, ElementDrzewa *, ElementDrzewa *&);

    void znajdzWartosc(int, ElementDrzewa *, bool &znalezione);

    ElementDrzewa *znajdzNastepnyElement(ElementDrzewa *);

    ElementDrzewa *znajdzNajmniejszyElementDrzewa(ElementDrzewa *);

    void wydrukujDrzewo(string, string, ElementDrzewa *);
};

#endif // DRZEWO_H
