#include "../include/Kopiec.h"

#include <iostream>

using namespace std;

Kopiec::Kopiec() {
    Kopiec::tablica = NULL;
    Kopiec::rozmiar = 0;
}

Kopiec::~Kopiec() {
    if (Kopiec::rozmiar > 0)
        delete[]tablica;
}

void Kopiec::dodaj(int wartosc) {
        // Tworzenie tablicy większej o element
        int *nowaTablica = new int[rozmiar + 1];

        // Przepisanie danych do nowej tablicy
        for (int i = 0; i < rozmiar; i++)
            nowaTablica[i] = tablica[i];

        // Dodanie wartości do kopca
        nowaTablica[rozmiar] = wartosc;

        // Usinięcie starej tablicy
        delete[]tablica;

        // Przypisanie nowej tablicy do starej
        tablica = nowaTablica;

        // Aktualizacja rozmiaru kopca
        rozmiar++;

        // Poprawienie struktury kopca
        Kopiec::poprawStrukture();
    }

void Kopiec::usun(int wartosc) {
    // Znajdź wartość do usunięcia
    for (int i = 0; i < rozmiar; i++) {
        if (tablica[i] == wartosc) {
            // Stwórz tablicę mniejszą o element
            int *nowaTablica = new int[rozmiar - 1];

            // Przepisz elementy z tablicy do pozycji na której jest usuwany element
            for (int k = 0; k < i; k++)
                nowaTablica[k] = tablica[k];

            // Przepisz pozostałe elementy przesunięte o jedną pozycję
            for (int k = i + 1; k < rozmiar; k++)
                nowaTablica[k - 1] = tablica[k];

            // Usuń starą tablicę i przypisz na jej miejsce nową
            delete[]tablica;
            tablica = nowaTablica;

            // Zmniejsz rozmiar tablicy
            rozmiar--;

            // Popraw strukturę kopca
            Kopiec::poprawStrukture();
            return;
        }
    }
}

bool Kopiec::sprawdzCzyIstnieje(int wartosc) {
    // Znajdź w tablicy szukaną wartość
    //  - zwraca true w razie znalezienia lub false w razie nieznalezienia
    for (int i = 0; i < rozmiar; i++)
        if (tablica[i] == wartosc) {
            cout << "Szukana warto\230\206 zajmuje w tablicy kopca pozycj\251 [" << i << "]" << endl;
            return true;
        }
    cout << "Szukana warto\230\206 nie wyst\251puje w kopcu" << endl;
    return false;
}

void Kopiec::wydrukujKopiec() {
    cout << "Aktualny stan kopca:" << endl;
    // Jeśli tablica nie jest pusta, to wypisz wszystkie elementy
    // W przeciwnym wypadku wyświetl komunikat
    if (tablica != NULL)
        for (int i = 0; i < rozmiar; i++)
            cout << "    [" << i << "] " << tablica[i] << endl;
    else
        cout << "    Tablica nie ma \276adnych element\242w" << endl;
}

void Kopiec::poprawStrukture() {
	int temp = 0;
	// Popraw strukturę kopca
	for (int i = rozmiar - 1; i > 0; i--)
		if (tablica[i] > tablica[(i - 1) / 2])
		{
			temp = tablica[(i - 1) / 2];
			tablica[(i - 1) / 2] = tablica[i];
			tablica[i] = temp;
		}
}

void Kopiec::wyrysuj(){
    // Przygotowywuje znaki do wyświetlenia graficznej struktury kopca
    std::string cr, cl, cp;
    cr = cl = cp = "  ";
    cr[0] = 218; cr[1] = 196;
    cl[0] = 192; cl[1] = 196;
    cp[0] = 179;
    std::cout<<std::endl;
    wyswietl_ladnie(cr,cl,cp,"","",0);
}

void Kopiec::wyswietl_ladnie(std::string cr, std::string cl, std::string cp, std::string sp, std::string sn,int ktora){
    std::string s;
    // Wyświetla graficznie strukturę kopca
  if(ktora<rozmiar)
  {
    s = sp;
    if(sn == cr) s[s.length() - 2] = ' ';
    wyswietl_ladnie(cr,cl,cp,s + cp, cr, 2*ktora+2);

    s = s.substr(0,sp.length()-2);
    std::cout << s << sn << tablica[ktora] << std::endl;

    s = sp;
    if(sn == cl) s[s.length() - 2] = ' ';
    wyswietl_ladnie(cr,cl,cp,s + cp, cl, 2*ktora+1);
  }
}
