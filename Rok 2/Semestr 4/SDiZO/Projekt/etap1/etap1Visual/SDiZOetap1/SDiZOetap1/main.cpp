
#include <iostream>
#include <string>

#include "include/Testy.h"
#include "include/TestyAutomatyczne.h"

using namespace std;

int main() {
    // Inicjalizacja klasy do test�w
    Testy testy;
    TestyAutomatyczne testyAutomatyczne;
    int wybor = 5;
    string tn = "n";

	cout << "|-------------------------------------------|\n"
		 << "| Struktury Danych i Z\210o\276ono\230\206 Obliczeniowa |\n" 
		 << "|          Zadanie projektowe nr 1          |\n"
		 << "|                Filip Wanat                |\n"
		 << "|           Numer albumu: 241121            |\n" 
		 << "|-------------------------------------------|" << endl << endl;

    // Startowe menu wyboru
    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz test:                    |" << endl;
		cout << "|   1. Tablica                     |" << endl;
		cout << "|   2. Lista                       |" << endl;
		cout << "|   3. Kopiec                      |" << endl;
		cout << "|   4. Drzewo Czerwono-Czarne      |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cout << "Wyb\242r: ";
        cin >> wybor;

        //Zamknij, je�li wybrano 0
        if (wybor == 0) return 0;

        cout << "Automatyzowa\206 test? (t/n): ";
        cin >> tn;

        switch (wybor) {
            case 0:
                break;

            case 1:
                cout << "Rozpoczynam test tablicy dynamicznej..." << endl;
                if (tn == "t") testyAutomatyczne.testTablicy();
                else testy.testTablicy();

                break;

            case 2:
                cout << "Rozpoczynam test listy..." << endl;
                if (tn == "t") testyAutomatyczne.testListy();
                else testy.testListy();
                break;

            case 3:
                cout << "Rozpoczynam test kopca binarnego..." << endl;
                if (tn == "t") testyAutomatyczne.testKopca();
                else testy.testKopca();
                break;

            case 4:
                cout << "Rozpoczynam test drzewa..." << endl;
                if (tn == "t") testyAutomatyczne.testDrzewa();
                else testy.testDrzewa();
                break;

			default:
				cout << "B\210\251dny wyb\242r!" << endl;
				break;
        }
    }
    return 0;
}
