#include <iostream>
#include "../include/Testy.h"
#include "../include/Tablica.h"
#include "../include/Lista.h"
#include "../include/Kopiec.h"
#include "../include/Drzewo.h"

using namespace std;

void Testy::testTablicy() {
    Czas czas;
    Tablica tablica;
    int wybor = 99;
    int wartosc;
    int pozycja;

    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz funkcj\251 tablicy:         |" << endl;
		cout << "|   1. Dodaj na pocz\245tek           |" << endl;
		cout << "|   2. Dodaj na koniec             |" << endl;
		cout << "|   3. Dodaj w wybrane miejsce     |" << endl;
		cout << "|   4. Usu\344 pierwszy               |" << endl;
		cout << "|   5. Usu\344 ostatni                |" << endl;
		cout << "|   6. Usu\344 wybrany element        |" << endl;
		cout << "|   7. Wyszukaj element            |" << endl;
		cout << "|   8. Wydrukuj tablic\251            |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cout << "Wyb\242r: ";
        cin >> wybor;
        switch (wybor) {
            case 0:
                break;

            case 1:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                tablica.dodajNaPoczatek(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 2:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                tablica.dodajNaKoniec(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 3:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                cout << "Podaj pozycj\251: ";
                cin >> pozycja;
                czas.czasStart();
                tablica.dodajDowolnie(wartosc, pozycja);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 4:
                czas.czasStart();
                tablica.usunPierwszy();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 5:
                czas.czasStart();
                tablica.usunOstatni();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 6:
                cout << "Podaj pozycj\251: ";
                cin >> pozycja;
                czas.czasStart();
                tablica.usunWybrany(pozycja);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 7:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                tablica.sprawdzCzyIstnieje(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 8:
                czas.czasStart();
                tablica.wydrukujTablice();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

			default:
				cout << "Bł\251dny wyb\242r!" << endl;
				break;
        }
    }
}

void Testy::testListy() {
    Czas czas;
    Lista lista;
    int wybor = 99;
    int wartosc;
    int pozycja;

    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz funkcj\251 listy:           |" << endl;
		cout << "|   1. Dodaj na pocz\245tek           |" << endl;
		cout << "|   2. Dodaj na koniec             |" << endl;
		cout << "|   3. Dodaj w wybrane miejsce     |" << endl;
		cout << "|   4. Usu\344 pierwszy               |" << endl;
		cout << "|   5. Usu\344 ostatni                |" << endl;
		cout << "|   6. Usu\344 wybrany element        |" << endl;
		cout << "|   7. Wyszukaj element            |" << endl;
		cout << "|   8. Wydrukuj list\251              |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cout << "Wyb\242r: ";
        cin >> wybor;
        switch (wybor) {
            case 0:
                break;

            case 1:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                lista.dodajNaPoczatek(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 2:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                lista.dodajNaKoniec(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 3:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                cout << "Podaj pozycj\251: ";
                cin >> pozycja;
                czas.czasStart();
                lista.dodajDowolnie(wartosc, pozycja);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 4:
                czas.czasStart();
                lista.usunPierwszy();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 5:
                czas.czasStart();
                lista.usunOstatni();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 6:
                cout << "Podaj pozycj\251: ";
                cin >> pozycja;
                czas.czasStart();
                lista.usunWybrany(pozycja);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 7:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                lista.sprawdzCzyIstnieje(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 8:
                czas.czasStart();
                lista.wydrukujListe();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

			default:
				cout << "Bł\251dny wyb\242r!" << endl;
				break;
        }
    }
}

void Testy::testKopca() {
    Czas czas;
    Kopiec kopiec;
    int wybor = 99;
    int wartosc;
    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz funkcj\251 Kopca:           |" << endl;
		cout << "|   1. Dodaj element               |" << endl;
		cout << "|   2. Usu\344 wybrany element        |" << endl;
		cout << "|   3. Wyszukaj                    |" << endl;
		cout << "|   4. Wydrukuj kopiec             |" << endl;
		cout << "|   5. Narysuj kopiec              |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cout << "Wyb\242r: ";
        cin >> wybor;
        switch (wybor) {
            case 0:
                break;

            case 1:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                kopiec.dodaj(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 2:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                kopiec.usun(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 3:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                kopiec.sprawdzCzyIstnieje(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 4:
                czas.czasStart();
                kopiec.wydrukujKopiec();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;
            case 5:
                kopiec.wyrysuj();
                cout<<""<<endl;
                break;

			default:
				cout << "Bł\251dny wyb\242r!" << endl;
				break;
        }
    }
}

void Testy::testDrzewa() {
    Czas czas;
    Drzewo drzewo;
    int wybor = 99;
    int wartosc;
    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz funkcj\251 Drzewa:          |" << endl;
		cout << "|   1. Dodaj element               |" << endl;
		cout << "|   2. Usu\344 wybrany element        |" << endl;
		cout << "|   3. Wyszukaj                    |" << endl;
		cout << "|   4. Wydrukuj drzewo             |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cout << "Wyb\242r: ";
        cin >> wybor;
        switch (wybor) {
            case 0:
                break;

            case 1:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                drzewo.dodaj(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 2:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                drzewo.usun(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 3:
                cout << "Podaj warto\230\206: ";
                cin >> wartosc;
                czas.czasStart();
                drzewo.sprawdzCzyIstnieje(wartosc);
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

            case 4:
                czas.czasStart();
                drzewo.wydrukuj();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

			default:
				cout << "Bł\251dny wyb\242r!" << endl;
				break;
        }
    }
}