#include <iostream>
#include <fstream>
#include "../include/TestyAutomatyczne.h"
#include "../include/Czas.h"
#include "../include/Tablica.h"
#include "../include/Lista.h"
#include "../include/Kopiec.h"
#include "../include/Drzewo.h"
#include <ctime>
#include <cstdlib>
#include <cstdio>

using namespace std;

void TestyAutomatyczne::testTablicy() {
    srand(time(NULL));
    Czas czas;
    Tablica tablica;
    int wybor = 99;
    int wartosc;
    int pozycja;
    string sciezkaWejscia = "F:/Input/";
    string sciezkaWyjscia = "F:/Output/";
    string daneWyjsciowe;
    string daneWejsciowe;
    ofstream plikWyjsciowy;
    ifstream plikWejsciowy;

    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz funkcj\251 tablicy:         |" << endl;
		cout << "|   1. Dodaj na pocz\245tek           |" << endl;
		cout << "|   2. Dodaj na koniec             |" << endl;
		cout << "|   3. Dodaj w wybrane miejsce     |" << endl;
		cout << "|   4. Usu\344 pierwszy               |" << endl;
		cout << "|   5. Usu\344 ostatni                |" << endl;
		cout << "|   6. Usu\344 wybrany element        |" << endl;
		cout << "|   7. Wyszukaj element            |" << endl;
		cout << "|   8. Wydrukuj tablic\251            |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cout << "Wyb\242r: ";
        cin >> wybor;

        //Zamknij, jeśli wybrano 0
        if (wybor == 0) return;

        cout << "Plik zawieraj\245cy dane wej\230ciowe: ";
        cin >> daneWejsciowe;
        cout << "Plik z wynikami testu: ";
        cin >> daneWyjsciowe;

        //Otwórz pliki
        plikWejsciowy.open((sciezkaWejscia + daneWejsciowe).c_str());
        plikWyjsciowy.open((sciezkaWyjscia + daneWyjsciowe).c_str(), fstream::out);
        //Sprawd\276 czy plik jest otwarty poprawnie
        if (plikWejsciowy.is_open()) {
            cout << "Otwarto plik " << daneWejsciowe << endl;
        } else {
            cout << "Nie uda\210o si\251 otworzy\206 pliku wej\230ciowego!" << endl;
            return;
        }

        if (plikWyjsciowy.is_open()) {
            cout << "Otwarto plik " << daneWyjsciowe << endl;
        } else {
            cout << "Nie uda\210o si\251 otworzy\206 pliku wyj\230ciowego!" << endl;
            return;
        }

        switch (wybor) {
            default:
                cout << "B\210\251dny wyb\242r!" << endl;
                break;

            case 0:
                return;

            case 1:
                cout << "Rozpoczynam test automatyczny...";

                while (plikWejsciowy.good()) {
                    //Wczytaj warto\230\206 z pliku
                    plikWejsciowy >> wartosc;
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    tablica.dodajNaPoczatek(wartosc);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 2:
                cout << "Rozpoczynam test automatyczny...";

                while (plikWejsciowy.good()) {
                    //Wczytaj warto\230\206 z pliku
                    plikWejsciowy >> wartosc;
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    tablica.dodajNaKoniec(wartosc);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 3:
                cout << "Rozpoczynam test automatyczny...";

                while (plikWejsciowy.good()) {
                    //Wczytaj warto\230\206 z pliku
                    plikWejsciowy >> wartosc;
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    tablica.dodajDowolnie(wartosc, rand() % tablica.rozmiarTablicy);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 4:
                cout << "Rozpoczynam test automatyczny...";

                //Wype\210nij tablic\251 warto\230ciami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    tablica.dodajNaPoczatek(wartosc);
                }
                while (tablica.rozmiarTablicy != 0) {
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    tablica.usunPierwszy();
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 5:
                //Wype\210nij tablic\251 warto\230ciami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    tablica.dodajNaPoczatek(wartosc);
                }
                while (tablica.rozmiarTablicy != 0) {
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    tablica.usunOstatni();
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 6:
                //Wype\210nij tablic\251 warto\230ciami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    tablica.dodajNaPoczatek(wartosc);
                }
                while (tablica.rozmiarTablicy != 0) {
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    tablica.usunWybrany(rand() % tablica.rozmiarTablicy);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 7:
                //Wype\210nij tablic\251 warto\230ciami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    tablica.dodajNaPoczatek(wartosc);
                }

                for (int i = 0; i < tablica.rozmiarTablicy; i++) {
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    tablica.sprawdzCzyIstnieje(rand() % 2000000 - 1000000);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 8:
                czas.czasStart();
                tablica.wydrukujTablice();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;
        }

    }
}

void TestyAutomatyczne::testListy() {
    srand(time(NULL));
    Czas czas;
    Lista lista;
    int wybor = 99;
    int wartosc;
    int pozycja;
    string sciezkaWejscia = "F:/Input/";
    string sciezkaWyjscia = "F:/Output/";
    string daneWyjsciowe;
    string daneWejsciowe;
    ofstream plikWyjsciowy;
    ifstream plikWejsciowy;


    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz funkcj\251 listy:           |" << endl;
		cout << "|   1. Dodaj na pocz\245tek           |" << endl;
		cout << "|   2. Dodaj na koniec             |" << endl;
		cout << "|   3. Dodaj w wybrane miejsce     |" << endl;
		cout << "|   4. Usu\344 pierwszy               |" << endl;
		cout << "|   5. Usu\344 ostatni                |" << endl;
		cout << "|   6. Usu\344 wybrany element        |" << endl;
		cout << "|   7. Wyszukaj element            |" << endl;
		cout << "|   8. Wydrukuj list\251              |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cin >> wybor;

        // Zamknij, jeśli wybrano 0
        if (wybor == 0) return;

        cout << "Plik zawieraj\245cy dane wej\230ciowe: ";
        cin >> daneWejsciowe;
        cout << "Plik z wynikami testu: ";
        cin >> daneWyjsciowe;


        // Otwórz pliki
        plikWejsciowy.open((sciezkaWejscia + daneWejsciowe).c_str());
        plikWyjsciowy.open((sciezkaWyjscia + daneWyjsciowe).c_str(), fstream::out);
        // Sprawdź, czy plik został otwarty pomyślnie
        if (plikWejsciowy.is_open()) {
            cout << "Otwarto plik " << daneWejsciowe << endl;
        } else {
            cout << "Nie uda\210o si\251 otworzy\206 pliku wej\230ciowego!" << endl;
            return;
        }

        if (plikWyjsciowy.is_open()) {
            cout << "Otwarto plik " << daneWyjsciowe << endl;
        } else {
            cout << "Nie uda\210o si\251 otworzy\206 pliku wyj\230ciowego!" << endl;
            return;
        }

        switch (wybor) {
            default:
                cout << "B\210\251dny wyb\242r!" << endl;
                break;

            case 0:
                break;

            case 1:
                while (plikWejsciowy.good()) {
                    // Wczytaj kolejną wartość z pliku
                    plikWejsciowy >> wartosc;
                    // Wykonaj pomiar czasu wykonywania funkcji
                    czas.czasStart();
                    lista.dodajNaPoczatek(wartosc);
                    czas.czasStop();
                    // Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                // Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 2:
                while (plikWejsciowy.good()) {
					// Wczytaj kolejną wartość z pliku
                    plikWejsciowy >> wartosc;
					// Wykonaj pomiar czasu wykonywania funkcji
                    czas.czasStart();
                    lista.dodajNaKoniec(wartosc);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 3:
                while (plikWejsciowy.good()) {
					// Wczytaj kolejną wartość z pliku
                    plikWejsciowy >> wartosc;
					// Wykonaj pomiar czasu wykonywania funkcji
                    if (lista.rozmiar == 0) {
                        czas.czasStart();
                        lista.dodajDowolnie(wartosc, 0);
                        czas.czasStop();
                    } else {
                        czas.czasStart();
                        lista.dodajDowolnie(wartosc, 1);
                        czas.czasStop();
                    }
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 4:
				// Wypełnij tablicę wartościami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    lista.dodajNaPoczatek(wartosc);
                }
                while (lista.rozmiar != 0) {
					// Wykonaj pomiar czasu wykonywania funkcji
                    czas.czasStart();
                    lista.usunPierwszy();
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 5:
				// Wypełnij tablicę wartościami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    lista.dodajNaPoczatek(wartosc);
                }
                while (lista.rozmiar != 0) {
					// Wykonaj pomiar czasu wykonywania funkcji
                    czas.czasStart();
                    lista.usunOstatni();
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 6:
				// Wykonaj pomiar czasu wykonywania funkcji
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    lista.dodajNaPoczatek(wartosc);
                }
                while (lista.rozmiar != 0) {
					// Wykonaj pomiar czasu wykonywania funkcji
                    czas.czasStart();
                    lista.usunWybrany(rand() % lista.rozmiar);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 7:
                // Wypełnij tablicę wartościami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    lista.dodajNaPoczatek(wartosc);
                }

                for (int i = 0; i < lista.rozmiar; i++) {
					// Wykonaj pomiar czasu wykonywania funkcji
                    czas.czasStart();
                    lista.sprawdzCzyIstnieje(rand() % 2000000 - 1000000);
                    czas.czasStop();
                    // Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                // Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 8:
                czas.czasStart();
                lista.wydrukujListe();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;

        }

    }
}

void TestyAutomatyczne::testKopca() {
    srand(time(NULL));
    Czas czas;
    Kopiec kopiec;
    int wybor = 99;
    int wartosc;
    string sciezkaWejscia = "F:/Input/";
    string sciezkaWyjscia = "F:/Output/";
    string daneWyjsciowe;
    string daneWejsciowe;
    ofstream plikWyjsciowy;
    ifstream plikWejsciowy;

    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz funkcj\251 Kopca:           |" << endl;
		cout << "|   1. Dodaj element               |" << endl;
		cout << "|   2. Usu\344 wybrany element        |" << endl;
		cout << "|   3. Wyszukaj                    |" << endl;
		cout << "|   4. Wydrukuj kopiec             |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cout << "Wyb\242r: ";
        cin >> wybor;

        //Zamknij, jeśli wybrano 0
        if (wybor == 0) return;

        cout << "Plik zawieraj\245cy dane wej\230ciowe: ";
        cin >> daneWejsciowe;
        cout << "Plik z wynikami testu: ";
        cin >> daneWyjsciowe;


        //Otwórz pliki
        plikWejsciowy.open((sciezkaWejscia + daneWejsciowe).c_str());
        plikWyjsciowy.open((sciezkaWyjscia + daneWyjsciowe).c_str(), fstream::out);
        //Sprawdź czy plik został otwarty pomyślnie
        if (plikWejsciowy.is_open()) {
            cout << "Otwarto plik " << daneWejsciowe << endl;
        } else {
            cout << "Nie uda\210o si\251 otworzy\206 pliku wej\230ciowego!" << endl;
            return;
        }

        if (plikWyjsciowy.is_open()) {
            cout << "Otwarto plik " << daneWyjsciowe << endl;
        } else {
            cout << "Nie uda\210o si\251 otworzy\206 pliku wyj\230ciowego!" << endl;
            return;
        }

        switch (wybor) {
            default:
                cout << "B\210\251dny wyb\242r!" << endl;
                break;

            case 0:
                break;

            case 1:
                while (plikWejsciowy.good()) {
                    //Wczytaj wartość z pliku
                    plikWejsciowy >> wartosc;
                    //Wykonaj funkcję z pomiarem
                    czas.czasStart();
                    kopiec.dodaj(wartosc);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 2:


                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    //Wykonaj funkcję z pomiarem
                    czas.czasStart();
                    kopiec.usun(wartosc);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 3:
                // Wypełnij tablicę wartościami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    kopiec.dodaj(wartosc);
                }

                for (int i = 0; i < kopiec.rozmiar; i++) {
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    kopiec.sprawdzCzyIstnieje(rand() % 2000000 - 1000000);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 4:
                czas.czasStart();
                kopiec.wydrukujKopiec();
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;
        }

    }
}

void TestyAutomatyczne::testDrzewa() {
    srand(time(NULL));
    Czas czas;
    Drzewo drzewo;
    int wybor = 99;
    int wartosc;
    string sciezkaWejscia = "F:/Input/";
    string sciezkaWyjscia = "F:/Output/";
    string daneWyjsciowe;
    string daneWejsciowe;
    ofstream plikWyjsciowy;
    ifstream plikWejsciowy;

    while (wybor != 0) {
		cout << endl << endl;
		cout << "|----------------------------------|" << endl;
		cout << "| Wybierz funkcj\251 Drzewa:          |" << endl;
		cout << "|   1. Dodaj element               |" << endl;
		cout << "|   2. Usu\344 wybrany element        |" << endl;
		cout << "|   3. Wyszukaj                    |" << endl;
		cout << "|   4. Wydrukuj drzewo             |" << endl;
		cout << "|   0. Wyj\230cie                     |" << endl;
		cout << "|----------------------------------|" << endl << endl;
        cin >> wybor;

        //Zamknij, je\276eli wybrano 0
        if (wybor == 0) return;

        cout << "Plik zawieraj\245cy dane wej\230ciowe: ";
        cin >> daneWejsciowe;
        cout << "Plik z wynikami testu: ";
        cin >> daneWyjsciowe;


        //Otw\242rz pliki
        plikWejsciowy.open((sciezkaWejscia + daneWejsciowe).c_str());
        plikWyjsciowy.open((sciezkaWyjscia + daneWyjsciowe).c_str(), fstream::out);
        //Sprawd\276 czy plik jest otwarty poprawnie
        if (plikWejsciowy.is_open()) {
            cout << "Otwarto plik " << daneWejsciowe << endl;
        } else {
            cout << "Nie uda\210o si\251 otworzy\206 pliku wej\230ciowego!" << endl;
            return;
        }

        if (plikWyjsciowy.is_open()) {
            cout << "Otwarto plik " << daneWyjsciowe << endl;
        } else {
            cout << "Nie uda\210o si\251 otworzy\206 pliku wyj\230ciowego!" << endl;
            return;
        }

        switch (wybor) {
            default:
                cout << "B\210\251dny wyb\242r!" << endl;
                break;

            case 0:
                break;

            case 1:
                while (plikWejsciowy.good()) {
                    //Wczytaj warto\230\206 z pliku
                    plikWejsciowy >> wartosc;
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    drzewo.dodaj(wartosc);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 2:
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    drzewo.usun(wartosc);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;

            case 3:
                //Wype\210nij tablic\251 warto\230ciami
                while (plikWejsciowy.good()) {
                    plikWejsciowy >> wartosc;
                    drzewo.dodaj(wartosc);
                }

                for (int i = 0; i < drzewo.rozmiar; i++) {
                    //Wykonaj funkcj\251 z pomiarem
                    czas.czasStart();
                    drzewo.sprawdzCzyIstnieje(rand() % 2000000 - 1000000);
                    czas.czasStop();
                    //Zapisz do pliku wynik pomiaru
                    plikWyjsciowy << czas.czasWykonania() << endl;
                }

                //Zamknij oba pliki
                plikWejsciowy.close();
                plikWyjsciowy.close();
                break;


            case 4:
                czas.czasStart();
                cout << "NIE DZIAŁA" << endl;
                czas.czasStop();
                cout << "Czas wykonania: " << czas.czasWykonania() << "ms" << endl;
                break;
        }
    }
}
