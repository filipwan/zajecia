#include <iostream>
#include "../include/Tablica.h"

using namespace std;

// Konstruktor
Tablica::Tablica() {
    Tablica::wskaznikNaPoczatek = NULL;
    Tablica::rozmiarTablicy = 0;
}

// Destruktor
Tablica::~Tablica() {
    if (wskaznikNaPoczatek != NULL)
        delete[]wskaznikNaPoczatek;
}

void Tablica::dodajNaPoczatek(int wartosc) {
    // Powiększ tablicę o 1 i dodaj wartosc na początek
    int *nowyNaPocz = new int[rozmiarTablicy + 1];
    nowyNaPocz[0] = wartosc;

    // Wpisz dane ze starej tablicy do nowej
    for (int i = 0; i < rozmiarTablicy; i++)
        nowyNaPocz[i + 1] = wskaznikNaPoczatek[i];

    // Zwiększ rozmiar tablicy i zastąp starą tablicę nową
    delete wskaznikNaPoczatek;
    wskaznikNaPoczatek = nowyNaPocz;
    rozmiarTablicy++;
}

void Tablica::dodajNaKoniec(int wartosc) {
    // Stwórz tablicę o 1 większą i przypisz wartosc na koniec
    int *nowyNaPocz = new int[rozmiarTablicy + 1];
    nowyNaPocz[rozmiarTablicy] = wartosc;

    // Wpisz dane ze starej tablicy do nowej
    for (int i = 0; i < rozmiarTablicy; i++)
        nowyNaPocz[i] = wskaznikNaPoczatek[i];

    // Zwiększ rozmiar tablicy i zastąp starą tablicę nową
    delete wskaznikNaPoczatek;
    wskaznikNaPoczatek = nowyNaPocz;
    rozmiarTablicy++;
}

void Tablica::dodajDowolnie(int wartosc, int pozycja) {
    // Sprawdź, czy dana pozycja znajduje się w tablicy
    if (pozycja < 0 || pozycja > rozmiarTablicy)
        cout << "W tablicy nie ma podanej pozycji [" << pozycja << "]!" << endl;
    else{
        // Powiększ tablicę o 1 i dodaj wartosc na wybrane miejsce
        int *nowyNaPocz = new int[rozmiarTablicy + 1];
        nowyNaPocz[pozycja] = wartosc;

        // Przepisz tablice ze starej tablicy, do pozycji i-1
        for (int i = 0; i < pozycja; i++)
            nowyNaPocz[i] = wskaznikNaPoczatek[i];

        // Przepisz dane powyzej i-tej pozycji
        for (int i = pozycja; i < rozmiarTablicy; i++)
            nowyNaPocz[i + 1] = wskaznikNaPoczatek[i];

        // Zwiększ rozmiar tablicy i zastąp starą tablicę nową
        delete wskaznikNaPoczatek;
        wskaznikNaPoczatek = nowyNaPocz;
        rozmiarTablicy++;
    }
}

void Tablica::usunPierwszy() {
    if (rozmiarTablicy > 0) {
        // Stwórz tablicę o n-1 elementach
        int *nowyNaPocz = new int[rozmiarTablicy - 1];

        // Przepisz elementy starej tablicy poza pierwszym
        for (int i = 0; i < rozmiarTablicy - 1; i++)
            nowyNaPocz[i] = wskaznikNaPoczatek[i + 1];

        // Zmniejsz rozmiar tablicy i zastąp starą tablicę nową
        delete[] wskaznikNaPoczatek;
        wskaznikNaPoczatek = nowyNaPocz;
        rozmiarTablicy--;
    } else
        cout << "Pusta tablica!" << endl;
}

void Tablica::usunOstatni() {
    if (rozmiarTablicy > 0) {
        // Stwórz tablicę o n-1 elementach
        int *nowyNaPocz = new int[rozmiarTablicy - 1];

        // Wpisz elementy starej tablicy poza ostatnim
        for (int i = 0; i < rozmiarTablicy - 1; i++)
            nowyNaPocz[i] = wskaznikNaPoczatek[i];

        // Zmniejsz rozmiar tablicy i zastąp starą tablicę nową
        delete[] wskaznikNaPoczatek;
        wskaznikNaPoczatek = nowyNaPocz;
        rozmiarTablicy--;
    } else
        cout << "Pusta tablica!" << endl;
}

void Tablica::usunWybrany(int pozycja) {
    //sprawdz czy wybrana pozycja nalezy do tablicy
    if (rozmiarTablicy > 0) {
        if (rozmiarTablicy > 0 || pozycja > 0 || pozycja < rozmiarTablicy) {
            // Stwórz tablicę o 1 mniej elementach niż poprzednio
            int *nowyNaPocz = new int[rozmiarTablicy - 1];

            // Wpisz elementy do tablicy do wybranej pozycji
            for (int i = 0; i < pozycja; i++)
                nowyNaPocz[i] = wskaznikNaPoczatek[i];

            // Przepisz elementy po wybranej pozycji
            for (int i = pozycja; i < rozmiarTablicy - 1; i++)
                nowyNaPocz[i] = wskaznikNaPoczatek[i + 1];

            // Zmniejsz rozmiar tablicy i zastąp starą tablicę nową
            delete[] wskaznikNaPoczatek;
            wskaznikNaPoczatek = nowyNaPocz;
            rozmiarTablicy--;
        } else
            cout << "W tablicy nie istnieje pozycja [" << pozycja << "]!" << endl;
    } else
        cout<< "Pusta tablica!";
}

bool Tablica::sprawdzCzyIstnieje(int wartosc) {
    // Szukanie wartości w tablicy
    // W razie wystąpienia zwróć true, w przeciwnym razie false
    for (int i = 0; i < rozmiarTablicy; i++)
        if (wskaznikNaPoczatek[i] == wartosc) {
            cout << "Podana wartosc zajmuje [" << i << "] pozycje" << endl;
            return true;
        }
    cout << "Szukanej wartosci nie ma w tablicy" << endl;
    return false;
}

void Tablica::wydrukujTablice() {
    cout << "Tablica:" << endl;
    //Wydrukuj tablice jesli ma jakies elementy
    //w przeciwnym razie wypisz blad
    if (wskaznikNaPoczatek != NULL)
        for (int i = 0; i < rozmiarTablicy; i++)
            cout << "    [" << i << "] " << wskaznikNaPoczatek[i] << endl;
    else
        cout << "Pusta tablica!" << endl;
}
