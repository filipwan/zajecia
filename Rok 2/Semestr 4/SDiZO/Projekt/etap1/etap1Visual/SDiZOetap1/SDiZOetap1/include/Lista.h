#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include "ElementListy.h"

using namespace std;

class Lista {

public:

    int rozmiar;

    Lista();

    ~Lista();

    ElementListy *pierwszyElement;
    ElementListy *ostatniElement;
    //ElementListy *aktualnyElement;

    void dodajNaPoczatek(int wartosc);

    void dodajDowolnie(int wartosc, int pozycja);

    void dodajNaKoniec(int wartosc);

    void wydrukujListe();

    void usunPierwszy();

    void usunOstatni();

    void usunWybrany(int);

    bool sprawdzCzyIstnieje(int wartosc);
};

#endif //LISTA_H
