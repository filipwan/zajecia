#include <iostream>
#include "Czas.h"

#ifndef TESTY_H
#define TESTY_H


class Testy {

public:

    void testTablicy();

    void testListy();

    void testKopca();

    void testDrzewa();
};

#endif // TESTY_H
