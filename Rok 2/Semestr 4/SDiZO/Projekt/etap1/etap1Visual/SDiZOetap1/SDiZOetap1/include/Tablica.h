#ifndef TABLICA_H
#define TABLICA_H


class Tablica {


public:
//Zmienne
    int *wskaznikNaPoczatek;

    int rozmiarTablicy;

//Metody
    Tablica();

    ~Tablica();

    void dodajNaPoczatek(int);

    void dodajNaKoniec(int wartosc);

    void dodajDowolnie(int wartosc, int pozycja);

    void usunOstatni();

    void usunPierwszy();

    void usunWybrany(int pozycja);

    void wydrukujTablice();

    bool sprawdzCzyIstnieje(int wartosc);
};

#endif //TABLICA_H
