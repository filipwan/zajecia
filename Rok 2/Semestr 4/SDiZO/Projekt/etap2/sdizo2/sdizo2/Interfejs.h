#include <fstream>
#include <math.h>
#include <iomanip>
#include <queue>
#include <string>
#include "Krawedz.h"
#include "Stos.h"
#include "Graf.h"

class UI {
public:
    void wyborMetodyGenerowaniaGrafu();

    bool naPoczatek = true;
    int wybor, w, g, b;
    Graf *graf;

    void grafLosowy();

    void wyborFunkcji();

    void grafZPliku();

	void grafLosowyParams(int w, int g);

	void testy(int w, int g);

	std::ofstream fileOut;
};
