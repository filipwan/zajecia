#include "Interfejs.h"

using namespace std;

void UI::wyborMetodyGenerowaniaGrafu() {

    while (naPoczatek) {
		system("cls");
		cout << "|-------------------------------------------|\n"
			<< "| Struktury Danych i Złożoność Obliczeniowa |\n"
			<< "|          Zadanie projektowe nr 2          |\n"
			<< "|                Filip Wanat                |\n"
			<< "|           Numer albumu: 241121            |\n"
			<< "|-------------------------------------------|" << endl << endl;
		cout << "|-------------------------------------------|" << endl
			<< "| Etap 1 - Generowanie grafu:               |" << endl
			<< "|  1. Wygeneruj losowy graf programowo      |" << endl
			<< "|  2. Wygeneruj graf z pliku                |" << endl
			<< "|  3. Testy                                 |" << endl
			<< "|  4. Zakończ                               |" << endl
			<< "|-------------------------------------------|" << endl;
        cin >> wybor;
        switch (wybor) {
            case 1: {
                grafLosowy();
                wyborFunkcji();
            }
                break;
            case 2: {
                grafZPliku();
                wyborFunkcji();
            }
                break;
			case 3: {
				testy(30, 25);
				testy(30, 50);
				testy(30, 75);
				testy(30, 99);

				testy(60, 25);
				testy(60, 50);
				testy(60, 75);
				testy(60, 99);

				testy(90, 25);
				testy(90, 50);
				testy(90, 75);
				testy(90, 99);

				testy(120, 25);
				testy(120, 50);
				testy(120, 75);
				testy(120, 99);

				testy(150, 25);
				testy(150, 50);
				testy(150, 75);
				testy(150, 99);
			}
					break;
			default: {exit(0); }
					 break;
        }
    }
}

void UI::wyborFunkcji() {
    while (!naPoczatek) {

        cout << endl 
			 << "|-------------------------------------------------|" << endl 
			 << "| Etap 2 - Analiza grafu przy pomocy algorytmów:  |" << endl
             << "|  1. Wyświetl macierz incydencji                 |" << endl
			 << "|  2. Wyświetl reprezentację listową              |" << endl
             << "|  3. Algorytm Dijkstry                           |" << endl
             << "|  4. Algorytm Prima                              |" << endl
             << "|  5. Powrót do etapu I                           |" << endl
			 << "|-------------------------------------------------|" << endl;
        cin >> wybor;
        switch (wybor) {
            case 1: {
                graf->wyswietlMacierz();
                cout << endl;
            }
                break;
			case 2: {
				graf->wyswietlListe();
				cout << endl;
			}
					break;
            /*case 3: {
                while (true) {
                    cout << "Zdefiniuj wierzchołek początkowy: ";
                    cin >> b;
                    if (b < graf->wierzcholki) break;
                    else cout << "Graf nie zawiera wierzchołka o tym numerze." << endl;
                }

                while (true) {
                    int wyborReprezentacji;
                    cout << "Jakiej reprezentacji użyć w algorytmie?" << endl << "1. Macierz Incydecji" << endl
                         << "2. Lista poprzedników i następników" << endl;
                    cin >> wyborReprezentacji;
                    if (wyborReprezentacji == 1) {
                        graf->macierz_DFS(b);
                        break;
                    } else if (wyborReprezentacji == 2) {
                        graf->lista_DFS(b);
                        break;
                    }
                }
            }
                break;*/

            case 3: {
                while (true) {
                    cout << "Zdefiniuj wierzchołek początkowy: ";
                    cin >> b;
                    if (b < graf->wierzcholki) break;
                    else cout << "Podaj prawidlowy numer wierzcholka!" << endl;
                }

                while (true) {
                    int wyborReprezentacji;
                    cout << "Jakiej reprezentacji użyć w algorytmie?" << endl << "1. Macierz Incydecji" << endl
                         << "2. Lista poprzedników i następników" << endl;
                    cin >> wyborReprezentacji;
                    if (wyborReprezentacji == 1) {
                        graf->macierz_Dijkstra(b);
                        break;
                    } else if (wyborReprezentacji == 2) {
                        graf->lista_Dijkstra(b);
                        break;
                    }
                }
            }
                break;


            case 4: {
                while (true) {
                    int wyborReprezentacji;
                    cout << "Jakiej reprezentacji użyć w algorytmie?" << endl 
						 << "1. Macierz Incydecji" << endl
                         << "2. Lista poprzedników i następników" << endl;
                    cin >> wyborReprezentacji;
                    if (wyborReprezentacji == 1) {
                        graf->macierz_Prim();
                        break;
                    } else if (wyborReprezentacji == 2) {
                        graf->lista_Prim();
                        break;
                    }
                }

            }
                break;

			default: {
                delete graf;
                naPoczatek = true;
            }
                break;
        }
    }
}

void UI::grafLosowy() {
    naPoczatek = false;
    while (true) {
        cout << "Zdefiniuj ilość wierzchołków grafu: ";
        cin >> w;
        if (w > 1) break;
        else cout << "Liczba wierzchołków nie może być mniejsza lub równa 1." << endl;
    }


    int maxK = w * (w - 1);
    double minG = ceil((((double) w - 1) * 100) / (double) maxK);

    while (true) {

        cout << "Zdefiniuj gęstość grafu (minimum " << minG << "%): ";
        cin >> g;
        if (g < minG || g > 100)
            cout << "Podana gęstość nie pozwala na stworzenie grafu." << endl;
        else
            break;
    }
    double krawedzie = ceil((maxK * g) / 100);
    graf = new Graf(w, krawedzie);
    graf->losujGraf();
}

void UI::grafZPliku() {
    naPoczatek = false;
    ElementListy *e1;
    string s, nazwa;
    int a = 0;
    int krawedzie, wierzcholki;
    cout << "Nazwa pliku (bez rozszerzenia!): ";
    cin >> nazwa;
    nazwa = nazwa + ".txt";
    ifstream plik(nazwa);
    if (!plik) {
        cout << "Nie udało się otworzyć pliku, spróbuj ponownie." << endl;
        naPoczatek = true;

    } else {

        {
            plik >> krawedzie >> wierzcholki;
            if (!plik || krawedzie < wierzcholki - 1 || wierzcholki <= 1 ||
                krawedzie > (wierzcholki * (wierzcholki - 1))) {
                cout << "Plik jest uszkodzony, lub dane w nim są w złym formacie." << endl;

                naPoczatek = true;
            } else {
                graf = new Graf(wierzcholki, krawedzie);
                while (!plik.eof()) {

                    plik >> graf->K[a].wp >> graf->K[a].wk >> graf->K[a].waga;
                    if (graf->K[a].wp >= wierzcholki || graf->K[a].wk >= wierzcholki ||
                        graf->K[a].waga < 1) {
                        cout << "Krawędzie w pliku są nieprawidłowo określone." << endl;
                        naPoczatek = true;
                        break;
                    }

                    if (plik) {
                        a++;
                    } else {
                        cout << "Krawędzie w pliku są nieprawidłowo określone." << endl;
                        naPoczatek = true;
                        break;
                    }
                }
                if (a == krawedzie) {
                    for (int i = 0; i < wierzcholki; i++)
                        graf->macierzIncydencji[i] = new int[krawedzie];
                    for (int i = 0; i < wierzcholki; i++)
                        for (int j = 0; j < krawedzie; j++) {
                            graf->macierzIncydencji[i][j] = 0;
                        }

                    for (int i = 0; i < wierzcholki; i++)
                        graf->listySasiedztwa[i] = NULL;

                    for (int i = 0; i < krawedzie; i++) {
                        int wp = graf->K[i].wp;
                        int wk = graf->K[i].wk;
                        e1 = new ElementListy;
                        e1->w = wk;
                        e1->waga = graf->K[i].waga;
                        e1->nastepny = graf->listySasiedztwa[wp];
                        graf->listySasiedztwa[wp] = e1;
                        graf->macierzIncydencji[wp][i] = 1;
                        graf->macierzIncydencji[wk][i] = -1;
                    }
                    plik.close();
                    graf->zamienGrafNaNieskierowany();
                    if (!(graf->sprawdzSpojnosc())) {
                        cout << "Zawarty w pliku graf jest niespójny, nie można wykonać algorytmów!" << endl;
                        naPoczatek = true;
                        delete graf;
                    } else {
                        wyborFunkcji();
                    }

                } else if (!naPoczatek) {
                    naPoczatek = true;
                    cout << "W pliku brakuje danych dotyczących krawędzi grafu." << endl;
                }
            }
        }
    }
}

void UI::grafLosowyParams(int w, int g)
{
	int maxK = w * (w - 1);
	double krawedzie = ceil((maxK * g) / 100);
	graf = new Graf(w, krawedzie);
	graf->losujGraf();
}

void UI::testy(int w, int g)
{
	unsigned long long sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0, sum5 = 0, sum6 = 0;
	unsigned long long petle = 100;
	string nameOfFileOut = "wyniki\\result" + to_string(w) + ".txt";
	fileOut.open(nameOfFileOut, fstream::out | fstream::app);

	if (fileOut.is_open())
	{
		cout << "Otwarto plik: " << nameOfFileOut << endl;
	}
	else
	{
		cout << "Błąd otwarcia pliku" << endl;
		return;
	}

	for (int i = 0; i < petle; i++)
	{
		grafLosowyParams(w, g);
		sum1 += graf->macierz_DFS_T(0);
		sum2 += graf->lista_DFS_T(0);
		sum3 += graf->macierz_Dijkstra_T(0);
		sum4 += graf->lista_Dijkstra_T(0);
		sum5 += graf->macierz_Prim_T();
		sum6 += graf->lista_Prim_T();
		cout << i << endl;
		delete(graf);
	}
	fileOut << endl;
	fileOut << "macierz_DFS(" + to_string(g) + "): " << sum1 / petle << endl;
	fileOut << "lista_DFS(" + to_string(g) + "): " << sum2 / petle << endl;
	fileOut << "macierz_Dijkstra(" + to_string(g) + "): " << sum3 / petle << endl;
	fileOut << "lista_Dijkstra(" + to_string(g) + "): " << sum4 / petle << endl;
	fileOut << "macierz_Prim(" + to_string(g) + "): " << sum5 / petle << endl;
	fileOut << "lista_Prim(" + to_string(g) + "): " << sum6 / petle << endl;
	fileOut.close();
}