#include <iostream>

using namespace std;

int main()
{
	int tab[100] = {0};
	int wprowadzona, i;
	
	for(i = 0; i < 100; i++)
	{
		cout << "Wprowadz czas: ";
		cin >> wprowadzona;
		if(!wprowadzona) break;
		
		tab[i] = wprowadzona;
		
		for(int j = i; j > 0; j--)
			if(tab[j-1] > tab[j])
			{
				tab[j] = tab[j-1];
				tab[j-1] = wprowadzona;
			}else break;
			
		cout << "Tablica czasow: ";
		for(int j = 0; j <= i; j++)
			cout << tab[j] << " ";
		cout << "\nIlosc czasow: " << i+1 << "\n\n";
	}
	
	return 0;
}
