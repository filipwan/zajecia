#include <iostream>

using namespace std;

void Przesun(int tab[], int n);

int main()
{
	int n;
	cin >> n;
	int tab[n];
	for(int i = 0; i < n; i++)
		cin >> tab[i];
	
	Przesun(tab, n);
	
	cout << endl;
	
	for(int i = 0; i < n; i++)
		cout << tab[i] << " ";
	
	return 0;
}

void Przesun(int tab[], int n)
{
	int temp = tab[n-1];
	
	for(int i = 1; i < n; i++)
		tab[n-i] = tab[n-i-1];
	
	tab[0] = temp;
}
