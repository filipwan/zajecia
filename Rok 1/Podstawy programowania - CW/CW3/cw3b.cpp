#include <iostream>

using namespace std;

int main()
{
	double tab[10];
	bool czypowt = 0, czysym = 1, czyros = 0, czymal = 0;
	
	for(int i = 0; i < 10; i++)
	{
		cin >> tab[i];
		
		if(!czypowt)
			for(int j = 0; j < i; j++)
				if(tab[i] == tab[j])
					czypowt = 1;
		
		
		if(i >= 5 && czysym)
			if(tab[i] != tab[9-i])
				czysym = 0;
		
		
		if(i>0 && !czyros && tab[i] > tab[i-1])
			czyros = 1;
		else if(i>0 && !czymal &&tab[i] < tab[i-1])
			czymal = 1;
	}
	
	if(czypowt)
		cout << "Co najmniej jedna liczba sie powtorzyla. \n";
	else
		cout << "Zadna liczba sie nie powtorzyla. \n";
	
	if(czysym)
		cout << "Elementy tablicy maja symetryczna zawartosc. \n";
	else 
		cout << "Elementy tablicy nie maja symetrycznej zawartosci. \n";
	
	
	if(czyros && czymal)
		cout << "Te liczby nie sa uporzadkowane ani malejaco, ani rosnaco. \n";
	else if(czyros && !czymal)
		cout << "Te liczby sa uporzadkowane rosnaco. \n";
	else if(!czyros && czymal)
		cout << "Te liczby sa uporzadkowane malejaco. \n";
	else
		cout << "Wszystkie podane liczby sa rowne. \n";
	
	return 0;
}


