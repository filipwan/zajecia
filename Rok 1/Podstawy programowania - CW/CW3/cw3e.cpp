#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
	int tab[26] = {0};
	
	for(int k = getch(); k != 27; k = getch())
	{
		cout << (char)k;
		if(k >= 'A' && k <= 'Z')
			tab[k-'A']++;
		else if(k >= 'a' && k <= 'z')
			tab[k-'a']++;
	}
	cout << "\n\n";
	for(int i = 0, j = tab[0]; i < 26; i++, j = tab[i])
	{
		cout << (char)('A'+i);
		cout.width(3); cout << right << tab[i] << " ";
		while(j--)
			cout << "#";
		cout << endl;
	}
	
	return 0;
}


