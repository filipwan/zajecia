#include <iostream>

using namespace std;

// Mo�na

void Pisz0()
{
	cout << "Cos0\n";
}

void Pisz1()
{
	cout << "Cos1\n";
}

void Pisz2()
{
	cout << "Cos2\n";
}

int main()
{
	void (*a[3])();
	a[0] = &Pisz0;
	a[1] = &Pisz1;
	a[2] = &Pisz2;
	
	a[0]();
	a[1]();
	a[2]();
	
	return 0;
}


