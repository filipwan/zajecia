#include <iostream>

using namespace std;

// Mo�na zdefiniowa� dwie funkcje o takiej samej nazwie, ale tylko w C++
// Wykonuje si� przeci��enie funkcji. (R�ny zestaw zmiennych wej�ciowych)

void WriteData(int data);

void WriteData(string data);

void WriteData(bool data);

int main()
{
	int dataInt32 = 32;
	string dataString = "To jest tekst.";
	bool dataBoolean = 1;
	
	WriteData(dataInt32);
	WriteData(dataString);
	WriteData(dataBoolean);
	
	return 0;
}

void WriteData(int data)
{
	cout << data << endl;
}

void WriteData(string data)
{
	cout << data << endl;
}

void WriteData(bool data)
{
	if(data)
		cout << "Tak" << endl;
	else
		cout << "Nie" << endl;
}
