#include <iostream>

using namespace std;

//Należy zastosować przeciążenie funkcji

void WriteData(int data);

void WriteData(string data);

void WriteData(bool data);

int main()
{
	int dataInt32 = 32;
	string dataString = "To jest tekst.";
	bool dataBoolean = 1;
	
	WriteData(dataInt32);
	WriteData(dataString);
	WriteData(dataBoolean);
	
	return 0;
}

void WriteData(int data)
{
	cout << data << endl;
}

void WriteData(string data)
{
	cout << data << endl;
}

void WriteData(bool data)
{
	if(data)
		cout << "Tak" << endl;
	else
		cout << "Nie" << endl;
}
