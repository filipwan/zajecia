#include <iostream>

using namespace std;

// Return(0) wychodzi z funkcji, a exit(0) wychodzi z programu

int UseReturn()
{
	return 0;
}

int UseExit()
{
	exit(0);
}

int main()
{
	cout << "Uzyj Return\n";
	UseReturn();
	cout << "Uzyj Exit\n";
	UseExit();
	cout << "Test";
	
	return 0;
}


