#include <iostream>

using namespace std;

// prototyp - wymagana nazwa, typy zmiennych zwracanych i typy argumentow (mozna pominac nazwy argumentow)
void Cos(int, int);


int main()
{
	// wywo�anie
	Cos(2, 3);
	
	return 0;
}

// definicja
void Cos(int kappa, int gamma)
{
	cout << "Cos " << kappa;
}
