#include <iostream>

using namespace std;

int main()
{
	// umieszcza zmienn� w rejestsze w celu szybkiego dostepu
	register int a = 2;
	
	// definiuje zmienn� jak zmienn� globaln� i nie usuwa jej po wyjsciu z funkcji
	static int b = 3;
	
	return 0;
}


