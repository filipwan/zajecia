#include <iostream>

using namespace std;

// Przekazywanie tablicy
// Ma dostep do oryginalnej tablicy

void PrzezTab(int tab[])
{
	tab[0] = 2;
	tab[1] = 3;
}

int main()
{
	int data4[2] = {1, 1};
	
	cout << "Dane przed wykonaniem funkcji: " << data4[0] << " " << data4[1] << endl;
	PrzezTab(data4);
	cout << "Dane po wykonaniu funkcji: " << data4[0] << " " << data4[1] << endl;
	
	return 0;
}


