#include <iostream>

using namespace std;

// Przekazywanie przez warto��:
// (nie ma dostepu do oryginalnej zmiennej)
// najbezpieczniejszy
// najprostszy

int PrzezWartosc(int data)
{
	data = 2;
	return data;
}

// Przekazywanie przez wska�nik:
// (pracuje na oryginalnej zmiennej wskazywanej przez wskaznik,
// ale nie ma dostepu do oryginalnego wskaznika)

void PrzezWskaznik(int *data)
{
	*data = 2;
}

// Przekazywanie przez referencje:
// Ma dostep do oryginalnej zmiennej
// Najwygodniejszy
// Najszybszy

void PrzezReferencje(int &data)
{
	data = 2;
}



int main()
{
	int data1 = 1;
	int data2 = 1;
	int *wskData2 = &data2;
	int data3 = 1;
	
	cout << "Dane przed wykonaniem funkcji: " << data1 << endl;
	cout << "Funkcja zwraca: " << PrzezWartosc(data1) << endl;
	cout << "Dane po wykonaniu funkcji: " << data1 << endl;
	
	cout << endl;
	
	cout << "Dane przed wykonaniem funkcji: " << data2 << endl;
	PrzezWskaznik(wskData2);
	cout << "Dane po wykonaniu funkcji: " << data2 << endl;
	
	cout << endl;
	
	cout << "Dane przed wykonaniem funkcji: " << data3 << endl;
	PrzezReferencje(data3);
	cout << "Dane po wykonaniu funkcji: " << data3 << endl;
	
	
	
	return 0;
}


