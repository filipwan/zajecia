#include <iostream>

using namespace std;

int main()
{
	int n, imin = 0, last = 0, suma = 0;
	bool end = true, first = true;
	do
	{
		cin >> n;
		
		if(first)
		{
			last = n;
			first = false;
		}else
		{
			if(n == last)
				end = false;
			last = n;
		}
		
		suma += n;
		if(suma > 100)
			end = false;
		
		if(n < 0)
		{
			imin++;
			if(imin > 10)
				end = false;
		}
		
	}while(end);
	
	return 0;
}


