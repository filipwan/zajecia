#include <iostream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	if(n < 0) n = -n;
	if(n != 0)
		for(int i = 1; i < 101; i++)
			cout << i << ".\t" << i * n << endl;
	
	return 0;
}


