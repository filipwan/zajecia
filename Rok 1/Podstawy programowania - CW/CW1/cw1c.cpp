#include <iostream>

using namespace std;

int main()
{
	string dane[2][5];
	int roznica[5];
	
	for(int i = 0; i < 2; i++)
	{
		cout << "Podaj nazwisko pracownika nr " << i+1 << ": ";
		getline(cin, dane[i][0]);
		cout << "Podaj imie pracownika nr " << i+1 << ": ";
		getline(cin, dane[i][1]);
		cout << "Podaj date urodzenia pracownika nr " << i+1 << ": ";
		getline(cin, dane[i][2]);
		cout << "Podaj ilosc stazu pracownika nr " << i+1 << ": ";
		getline(cin, dane[i][3]);
		cout << "Podaj stawke godzinowa pracownika nr " << i+1 << ": ";
		getline(cin, dane[i][4]);
		cout << endl;
	}
	
	for(int i = 0; i < 5; i++)
	{
		roznica[i] = dane[0][i].length() - dane[1][i].length();
	}
	
	for(int i = 0; i < 2; i++)
	{
		cout << "\nNazwisko: " << dane[i][0];
		int j = roznica[0];
		while(j < 0 && i == 0)
		{
			cout << " ";
			j++;
		}
		while(j > 0 && i == 1)
		{
			cout << " ";
			j--;
		}
		
		cout << "  Imie: " << dane[i][1];
		j = roznica[1];
		while(j < 0 && i == 0)
		{
			cout << " ";
			j++;
		}
		while(j > 0 && i == 1)
		{
			cout << " ";
			j--;
		}
		
		cout << "  Urodzony: " << dane[i][2];
		j = roznica[2];
		while(j < 0 && i == 0)
		{
			cout << " ";
			j++;
		}
		while(j > 0 && i == 1)
		{
			cout << " ";
			j--;
		}
		
		cout << "  Staz: " << dane[i][3];
		j = roznica[3];
		while(j < 0 && i == 0)
		{
			cout << " ";
			j++;
		}
		while(j > 0 && i == 1)
		{
			cout << " ";
			j--;
		}
		
		cout << "  Stawka: " << dane[i][4];
		j = roznica[4];
		while(j < 0 && i == 0)
		{
			cout << " ";
			j++;
		}
		while(j > 0 && i == 1)
		{
			cout << " ";
			j--;
		}
	}
	
	return 0;
}


