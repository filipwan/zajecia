#include <iostream>

using namespace std;

string DeleteUnnecesarySpaces(string tekst)
{
	int firstSpaces = -1;
	int lastSpaces = -1;
	
	for(int i = 0; i < tekst.length(); i++)
		if(tekst[i] == ' ')
			firstSpaces = i;
		else
			break;
	
	if(firstSpaces != -1)
		tekst.erase(0, firstSpaces+1);
	
	for(int i = tekst.length() - 1; i >= 0; i--)
		if(tekst[i] == ' ')
			lastSpaces = i;
		else
			break;
	
	if(lastSpaces != -1)
		tekst.erase(lastSpaces, tekst.length()-lastSpaces+1);
		
	return tekst;
}

int main()
{
	string tekst;
	getline(cin, tekst);
	
	tekst = DeleteUnnecesarySpaces(tekst);
	
	cout << "\n#" << tekst <<"#";
	
	return 0;
}


