#include <iostream>

using namespace std;

int ZsumujLiczbyWStringu(string tekst);

int main()
{
	string tekst;
	getline(cin, tekst);
	cout << endl;
	
	int sumaLiczb = ZsumujLiczbyWStringu(tekst);
	
	cout << "\nSuma podanych liczb to " << sumaLiczb;
	
	return 0;
}

int ZsumujLiczbyWStringu(string tekst)
{
	int sumaLiczb = 0;
	int currentNumber = 0;
	int currentSign = 1;
	bool isLastSpace = true;
	
	for(int i = 0; i < tekst.length(); i++)
	{
		if(tekst[i] == '-')
			currentSign *= -1;	
		else if(tekst[i] >= '0' && tekst[i] <= '9')
		{
			currentNumber *= 10;
			currentNumber += tekst[i] - '0';
			isLastSpace = false;
		}
		if(isLastSpace == false && (!(tekst[i] >= '0' && tekst[i] <= '9') || i+1 == tekst.length()))
		{
			isLastSpace = true;
			currentNumber *= currentSign;
			currentSign = 1;
			cout << currentNumber << " ";
			sumaLiczb += currentNumber;
			currentNumber = 0;
		}
	}
	return sumaLiczb;
}





