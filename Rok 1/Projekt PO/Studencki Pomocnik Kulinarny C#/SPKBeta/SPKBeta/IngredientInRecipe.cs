﻿namespace SPKBeta
{
    public class IngredientInRecipe
    {
        public Ingredient Ingredient { get; set; }
        public double Ammount { get; set; }
        public Unit Unit { get; set; }
    }
}
