﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace SPKBeta
{
    public static class RecipeParser
    {
  /*
        /// <summary>
        /// Parsuje listę linków do przepisów
        /// </summary>
        /// <param name="recipeList"></param>
        public static void ParseListOfRecipeLinks(List<Recipe> recipeList)
        {
            for (int i = 0; i < 90; i++)
            {
                try
                {
                    HtmlWeb web = new HtmlWeb();
                    var htmlDoc = web.Load("https://www.kwestiasmaku.com/home-przepisy?page=" + i);
                    var rowList = htmlDoc.DocumentNode.Descendants().Where
                    (x => x.Id == "views-bootstrap-grid-1").ToList();

                    //recipeList.Clear();

                    foreach (var item in rowList)
                    {
                        var recipes = item.Descendants("div").ToList();
                        foreach (var recipe in recipes)
                        {
                            try
                            {
                                string url = "https://www.kwestiasmaku.com" + recipe.Descendants("a").ToList()[0].GetAttributeValue("href", null);
                                if (recipeList.Count == 0 || url != recipeList.Last().OriginalUrl)
                                {
                                    string originalUrl = "https://www.kwestiasmaku.com" + recipe.Descendants("a").ToList()[0].GetAttributeValue("href", null);
                                    string photoUrl = recipe.Descendants("img").ToList()[0].GetAttributeValue("src", null);
                                    recipeList.Add
                                    (
                                        new Recipe(originalUrl, photoUrl)
                                    );
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show(@"Couldn't connect");
                }
            }
        }

        public static void ParseRecipe(Recipe recipe)
        {
            try
            {
                HtmlWeb web = new HtmlWeb();
                var htmlDoc = web.Load(recipe.OriginalUrl);
                var listOfIngredients = htmlDoc.DocumentNode.Descendants().Where
                (x => (x.Name == "div" && x.Attributes["class"] != null &&
                       x.Attributes["class"].Value == "field field-name-field-skladniki field-type-text-long field-label-hidden")).ToList();

                recipe.Ingredients = new List<string>();
                var list = listOfIngredients[0].SelectNodes("ul").Nodes().Where(x => x.Name == "li");
                foreach (var li in list)
                {
                    recipe.Ingredients.Add(li.InnerText);
                }
            }
            catch (Exception)
            {
            }
        }*/
    }
}
