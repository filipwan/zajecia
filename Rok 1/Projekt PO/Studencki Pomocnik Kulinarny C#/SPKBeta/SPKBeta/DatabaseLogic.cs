﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SPKBeta
{
    internal static class DatabaseLogic
    {
        private const string XmlFileNameRecipes = @"XMLFiles\XMLDatabaseRecipes.xml";
        private const string XmlFileNameIngredients = @"XMLFiles\XMLDatabaseIngredients.xml";
        private static XDocument xmlRecipes;
        private static XDocument xmlIngredients;

        /// <summary>
        /// Inicjalizuje bazę xml
        /// </summary>
        /// <param name="recipes"></param>
        /// <param name="ingredients"></param>
        public static void InitializeXml(ref List<Recipe> recipes, ref List<Ingredient> ingredients)
        {
            try
            {
                ingredients = LoadIngredientList();
                recipes = LoadRecipeList();
            }
            catch (Exception)
            {
                Directory.CreateDirectory("XMLFiles");
                xmlRecipes = new XDocument();
                xmlIngredients = new XDocument();
                UpdateDatabase(ingredients);
                UpdateDatabase(recipes);
            }
        }

        /// <summary>
        /// Funkcja zwracająca listę wszystkich składników z bazy danych
        /// </summary>
        /// <returns></returns>
        public static List<Ingredient> LoadIngredientList()
        {
            xmlIngredients = XDocument.Load(XmlFileNameIngredients);
            return DeserializeParams<Ingredient>(xmlIngredients);
        }

        /// <summary>
        /// Funkcja zwracająca listę wszystkich przepisów z bazy danych
        /// </summary>
        /// <returns></returns>
        public static List<Recipe> LoadRecipeList()
        {
            xmlRecipes = XDocument.Load(XmlFileNameRecipes);
            return DeserializeParams<Recipe>(xmlRecipes);
        }

        /// <summary>
        /// Deserializuje XDocument na listę
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="doc"></param>
        /// <returns></returns>
        private static List<T> DeserializeParams<T>(XDocument doc)
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            XmlReader reader = doc.CreateReader();
            var result = (List<T>)serializer.Deserialize(reader);
            reader.Close();
            return result;
        }

        /// <summary>
        /// Funkcja aktualizująca listę składników w bazie
        /// </summary>
        /// <param name="ingredients"></param>
        public static void UpdateDatabase(List<Ingredient> ingredients)
        {
            xmlIngredients = SerializeParams(xmlIngredients, ingredients);
            xmlIngredients.Save(XmlFileNameIngredients);
        }

        /// <summary>
        /// Funkcja aktualizująca listę przepisów w bazie
        /// </summary>
        /// <param name="recipes"></param>
        public static void UpdateDatabase(List<Recipe> recipes)
        {
            xmlRecipes = SerializeParams(xmlRecipes, recipes);
            xmlRecipes.Save(XmlFileNameRecipes);
        }

        /// <summary>
        /// Serializuje listę na XDocument
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="doc"></param>
        /// <param name="paramList"></param>
        private static XDocument SerializeParams<T>(XDocument doc, List<T> paramList)
        {
            doc = new XDocument();
            var serializer = new XmlSerializer(paramList.GetType());
            var writer = doc.CreateWriter();
            serializer.Serialize(writer, paramList);
            writer.Close();
            return doc;
        }

        /// <summary>
        /// Funkcja dodaje nowy przepis do bazy
        /// </summary>
        /// <param name="recipe"></param>
        /// <param name="recipes"></param>
        public static void AddNewRecipe(Recipe recipe, ref List<Recipe> recipes)
        {
            if (recipes.Count == 0)
            {
                recipe.Id = 0;

            }
            else
            {
                recipe.Id = recipes.Max(x => x.Id) + 1;
            }
            recipes.Add(recipe);
            UpdateDatabase(recipes);
        }

        /// <summary>
        /// Funkcja dodaje nowy składnik do bazy
        /// </summary>
        /// <param name="ingredient"></param>
        /// <param name="ingredients"></param>
        public static void AddNewIngredient(Ingredient ingredient, ref List<Ingredient> ingredients)
        {
            ingredient.Id = ingredients.Count == 0 ? 0 : (ingredients.Max(x => x.Id) + 1);
            ingredients.Add(ingredient);
            UpdateDatabase(ingredients);
        }
    }
}
