﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SPKBeta
{
    public partial class FormMain : Form
    {
        private List<Ingredient> ingredientList;
        private List<Recipe> recipeList;

        #region UnitList

        private readonly List<Unit> unitList = new List<Unit>
        {
            new UnitLiquid
            {
                Id = 0,
                Name = "Mililitry",
                UnitInMililiters = 1
            },
            new UnitLiquid
            {
                Id = 1,
                Name = "Litry",
                UnitInMililiters = 1000
            },
            new UnitLiquid
            {
                Id = 2,
                Name = "Szklanka",
                UnitInMililiters = 250
            },
            new UnitLiquid
            {
                Id = 3,
                Name = "Decylitry",
                UnitInMililiters = 1
            },
            new UnitLiquid
            {
                Id = 4,
                Name = "Łyżka",
                UnitInMililiters = 15
            },
            new UnitLiquid
            {
                Id = 5,
                Name = "Łyżeczka",
                UnitInMililiters = 5
            },
            new UnitSolid
            {
                Id = 6,
                Name = "Gram",
                UnitInGrams = 1
            },
            new UnitSolid
            {
                Id = 7,
                Name = "Dekagram",
                UnitInGrams = 10
            },
            new UnitSolid
            {
                Id = 8,
                Name = "Kilogram",
                UnitInGrams = 1000
            },
            new UnitIncalculable
            {
                Id = 9,
                Name = "Sztuka"
            },
            new UnitIncalculable
            {
                Id = 10,
                Name = "Szklanka"
            },
            new UnitIncalculable
            {
                Id = 11,
                Name = "Łyżka"
            },
            new UnitIncalculable
            {
                Id = 12,
                Name = "Łyżeczka"
            }
        };

        #endregion UnitList

        private List<IngredientInRecipe> newRecipeIngredients;
        protected List<Panel> OwnedIngredientPanels { get; set; }
        protected List<Panel> RecipeSearchResultPanels { get; set; }
        protected List<Panel> NewRecipeIngredientPanels { get; set; }
        protected List<Panel> RecipeIngredientPanels { get; set; }
        protected List<Panel> IngredientsPanel { get; set; }

        private const int PanelsMargin = 6;

        public FormMain()
        {
            InitializeComponent();

            recipeList = new List<Recipe>();
            ingredientList = new List<Ingredient>();

            DatabaseLogic.InitializeXml(ref recipeList, ref ingredientList);

            LoadIngredientsToFridge(ref ingredientList);
            LoadSearchResultsToRecipes(recipeList);
            LoadIngredients();

            HideTabControlHeader(tabControlMain);
        }

        /// <summary>
        /// Ukrywa pasek danego TabControl
        /// </summary>
        private static void HideTabControlHeader(TabControl tabControl)
        {
            tabControl.ItemSize = new Size(0, 1);
            tabControl.SizeMode = TabSizeMode.Fixed;
            tabControl.Appearance = TabAppearance.FlatButtons;
        }
        
        /// <summary>
        /// Zmienia kartę na lodówkę
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFridge_Click(object sender, EventArgs e)
        {
            tabControlMain.SelectedTab = tabPageFridge;
            LoadIngredientsToFridge(ref ingredientList);
        }

        /// <summary>
        /// Zmienia kartę na przepisy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRecipes_Click(object sender, EventArgs e)
        {
            LoadSearchResultsToRecipes(recipeList);
            tabControlMain.SelectedTab = tabPageRecipes;
        }

        /// <summary>
        /// Zmienia kartę na składniki
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIngredients_Click(object sender, EventArgs e)
        {
            LoadIngredients();
            tabControlMain.SelectedTab = tabPageIngredients;
        }

        #region Fridge

        /// <summary>
        /// Wczytuje posiadane składniki do poszczególnych pól w lodówce
        /// </summary>
        /// <param name="ingredients"></param>
        private void LoadIngredientsToFridge(ref List<Ingredient> ingredients)
        {
            if (OwnedIngredientPanels != null)
            {
                foreach (var item in OwnedIngredientPanels)
                {
                    item.Dispose();
                }
                OwnedIngredientPanels = null;
            }
            comboBoxOwnedIngredientAdd.DataSource = ingredients;
            comboBoxOwnedIngredientAdd.DisplayMember = "Name";
            if(ingredients.Count > 1)
                comboBoxOwnedIngredientAdd.SelectedIndex = 0;
            comboBoxOwnedIngredientAddUnit.DataSource = unitList;
            comboBoxOwnedIngredientAddUnit.DisplayMember = "Name";
            foreach (var ingredient in ingredients)
            {
                if (ingredient.UnitsOwned != 0)
                {
                    var panel = CreateNewOwnedIngredientPanel();
                    panel.Tag = ingredient.Id.ToString();
                    panel.Controls.OfType<Control>().FirstOrDefault(x => x.Tag == labelOwnedIngredient.Tag).Text =
                        ingredient.Name;
                    panel.Controls.OfType<Control>().FirstOrDefault(x => x.Tag == textBoxOwnedIngredientUnitAmmount.Tag).Text
                        = ingredient.UnitsOwned.ToString();
                    List<Unit> u = unitList.Where(x => x.GetType() == ingredient.BaseUnit.GetType()).ToList();
                    panel.Controls.OfType<ComboBox>().FirstOrDefault(x => x.Tag == comboBoxOwnedIngredientUnit.Tag)
                        .DataSource = u;
                    panel.Controls.OfType<ComboBox>().FirstOrDefault(x => x.Tag == comboBoxOwnedIngredientUnit.Tag)
                        .Text = ingredient.BaseUnit.Name;
                }
            }
        }

        /// <summary>
        /// Dodaj pusty panel z posiadanym składnikiem do lodówki
        /// </summary>
        /// <returns></returns>
        private Panel CreateNewOwnedIngredientPanel()
        {
            var panel = new Panel
            {
                BorderStyle = panelOwnedIngredient.BorderStyle,
                Location = panelOwnedIngredient.Location,
                Size = panelOwnedIngredient.Size,
                Visible = true
            };

            tabPageFridge.Controls.Add(panel);
            
            panel.Controls.Add(new Label
            {
                Text = labelOwnedIngredient.Text,
                Font = labelOwnedIngredient.Font,
                Location = labelOwnedIngredient.Location,
                Size = labelOwnedIngredient.Size,
                Tag = labelOwnedIngredient.Tag
            });
            var textbox = new TextBox
            {
                Text = textBoxOwnedIngredientUnitAmmount.Text,
                Font = textBoxOwnedIngredientUnitAmmount.Font,
                Location = textBoxOwnedIngredientUnitAmmount.Location,
                Size = textBoxOwnedIngredientUnitAmmount.Size,
                Tag = textBoxOwnedIngredientUnitAmmount.Tag
            };
            textbox.TextChanged += textBoxOwnedIngredientUnit_TextChanged;
            panel.Controls.Add(textbox);
            panel.Controls.Add(new Label
            {
                Text = labelOwnedIngredientUnit.Text,
                Font = labelOwnedIngredientUnit.Font,
                Location = labelOwnedIngredientUnit.Location,
                Size = labelOwnedIngredientUnit.Size,
                Tag = labelOwnedIngredientUnit.Tag
            });
            var combobox = new ComboBox
            {
                DataSource = new List<Unit>(unitList),
                DisplayMember = "Name",
                DropDownStyle = comboBoxOwnedIngredientUnit.DropDownStyle,
                Text = comboBoxOwnedIngredientUnit.Text,
                Font = comboBoxOwnedIngredientUnit.Font,
                Location = comboBoxOwnedIngredientUnit.Location,
                Size = comboBoxOwnedIngredientUnit.Size,
                Tag = comboBoxOwnedIngredientUnit.Tag
            };
            //combobox.SelectedIndexChanged += TODO combobox change method
            panel.Controls.Add(combobox);
            var button = new Button
            {
                Text = buttonOwnedIngredientDelete.Text,
                Font = buttonOwnedIngredientDelete.Font,
                Location = buttonOwnedIngredientDelete.Location,
                Tag = buttonOwnedIngredientDelete.Tag,
                Size = buttonOwnedIngredientDelete.Size,
                BackColor = buttonOwnedIngredientDelete.BackColor,
                Cursor = buttonOwnedIngredientDelete.Cursor,
                UseVisualStyleBackColor = buttonOwnedIngredientDelete.UseVisualStyleBackColor,
                AutoSize = true
            };
            button.Click += buttonOwnedIngredientDelete_Click;
            panel.Controls.Add(button);

            if (OwnedIngredientPanels == null)
            {
                OwnedIngredientPanels = new List<Panel>();
            }
            else
            {
                var point = OwnedIngredientPanels[OwnedIngredientPanels.Count - 1].Location;
                point.Y += OwnedIngredientPanels[OwnedIngredientPanels.Count - 1].Height + PanelsMargin;
                panel.Location = point;
            }

            OwnedIngredientPanels.Add(panel);

            return panel;
        }

        /// <summary>
        /// Dodaje podaną ilość wybranych składników
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOwnedIngredientAdd_Click(object sender, EventArgs e)
        {
            if (comboBoxOwnedIngredientAdd.SelectedItem == null || (comboBoxOwnedIngredientAdd.SelectedItem as Ingredient).Name != comboBoxOwnedIngredientAdd.Text)
            {
                double textBoxUnitsOwned = 0;
                try
                {
                    textBoxUnitsOwned = textBoxOwnedIngredientAddUnitAmmount.Text == String.Empty ? 0 : Convert.ToDouble(textBoxOwnedIngredientAddUnitAmmount.Text);
                }
                catch (Exception)
                {
                }
                DatabaseLogic.AddNewIngredient(new Ingredient
                {
                    Name = comboBoxOwnedIngredientAdd.Text,
                    UnitsOwned = textBoxUnitsOwned,
                    BaseUnit = comboBoxOwnedIngredientAddUnit.SelectedItem as Unit
                }, ref ingredientList);

                LoadIngredientsToFridge(ref ingredientList);
            }
            else if (comboBoxOwnedIngredientAdd.SelectedItem != null)
            {
                var id = (comboBoxOwnedIngredientAdd.SelectedItem as Ingredient).Id;
                double textBoxUnitsOwned = 0;
                try
                {
                    textBoxUnitsOwned = textBoxOwnedIngredientAddUnitAmmount.Text == String.Empty ? 0 : Convert.ToDouble(textBoxOwnedIngredientAddUnitAmmount.Text);
                }
                catch (Exception) { }

                var newAmmountOfUnitsOwned = textBoxUnitsOwned + (comboBoxOwnedIngredientAdd.SelectedItem as Ingredient).UnitsOwned;

                ingredientList.Find(x => x.Id == id).UnitsOwned = newAmmountOfUnitsOwned;
                ingredientList.Find(x => x.Id == id).BaseUnit = comboBoxOwnedIngredientAddUnit.SelectedItem as Unit;
                DatabaseLogic.UpdateDatabase(ingredientList);

                LoadIngredientsToFridge(ref ingredientList);
            }
        }

        /// <summary>
        /// Zmienia listę możliwych jednostek w zależności od wybranego składnika
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxOwnedIngredientAdd_TextChanged(object sender, EventArgs e)
        {
            if (comboBoxOwnedIngredientAdd.SelectedItem != null &&
                (comboBoxOwnedIngredientAdd.SelectedItem as Ingredient).Name == comboBoxOwnedIngredientAdd.Text &&
                (comboBoxOwnedIngredientAdd.SelectedItem as Ingredient).BaseUnit == null)
            {
                comboBoxOwnedIngredientAddUnit.DataSource = unitList;
            }
            else if (comboBoxOwnedIngredientAdd.SelectedItem != null &&
                (comboBoxOwnedIngredientAdd.SelectedItem as Ingredient).Name == comboBoxOwnedIngredientAdd.Text)
            {
                List<Unit> u = unitList.Where(x => x.GetType() == (comboBoxOwnedIngredientAdd.SelectedItem as Ingredient).BaseUnit.GetType()).ToList();
                comboBoxOwnedIngredientAddUnit.DataSource = u;
            }
            else
            {
                comboBoxOwnedIngredientAddUnit.DataSource = unitList;
            }
        }

        /// <summary>
        /// Zmienia ilość posiadanych składników
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxOwnedIngredientUnit_TextChanged(object sender, EventArgs e)
        {
            var id = Convert.ToInt32((sender as Control).Parent.Tag);
            double newAmmountOfUnitsOwned;
            if ((sender as Control).Text != String.Empty)
            {
                newAmmountOfUnitsOwned = Convert.ToDouble((sender as Control).Text);
            }
            else
            {
                return;
            }

            ingredientList.Find(x => x.Id == id).UnitsOwned = newAmmountOfUnitsOwned;
            DatabaseLogic.UpdateDatabase(ingredientList);

            if (newAmmountOfUnitsOwned == 0)
                LoadIngredientsToFridge(ref ingredientList);
        }

        /// <summary>
        /// Zmienia BaseUnit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxOwnedIngredientUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            var id = Convert.ToInt32((sender as Control).Parent.Tag);

            (sender as Control).Parent.Controls.OfType<ComboBox>().FirstOrDefault(x => x.Tag.ToString() == "unit");

            double newAmmountOfUnitsOwned = 0;
            if ((sender as Control).Text != String.Empty)
            {
                newAmmountOfUnitsOwned = Convert.ToDouble((sender as Control).Text);
            }

            ingredientList.Find(x => x.Id == id).UnitsOwned = newAmmountOfUnitsOwned;
            DatabaseLogic.UpdateDatabase(ingredientList);
        }

        /// <summary>
        /// Zeruje ilość posiadanych składników danego typu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOwnedIngredientDelete_Click(object sender, EventArgs e)
        {
            var id = Convert.ToInt32((sender as Control).Parent.Tag);

            ingredientList.Find(x => x.Id == id).UnitsOwned = 0;
            DatabaseLogic.UpdateDatabase(ingredientList);

            LoadIngredientsToFridge(ref ingredientList);
        }

        #endregion Fridge

        #region Recipes

        /// <summary>
        /// Zmienia na kartę dodawania przepisu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRecipesAdd_Click(object sender, EventArgs e)
        {
            InitializeNewRecipe();
            tabControlMain.SelectedTab = tabPageNewRecipe;
        }

        /// <summary>
        /// Wczytuje wyszukane przepisy do karty Recipes
        /// </summary>
        /// <param name="recipes"></param>
        private void LoadSearchResultsToRecipes(List<Recipe> recipes)
        {
            if (RecipeSearchResultPanels != null)
            {
                foreach (var item in RecipeSearchResultPanels)
                {
                    item.Dispose();
                }
                RecipeSearchResultPanels = null;
            }
            foreach (var recipe in recipes)
            {
                var panel = CreateNewRecipeSearchResultPanel();
                panel.Tag = recipe.Id.ToString();
                panel.Controls.OfType<Control>().FirstOrDefault(x => x.Text == labelRecipeSearchResultTitle.Text).Text =
                    recipe.Title;
            }
        }

        /// <summary>
        /// Dodaj pusty panel z wyszukanym przepisem
        /// </summary>
        /// <returns></returns>
        private Panel CreateNewRecipeSearchResultPanel()
        {
            var panel = new Panel
            {
                BorderStyle = panelRecipeSearchResult.BorderStyle,
                Location = panelRecipeSearchResult.Location,
                Size = panelRecipeSearchResult.Size,
                Visible = true,
                Cursor = panelRecipeSearchResult.Cursor,
                Anchor = panelRecipeSearchResult.Anchor
            };
            panel.Click += panelRecipeSearchResult_Click;
            tabPageRecipes.Controls.Add(panel);

            var label = new Label
            {
                Text = labelRecipeSearchResultTitle.Text,
                Font = labelRecipeSearchResultTitle.Font,
                Location = labelRecipeSearchResultTitle.Location,
                Size = labelRecipeSearchResultTitle.Size,
                Tag = labelRecipeSearchResultTitle.Tag
            };
            label.Click += panelRecipeSearchResult_Click;
            panel.Controls.Add(label);
            panel.Controls.OfType<Label>().Single(x => x.Tag == labelRecipeSearchResultTitle.Tag).Click += panelRecipeSearchResult_Click;
            
            if (RecipeSearchResultPanels == null)
            {
                RecipeSearchResultPanels = new List<Panel>();
            }
            else
            {
                var point = RecipeSearchResultPanels[RecipeSearchResultPanels.Count - 1].Location;
                point.Y += RecipeSearchResultPanels[RecipeSearchResultPanels.Count - 1].Height + PanelsMargin;
                panel.Location = point;
            }

            RecipeSearchResultPanels.Add(panel);

            return panel;
        }

        /// <summary>
        /// Załaduj wybrany przepis po wybraniu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelRecipeSearchResult_Click(object sender, EventArgs e)
        {
            var id = Convert.ToInt32((sender as Control).Tag == labelRecipeSearchResultTitle.Tag ? (sender as Control).Parent.Tag : (sender as Control).Tag);
            LoadRecipePage(id);
        }

        /// <summary>
        /// Załaduj wybrany przepis po id
        /// </summary>
        /// <param name="id"></param>
        private void LoadRecipePage(int id)
        {
            var recipe = recipeList.SingleOrDefault(x => x.Id == id);
            labelRecipeTitle.Text = recipe.Title;
            richTextBoxRecipeDescription.Text = recipe.Description;
            richTextBoxRecipeSource.Text = recipe.OriginalUrl;

            if (RecipeIngredientPanels != null)
            {
                foreach(var item in RecipeIngredientPanels)
        
                {
                    item.Dispose();
                }
                RecipeIngredientPanels.Clear();
            }
            else
                RecipeIngredientPanels = new List<Panel>();

            foreach(var ingredient in recipe.ListOfIngredient)
            {
                var panel = FormCreator.ClonePanel(panelRecipeIngredient);
                panel.Tag = ingredient.Ingredient.Id.ToString();

                foreach(Control control in panel.Controls)
        
                {
                    if (control.Tag == labelRecipeIngredientName.Tag)
                    {
                        control.Text = ingredient.Ingredient.Name;
                    }
                    else if (control.Tag == labelRecipeIngredientAmmmount.Tag)
                    {
                        control.Text = ingredient.Ammount.ToString();
                    }
                    else if (control.Tag == labelRecipeIngredientUnit.Tag)
                    {
                        control.Text = ingredient.Unit.Name;
                    }
                }
                if (RecipeIngredientPanels == null || RecipeIngredientPanels.Count == 0)
                {
                    RecipeIngredientPanels = new List<Panel>();
                }
                else
                {
                    var point = RecipeIngredientPanels[RecipeIngredientPanels.Count - 1].Location;
                    point.Y += RecipeIngredientPanels[RecipeIngredientPanels.Count - 1].Height + PanelsMargin;
                    panel.Location = point;
                }
                RecipeIngredientPanels.Add(panel);
                tabPageRecipe.Controls.Add(panel);
            }
            tabControlMain.SelectedTab = tabPageRecipe;
        }

        #endregion Recipes

        #region NewRecipe
        
        /// <summary>
        /// Dodaje składnik do listy składników tworzonego przepisu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNewRecipeIngredientAdd_Click(object sender, EventArgs e)
        {
            var button = (sender as Button);
            var comboBoxName = button.Parent.Controls.OfType<ComboBox>().FirstOrDefault(x => x.Tag.ToString() == "name");
            var textBoxAmmount = button.Parent.Controls.OfType<TextBox>().FirstOrDefault(x => x.Tag.ToString() == "ammount");
            var comboBoxUnit = button.Parent.Controls.OfType<ComboBox>().FirstOrDefault(x => x.Tag.ToString() == "unit");

            if (comboBoxName.SelectedItem == null || textBoxAmmount.Text == null || comboBoxUnit.SelectedItem == null)
                return;

            button.Parent.Tag = (comboBoxName.SelectedItem as Ingredient).Id;

            var oryginalIngredient = ingredientList.FirstOrDefault(x => x.Id == Convert.ToInt32(button.Parent.Tag));
            var ingredientInRecipe = new IngredientInRecipe();
            ingredientInRecipe.Ingredient = oryginalIngredient;
            try
            {
                ingredientInRecipe.Ammount = Convert.ToDouble(textBoxAmmount.Text);
            }
            catch (Exception)
            {
                ingredientInRecipe.Ammount = 0;
            }
            ingredientInRecipe.Unit = comboBoxUnit.SelectedItem as Unit;

            newRecipeIngredients.Add(ingredientInRecipe);
            button.Text = "X";
            button.Click -= buttonNewRecipeIngredientAdd_Click;
            button.Click += buttonNewRecipeIngredientRemove_Click;
            CreateNewRecipeIngredientPanel();
        }

        /// <summary>
        /// Usuwa składnik z listy składników
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNewRecipeIngredientRemove_Click(object sender, EventArgs e)
        {
            var button = (sender as Button);

            newRecipeIngredients.Remove(newRecipeIngredients.FirstOrDefault(x =>
                x.Ingredient.Id.ToString() == button.Parent.Tag.ToString()));

            if (NewRecipeIngredientPanels != null)
            {
                foreach (var item in NewRecipeIngredientPanels)
                {
                    item.Dispose();
                }
                NewRecipeIngredientPanels = null;
            }

            foreach (var ingredient in newRecipeIngredients)
            {
                if (ingredient.Ammount != 0)
                {
                    var panel = CreateNewRecipeIngredientPanel();
                    panel.Tag = ingredient.Ingredient.Id.ToString();
                    panel.Controls.OfType<ComboBox>().FirstOrDefault(x => x.Tag.ToString() == "name").Text =
                        ingredient.Ingredient.Name;
                    panel.Controls.OfType<TextBox>().FirstOrDefault(x => x.Tag.ToString() == "ammount").Text
                        = ingredient.Ammount.ToString();
                    panel.Controls.OfType<ComboBox>().FirstOrDefault(x => x.Tag.ToString() == "unit").Text = ingredient.Ingredient.BaseUnit.Name;
                    var b = panel.Controls.OfType<Control>().FirstOrDefault(x => x.Tag.ToString() == "button");
                    b.Text = "X";
                    b.Click -= buttonNewRecipeIngredientAdd_Click;
                    b.Click += buttonNewRecipeIngredientRemove_Click;
                }
            }

            CreateNewRecipeIngredientPanel();
        }

        /// <summary>
        /// Zmienia listę możliwych jednostek w zależności od wybranego składnika
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxNewRecipeIngredient_SelectedIndexChanged(object sender, EventArgs e)
        {
            var u = unitList.Where(x =>
                x.GetType() == ((sender as Control).Parent.Controls.OfType<ComboBox>()
                    .FirstOrDefault(t => t.Tag == comboBoxNewRecipeIngredient.Tag).SelectedItem as Ingredient).BaseUnit.GetType()).ToList();

            (sender as Control).Parent.Controls.OfType<ComboBox>()
                .FirstOrDefault(x => x.Tag == comboBoxNewRecipeIngredientUnit.Tag).DataSource = u;
        }

        /// <summary>
        /// Inicjalizuje karte zmiany przepisu
        /// </summary>
        private void InitializeNewRecipe()
        {
            newRecipeIngredients = new List<IngredientInRecipe>();
            comboBoxNewRecipeIngredient.DataSource = new List<Ingredient>(ingredientList);
            comboBoxNewRecipeIngredient.DisplayMember = "Name";
            comboBoxNewRecipeIngredientUnit.DataSource = new List<Unit>(unitList);
            if (NewRecipeIngredientPanels != null)
            {
                foreach (var item in NewRecipeIngredientPanels)
                {
                    item.Dispose();
                }
                NewRecipeIngredientPanels = null;
            }
            CreateNewRecipeIngredientPanel();
        }

        /// <summary>
        /// Dodaj pusty panel z wyszukanym przepisem
        /// </summary>
        /// <returns></returns>
        private Panel CreateNewRecipeIngredientPanel()
        {
            var panel = FormCreator.ClonePanel(panelNewRecipeIngredients);
            foreach (Control control in panel.Controls)

            {
                if (control.Tag == comboBoxNewRecipeIngredient.Tag)
                {
                    (control as ComboBox).DataSource = new List<Ingredient>(ingredientList);
                    (control as ComboBox).DisplayMember = "Name";
                }
                else if (control.Tag == comboBoxNewRecipeIngredientUnit.Tag)
                {
                    (control as ComboBox).DataSource = new List<Unit>(unitList);
                    (control as ComboBox).DisplayMember = "Name";
                }
                else if (control.Tag == buttonNewRecipeIngredientAdd.Tag)
                {
                    control.Click += buttonNewRecipeIngredientAdd_Click;
                }
            }
            if (NewRecipeIngredientPanels == null || NewRecipeIngredientPanels.Count == 0)
            {
                NewRecipeIngredientPanels = new List<Panel>();
            }
            else
            {
                var point = NewRecipeIngredientPanels[NewRecipeIngredientPanels.Count - 1].Location;
                point.Y += NewRecipeIngredientPanels[NewRecipeIngredientPanels.Count - 1].Height + PanelsMargin;
                panel.Location = point;
            }
            NewRecipeIngredientPanels.Add(panel);
            tabPageNewRecipe.Controls.Add(panel);
            return panel;
        }

        /// <summary>
        /// Zapisz nowy przepis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNewRecipeSave_Click(object sender, EventArgs e)
        {
            if (textBoxNewRecipeTitle.Text == null || richTextBoxNewRecipeDescription.Text == null) return;

            var recipe = new Recipe
            {
                OriginalUrl = textBoxNewRecipeOriginalUrl.Text,
                Description = richTextBoxNewRecipeDescription.Text,
                ListOfIngredient = newRecipeIngredients,
                Title = textBoxNewRecipeTitle.Text
            };

            DatabaseLogic.AddNewRecipe(recipe, ref recipeList);
            LoadSearchResultsToRecipes(recipeList);

            LoadRecipePage(recipe.Id);
        }

        #endregion NewRecipe

        #region Ingredients

        /// <summary>
        /// Dodaj składnik w zakładce składniki
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIngredientsAdd_Click(object sender, EventArgs e)
        {
            var  lName = textBoxIngredientsName.Text;
            var  lPrice = textBoxIngredientsPrice.Text;
            textBoxIngredientsName.Clear();
            textBoxIngredientsPrice.Clear();

            // sprawdzic czy Name istnieje
            if (ingredientList.Any(i => i.Name == lName))
            {
                MessageBox.Show("SKŁADNIK JUŻ ISTNIEJE");
                return;
            }
            // dodac nowy do listy
            DatabaseLogic.AddNewIngredient(new Ingredient
            {
                Name = lName,
                PricePerUnit = Convert.ToInt32(lPrice)
            }, ref ingredientList);
            // wyswietlic nowa pozycje
            DatabaseLogic.UpdateDatabase(ingredientList);
            LoadIngredients();
        }

        /// <summary>
        /// Wczytaj składniki do zakładki składniki
        /// </summary>
        private void LoadIngredients()
        {
            if (IngredientsPanel != null)
            {
                foreach(var  item in IngredientsPanel)
                {
                    item.Dispose();
                }
                IngredientsPanel.Clear();
            }
            else IngredientsPanel = new List < Panel>();

            foreach(var  ingredient in ingredientList)
            {
                var  panel = FormCreator.ClonePanel(panelIngredients);
                panel.Tag = ingredient.Id.ToString();

                foreach(Control  control in panel.Controls)
                {
                    if (control.Tag == textBoxIngredientsNameAdded.Tag)
                    {
                        control.Text = ingredient.Name;
                    }
                    else if (control.Tag == textBoxIngredientsPriceAdded.Tag)
                    {
                        control.Text = ingredient.PricePerUnit.ToString();
                        control.TextChanged += textBoxIngredientsPriceAdded_TextChanged;
                    }
                }

                if (IngredientsPanel == null || IngredientsPanel.Count == 0)
                {
                    IngredientsPanel = new List<Panel>();
                }
                else
                {
                    var point = IngredientsPanel[IngredientsPanel.Count - 1].Location;
                    point.Y += IngredientsPanel[IngredientsPanel.Count - 1].Height + PanelsMargin;
                    panel.Location = point;
                }
                IngredientsPanel.Add(panel);
                tabPageIngredients.Controls.Add(panel);
            }
        }

        /// <summary>
        /// Zaaktualizuj cenę składnika po zmianie ceny w polu tekstowyn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxIngredientsPriceAdded_TextChanged(object sender, EventArgs e)
        {
            var control = sender as Control;
            if (control != null && control.Text == string.Empty) return;

            var id = int.Parse(control.Parent.Tag.ToString());

            ingredientList.Single(x => x.Id == id).PricePerUnit = Double.Parse(control.Text);

            DatabaseLogic.UpdateDatabase(ingredientList);
        }

        #endregion Ingredients
    }
}
