﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SPKBeta
{
    [XmlInclude(typeof(UnitLiquid))]
    [XmlInclude(typeof(UnitSolid))]
    [XmlInclude(typeof(UnitIncalculable))]
    [Serializable]
    public abstract class Unit
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
