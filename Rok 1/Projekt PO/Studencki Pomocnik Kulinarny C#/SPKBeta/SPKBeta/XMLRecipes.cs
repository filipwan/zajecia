﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPKBeta
{
    class XMLRecipes
    {
        private XMLCategory() { }
        static DataSet ds = new DataSet();
        static DataView dv = new DataView();

        public static void Save()
        {
            ds.WriteXml(Application.StartupPath + "\\XML\\Category.xml", XmlWriteMode.WriteSchema);
        }
    }
}