﻿namespace SPKBeta
{
    public class Ingredient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Unit BaseUnit { get; set; }
        public double PricePerUnit { get; set; }
        public double UnitsOwned { get; set; }
    }
}
