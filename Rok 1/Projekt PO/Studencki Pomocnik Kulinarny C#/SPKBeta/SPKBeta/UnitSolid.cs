﻿using System;

namespace SPKBeta
{
    [Serializable]
    public class UnitSolid : Unit
    {
        public int UnitInGrams { get; set; }
    }
}
