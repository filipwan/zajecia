﻿using System.Collections.Generic;

namespace SPKBeta
{
    public class Recipe
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<IngredientInRecipe> ListOfIngredient { get; set; }
        public string OriginalUrl { get; set; }
   
    }
}
