﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SPKBeta
{
    public static class FormCreator
    {
        /// <summary>
        /// Zwraca kopię panelu wraz z zawartością
        /// </summary>
        /// <param name="panel"></param>
        /// <returns></returns>
        public static Panel ClonePanel(Panel panel)
        {
            var newPanel = new Panel
            {
                Size = panel.Size,
                Location = panel.Location,
                Anchor = panel.Anchor,
                BackColor = panel.BackColor,
                BorderStyle = panel.BorderStyle,
                Tag = panel.Tag,
                Visible = true
            };

            foreach(Control control in panel.Controls)
	        {
	            switch (control)
	            {
	                case Label _:
	                    newPanel.Controls.Add(CloneControl(control as Label));
	                    break;
	                case TextBox _:
	                    newPanel.Controls.Add(CloneControl(control as TextBox));
	                    break;
	                case Button _:
	                    newPanel.Controls.Add(CloneControl(control as Button));
	                    break;
	                case ComboBox _:
	                    newPanel.Controls.Add(CloneControl(control as ComboBox));
	                    break;
	            }
	        }
	        return newPanel;
        }

        /// <summary>
        /// Zwraca kopię Label
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private static Control CloneControl(Label control)
        {
            var newControl = new Label
            {
                Size = control.Size,
                Location = control.Location,
                Anchor = control.Anchor,
                Text = control.Text,
                TextAlign = control.TextAlign,
                Font = control.Font,
                Tag = control.Tag
            };

            return newControl;
        }

        /// <summary>
        /// Zwraca kopię TextBox
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private static Control CloneControl(TextBox control)
        {
            var newControl = new TextBox
            {
                Size = control.Size,
                Location = control.Location,
                Anchor = control.Anchor,
                Text = control.Text,
                TextAlign = control.TextAlign,
                Font = control.Font,
                Tag = control.Tag
            };

            return newControl;
        }

        /// <summary>
        /// Zwraca kopię Button
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private static Control CloneControl(Button control)
        {
            var newControl = new Button
            {
                Size = control.Size,
                Location = control.Location,
                Anchor = control.Anchor,
                BackColor = control.BackColor,
                Text = control.Text,
                Font = control.Font,
                Tag = control.Tag
            };

            return newControl;
        }

        /// <summary>
        /// Zwraca kopię ComboBox
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private static Control CloneControl(ComboBox control)
        {
            var newControl = new ComboBox
            {
                Size = control.Size,
                Location = control.Location,
                Anchor = control.Anchor,
                BackColor = control.BackColor,
                DisplayMember = control.DisplayMember,
                Enabled = control.Enabled,
                Font = control.Font,
                Tag = control.Tag
            };

            return newControl;
        }
    }
}
