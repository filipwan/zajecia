﻿namespace SPKBeta
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitterMenu = new System.Windows.Forms.Splitter();
            this.buttonFridge = new System.Windows.Forms.Button();
            this.buttonRecipes = new System.Windows.Forms.Button();
            this.buttonIngredients = new System.Windows.Forms.Button();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageFridge = new System.Windows.Forms.TabPage();
            this.labelFridge = new System.Windows.Forms.Label();
            this.panelOwnedIngredientAdd = new System.Windows.Forms.Panel();
            this.comboBoxOwnedIngredientAddUnit = new System.Windows.Forms.ComboBox();
            this.textBoxOwnedIngredientAddUnitAmmount = new System.Windows.Forms.TextBox();
            this.comboBoxOwnedIngredientAdd = new System.Windows.Forms.ComboBox();
            this.buttonOwnedIngredientAdd = new System.Windows.Forms.Button();
            this.labelOwnedIngredientAdd = new System.Windows.Forms.Label();
            this.panelOwnedIngredient = new System.Windows.Forms.Panel();
            this.comboBoxOwnedIngredientUnit = new System.Windows.Forms.ComboBox();
            this.textBoxOwnedIngredientUnitAmmount = new System.Windows.Forms.TextBox();
            this.labelOwnedIngredientUnit = new System.Windows.Forms.Label();
            this.buttonOwnedIngredientDelete = new System.Windows.Forms.Button();
            this.labelOwnedIngredient = new System.Windows.Forms.Label();
            this.tabPageRecipes = new System.Windows.Forms.TabPage();
            this.buttonRecipesAdd = new System.Windows.Forms.Button();
            this.textBoxRecipeSearch = new System.Windows.Forms.TextBox();
            this.panelRecipeSearchResult = new System.Windows.Forms.Panel();
            this.labelRecipeSearchResultTitle = new System.Windows.Forms.Label();
            this.tabPageNewRecipe = new System.Windows.Forms.TabPage();
            this.labelNewRecipeOriginalUrl = new System.Windows.Forms.Label();
            this.textBoxNewRecipeOriginalUrl = new System.Windows.Forms.TextBox();
            this.buttonNewRecipeSave = new System.Windows.Forms.Button();
            this.richTextBoxNewRecipeDescription = new System.Windows.Forms.RichTextBox();
            this.panelNewRecipeIngredients = new System.Windows.Forms.Panel();
            this.comboBoxNewRecipeIngredientUnit = new System.Windows.Forms.ComboBox();
            this.buttonNewRecipeIngredientAdd = new System.Windows.Forms.Button();
            this.labelNewRecipeIngredient = new System.Windows.Forms.Label();
            this.comboBoxNewRecipeIngredient = new System.Windows.Forms.ComboBox();
            this.textBoxNewRecipeIngredient = new System.Windows.Forms.TextBox();
            this.labelNewRecipeDescripton = new System.Windows.Forms.Label();
            this.labelNewRecipeIngredients = new System.Windows.Forms.Label();
            this.labelNewRecipeTitle = new System.Windows.Forms.Label();
            this.textBoxNewRecipeTitle = new System.Windows.Forms.TextBox();
            this.tabPageRecipe = new System.Windows.Forms.TabPage();
            this.richTextBoxRecipeSource = new System.Windows.Forms.RichTextBox();
            this.labelRecipeIngredientSource = new System.Windows.Forms.Label();
            this.richTextBoxRecipeDescription = new System.Windows.Forms.RichTextBox();
            this.panelRecipeIngredient = new System.Windows.Forms.Panel();
            this.labelRecipeIngredientName = new System.Windows.Forms.Label();
            this.labelRecipeIngredientUnit = new System.Windows.Forms.Label();
            this.labelRecipeIngredientAmmmount = new System.Windows.Forms.Label();
            this.labelRecipeIngredientSeparator = new System.Windows.Forms.Label();
            this.labelRecipeDescription = new System.Windows.Forms.Label();
            this.labelRecipeIngredients = new System.Windows.Forms.Label();
            this.labelRecipeTitle = new System.Windows.Forms.Label();
            this.tabPageIngredients = new System.Windows.Forms.TabPage();
            this.panelIngredients = new System.Windows.Forms.Panel();
            this.textBoxIngredientsPriceAdded = new System.Windows.Forms.TextBox();
            this.textBoxIngredientsNameAdded = new System.Windows.Forms.TextBox();
            this.labelIngredientsPriceOwned = new System.Windows.Forms.Label();
            this.labelIngredientsNameOwned = new System.Windows.Forms.Label();
            this.buttonIngredientsAdd = new System.Windows.Forms.Button();
            this.textBoxIngredientsPrice = new System.Windows.Forms.TextBox();
            this.labelIngredientsPrice = new System.Windows.Forms.Label();
            this.textBoxIngredientsName = new System.Windows.Forms.TextBox();
            this.labelIngredietnsName = new System.Windows.Forms.Label();
            this.labelIngredients = new System.Windows.Forms.Label();
            this.tabControlMain.SuspendLayout();
            this.tabPageFridge.SuspendLayout();
            this.panelOwnedIngredientAdd.SuspendLayout();
            this.panelOwnedIngredient.SuspendLayout();
            this.tabPageRecipes.SuspendLayout();
            this.panelRecipeSearchResult.SuspendLayout();
            this.tabPageNewRecipe.SuspendLayout();
            this.panelNewRecipeIngredients.SuspendLayout();
            this.tabPageRecipe.SuspendLayout();
            this.panelRecipeIngredient.SuspendLayout();
            this.tabPageIngredients.SuspendLayout();
            this.panelIngredients.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitterMenu
            // 
            this.splitterMenu.BackColor = System.Drawing.Color.LightSteelBlue;
            this.splitterMenu.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splitterMenu.Location = new System.Drawing.Point(0, 0);
            this.splitterMenu.Name = "splitterMenu";
            this.splitterMenu.Size = new System.Drawing.Size(194, 611);
            this.splitterMenu.TabIndex = 0;
            this.splitterMenu.TabStop = false;
            // 
            // buttonFridge
            // 
            this.buttonFridge.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonFridge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFridge.Font = new System.Drawing.Font("Calibri", 20F);
            this.buttonFridge.Location = new System.Drawing.Point(12, 132);
            this.buttonFridge.Name = "buttonFridge";
            this.buttonFridge.Size = new System.Drawing.Size(169, 52);
            this.buttonFridge.TabIndex = 1;
            this.buttonFridge.Text = "Lodówka";
            this.buttonFridge.UseVisualStyleBackColor = false;
            this.buttonFridge.Click += new System.EventHandler(this.buttonFridge_Click);
            // 
            // buttonRecipes
            // 
            this.buttonRecipes.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonRecipes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRecipes.Font = new System.Drawing.Font("Calibri", 20F);
            this.buttonRecipes.Location = new System.Drawing.Point(12, 230);
            this.buttonRecipes.Name = "buttonRecipes";
            this.buttonRecipes.Size = new System.Drawing.Size(169, 52);
            this.buttonRecipes.TabIndex = 2;
            this.buttonRecipes.Text = "Przepisy";
            this.buttonRecipes.UseVisualStyleBackColor = false;
            this.buttonRecipes.Click += new System.EventHandler(this.buttonRecipes_Click);
            // 
            // buttonIngredients
            // 
            this.buttonIngredients.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonIngredients.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonIngredients.Font = new System.Drawing.Font("Calibri", 20F);
            this.buttonIngredients.Location = new System.Drawing.Point(12, 328);
            this.buttonIngredients.Name = "buttonIngredients";
            this.buttonIngredients.Size = new System.Drawing.Size(169, 52);
            this.buttonIngredients.TabIndex = 3;
            this.buttonIngredients.Text = "Składniki";
            this.buttonIngredients.UseVisualStyleBackColor = false;
            this.buttonIngredients.Click += new System.EventHandler(this.buttonIngredients_Click);
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageFridge);
            this.tabControlMain.Controls.Add(this.tabPageRecipes);
            this.tabControlMain.Controls.Add(this.tabPageNewRecipe);
            this.tabControlMain.Controls.Add(this.tabPageRecipe);
            this.tabControlMain.Controls.Add(this.tabPageIngredients);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(194, 0);
            this.tabControlMain.Margin = new System.Windows.Forms.Padding(0);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.Padding = new System.Drawing.Point(0, 0);
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(690, 611);
            this.tabControlMain.TabIndex = 5;
            // 
            // tabPageFridge
            // 
            this.tabPageFridge.AutoScroll = true;
            this.tabPageFridge.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageFridge.Controls.Add(this.labelFridge);
            this.tabPageFridge.Controls.Add(this.panelOwnedIngredientAdd);
            this.tabPageFridge.Controls.Add(this.panelOwnedIngredient);
            this.tabPageFridge.Location = new System.Drawing.Point(4, 22);
            this.tabPageFridge.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageFridge.Name = "tabPageFridge";
            this.tabPageFridge.Size = new System.Drawing.Size(682, 585);
            this.tabPageFridge.TabIndex = 0;
            this.tabPageFridge.Text = "Lodówka";
            // 
            // labelFridge
            // 
            this.labelFridge.AutoSize = true;
            this.labelFridge.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.labelFridge.Location = new System.Drawing.Point(81, 34);
            this.labelFridge.Name = "labelFridge";
            this.labelFridge.Size = new System.Drawing.Size(181, 46);
            this.labelFridge.TabIndex = 27;
            this.labelFridge.Text = "Lodówka";
            // 
            // panelOwnedIngredientAdd
            // 
            this.panelOwnedIngredientAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOwnedIngredientAdd.Controls.Add(this.comboBoxOwnedIngredientAddUnit);
            this.panelOwnedIngredientAdd.Controls.Add(this.textBoxOwnedIngredientAddUnitAmmount);
            this.panelOwnedIngredientAdd.Controls.Add(this.comboBoxOwnedIngredientAdd);
            this.panelOwnedIngredientAdd.Controls.Add(this.buttonOwnedIngredientAdd);
            this.panelOwnedIngredientAdd.Controls.Add(this.labelOwnedIngredientAdd);
            this.panelOwnedIngredientAdd.Location = new System.Drawing.Point(32, 110);
            this.panelOwnedIngredientAdd.Name = "panelOwnedIngredientAdd";
            this.panelOwnedIngredientAdd.Size = new System.Drawing.Size(624, 52);
            this.panelOwnedIngredientAdd.TabIndex = 1;
            // 
            // comboBoxOwnedIngredientAddUnit
            // 
            this.comboBoxOwnedIngredientAddUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOwnedIngredientAddUnit.Font = new System.Drawing.Font("Calibri", 12F);
            this.comboBoxOwnedIngredientAddUnit.FormattingEnabled = true;
            this.comboBoxOwnedIngredientAddUnit.Location = new System.Drawing.Point(482, 14);
            this.comboBoxOwnedIngredientAddUnit.Name = "comboBoxOwnedIngredientAddUnit";
            this.comboBoxOwnedIngredientAddUnit.Size = new System.Drawing.Size(104, 27);
            this.comboBoxOwnedIngredientAddUnit.TabIndex = 7;
            // 
            // textBoxOwnedIngredientAddUnitAmmount
            // 
            this.textBoxOwnedIngredientAddUnitAmmount.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOwnedIngredientAddUnitAmmount.Location = new System.Drawing.Point(391, 14);
            this.textBoxOwnedIngredientAddUnitAmmount.Name = "textBoxOwnedIngredientAddUnitAmmount";
            this.textBoxOwnedIngredientAddUnitAmmount.Size = new System.Drawing.Size(85, 27);
            this.textBoxOwnedIngredientAddUnitAmmount.TabIndex = 2;
            // 
            // comboBoxOwnedIngredientAdd
            // 
            this.comboBoxOwnedIngredientAdd.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxOwnedIngredientAdd.Font = new System.Drawing.Font("Calibri", 12F);
            this.comboBoxOwnedIngredientAdd.FormattingEnabled = true;
            this.comboBoxOwnedIngredientAdd.Location = new System.Drawing.Point(137, 14);
            this.comboBoxOwnedIngredientAdd.Name = "comboBoxOwnedIngredientAdd";
            this.comboBoxOwnedIngredientAdd.Size = new System.Drawing.Size(248, 27);
            this.comboBoxOwnedIngredientAdd.TabIndex = 2;
            this.comboBoxOwnedIngredientAdd.TextChanged += new System.EventHandler(this.comboBoxOwnedIngredientAdd_TextChanged);
            // 
            // buttonOwnedIngredientAdd
            // 
            this.buttonOwnedIngredientAdd.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonOwnedIngredientAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonOwnedIngredientAdd.Font = new System.Drawing.Font("Calibri", 12F);
            this.buttonOwnedIngredientAdd.Location = new System.Drawing.Point(592, 13);
            this.buttonOwnedIngredientAdd.Name = "buttonOwnedIngredientAdd";
            this.buttonOwnedIngredientAdd.Size = new System.Drawing.Size(27, 27);
            this.buttonOwnedIngredientAdd.TabIndex = 6;
            this.buttonOwnedIngredientAdd.Text = "+";
            this.buttonOwnedIngredientAdd.UseVisualStyleBackColor = false;
            this.buttonOwnedIngredientAdd.Click += new System.EventHandler(this.buttonOwnedIngredientAdd_Click);
            // 
            // labelOwnedIngredientAdd
            // 
            this.labelOwnedIngredientAdd.AutoSize = true;
            this.labelOwnedIngredientAdd.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOwnedIngredientAdd.Location = new System.Drawing.Point(23, 16);
            this.labelOwnedIngredientAdd.Name = "labelOwnedIngredientAdd";
            this.labelOwnedIngredientAdd.Size = new System.Drawing.Size(108, 19);
            this.labelOwnedIngredientAdd.TabIndex = 0;
            this.labelOwnedIngredientAdd.Text = "Dodaj składnik:";
            // 
            // panelOwnedIngredient
            // 
            this.panelOwnedIngredient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelOwnedIngredient.Controls.Add(this.comboBoxOwnedIngredientUnit);
            this.panelOwnedIngredient.Controls.Add(this.textBoxOwnedIngredientUnitAmmount);
            this.panelOwnedIngredient.Controls.Add(this.labelOwnedIngredientUnit);
            this.panelOwnedIngredient.Controls.Add(this.buttonOwnedIngredientDelete);
            this.panelOwnedIngredient.Controls.Add(this.labelOwnedIngredient);
            this.panelOwnedIngredient.Location = new System.Drawing.Point(32, 168);
            this.panelOwnedIngredient.Name = "panelOwnedIngredient";
            this.panelOwnedIngredient.Size = new System.Drawing.Size(477, 52);
            this.panelOwnedIngredient.TabIndex = 0;
            this.panelOwnedIngredient.Visible = false;
            // 
            // comboBoxOwnedIngredientUnit
            // 
            this.comboBoxOwnedIngredientUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxOwnedIngredientUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOwnedIngredientUnit.Font = new System.Drawing.Font("Calibri", 12F);
            this.comboBoxOwnedIngredientUnit.FormattingEnabled = true;
            this.comboBoxOwnedIngredientUnit.Location = new System.Drawing.Point(335, 13);
            this.comboBoxOwnedIngredientUnit.Name = "comboBoxOwnedIngredientUnit";
            this.comboBoxOwnedIngredientUnit.Size = new System.Drawing.Size(104, 27);
            this.comboBoxOwnedIngredientUnit.TabIndex = 10;
            this.comboBoxOwnedIngredientUnit.Tag = "unit";
            this.comboBoxOwnedIngredientUnit.SelectedIndexChanged += new System.EventHandler(this.comboBoxOwnedIngredientUnit_SelectedIndexChanged);
            // 
            // textBoxOwnedIngredientUnitAmmount
            // 
            this.textBoxOwnedIngredientUnitAmmount.AllowDrop = true;
            this.textBoxOwnedIngredientUnitAmmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOwnedIngredientUnitAmmount.Font = new System.Drawing.Font("Calibri", 12F);
            this.textBoxOwnedIngredientUnitAmmount.Location = new System.Drawing.Point(264, 13);
            this.textBoxOwnedIngredientUnitAmmount.Name = "textBoxOwnedIngredientUnitAmmount";
            this.textBoxOwnedIngredientUnitAmmount.Size = new System.Drawing.Size(43, 27);
            this.textBoxOwnedIngredientUnitAmmount.TabIndex = 7;
            this.textBoxOwnedIngredientUnitAmmount.Tag = "ammount";
            this.textBoxOwnedIngredientUnitAmmount.Text = "1";
            this.textBoxOwnedIngredientUnitAmmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxOwnedIngredientUnitAmmount.TextChanged += new System.EventHandler(this.textBoxOwnedIngredientUnit_TextChanged);
            // 
            // labelOwnedIngredientUnit
            // 
            this.labelOwnedIngredientUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOwnedIngredientUnit.AutoSize = true;
            this.labelOwnedIngredientUnit.Font = new System.Drawing.Font("Calibri", 12F);
            this.labelOwnedIngredientUnit.Location = new System.Drawing.Point(313, 18);
            this.labelOwnedIngredientUnit.Name = "labelOwnedIngredientUnit";
            this.labelOwnedIngredientUnit.Size = new System.Drawing.Size(16, 19);
            this.labelOwnedIngredientUnit.TabIndex = 9;
            this.labelOwnedIngredientUnit.Tag = "unit";
            this.labelOwnedIngredientUnit.Text = "x";
            // 
            // buttonOwnedIngredientDelete
            // 
            this.buttonOwnedIngredientDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOwnedIngredientDelete.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonOwnedIngredientDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonOwnedIngredientDelete.Font = new System.Drawing.Font("Calibri", 12F);
            this.buttonOwnedIngredientDelete.Location = new System.Drawing.Point(445, 13);
            this.buttonOwnedIngredientDelete.Name = "buttonOwnedIngredientDelete";
            this.buttonOwnedIngredientDelete.Size = new System.Drawing.Size(27, 27);
            this.buttonOwnedIngredientDelete.TabIndex = 8;
            this.buttonOwnedIngredientDelete.Text = "X";
            this.buttonOwnedIngredientDelete.UseVisualStyleBackColor = false;
            this.buttonOwnedIngredientDelete.Click += new System.EventHandler(this.buttonOwnedIngredientDelete_Click);
            // 
            // labelOwnedIngredient
            // 
            this.labelOwnedIngredient.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOwnedIngredient.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOwnedIngredient.Location = new System.Drawing.Point(23, 17);
            this.labelOwnedIngredient.Name = "labelOwnedIngredient";
            this.labelOwnedIngredient.Size = new System.Drawing.Size(235, 19);
            this.labelOwnedIngredient.TabIndex = 1;
            this.labelOwnedIngredient.Tag = "name";
            this.labelOwnedIngredient.Text = "Składnik";
            // 
            // tabPageRecipes
            // 
            this.tabPageRecipes.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageRecipes.Controls.Add(this.buttonRecipesAdd);
            this.tabPageRecipes.Controls.Add(this.textBoxRecipeSearch);
            this.tabPageRecipes.Controls.Add(this.panelRecipeSearchResult);
            this.tabPageRecipes.Location = new System.Drawing.Point(4, 22);
            this.tabPageRecipes.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageRecipes.Name = "tabPageRecipes";
            this.tabPageRecipes.Size = new System.Drawing.Size(682, 585);
            this.tabPageRecipes.TabIndex = 1;
            this.tabPageRecipes.Text = "Przepisy";
            // 
            // buttonRecipesAdd
            // 
            this.buttonRecipesAdd.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonRecipesAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRecipesAdd.Font = new System.Drawing.Font("Calibri", 12F);
            this.buttonRecipesAdd.Location = new System.Drawing.Point(32, 44);
            this.buttonRecipesAdd.Name = "buttonRecipesAdd";
            this.buttonRecipesAdd.Size = new System.Drawing.Size(214, 27);
            this.buttonRecipesAdd.TabIndex = 9;
            this.buttonRecipesAdd.Text = "Dodaj nowy przepis";
            this.buttonRecipesAdd.UseVisualStyleBackColor = false;
            this.buttonRecipesAdd.Click += new System.EventHandler(this.buttonRecipesAdd_Click);
            // 
            // textBoxRecipeSearch
            // 
            this.textBoxRecipeSearch.Font = new System.Drawing.Font("Calibri", 12F);
            this.textBoxRecipeSearch.Location = new System.Drawing.Point(32, 77);
            this.textBoxRecipeSearch.Name = "textBoxRecipeSearch";
            this.textBoxRecipeSearch.Size = new System.Drawing.Size(214, 27);
            this.textBoxRecipeSearch.TabIndex = 1;
            // 
            // panelRecipeSearchResult
            // 
            this.panelRecipeSearchResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelRecipeSearchResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRecipeSearchResult.Controls.Add(this.labelRecipeSearchResultTitle);
            this.panelRecipeSearchResult.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelRecipeSearchResult.Location = new System.Drawing.Point(32, 110);
            this.panelRecipeSearchResult.Name = "panelRecipeSearchResult";
            this.panelRecipeSearchResult.Size = new System.Drawing.Size(642, 70);
            this.panelRecipeSearchResult.TabIndex = 2;
            this.panelRecipeSearchResult.Visible = false;
            this.panelRecipeSearchResult.Click += new System.EventHandler(this.panelRecipeSearchResult_Click);
            // 
            // labelRecipeSearchResultTitle
            // 
            this.labelRecipeSearchResultTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRecipeSearchResultTitle.Font = new System.Drawing.Font("Calibri", 24F);
            this.labelRecipeSearchResultTitle.Location = new System.Drawing.Point(29, 12);
            this.labelRecipeSearchResultTitle.Name = "labelRecipeSearchResultTitle";
            this.labelRecipeSearchResultTitle.Size = new System.Drawing.Size(608, 39);
            this.labelRecipeSearchResultTitle.TabIndex = 0;
            this.labelRecipeSearchResultTitle.Tag = "Title";
            this.labelRecipeSearchResultTitle.Text = "Tytuł";
            // 
            // tabPageNewRecipe
            // 
            this.tabPageNewRecipe.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageNewRecipe.Controls.Add(this.labelNewRecipeOriginalUrl);
            this.tabPageNewRecipe.Controls.Add(this.textBoxNewRecipeOriginalUrl);
            this.tabPageNewRecipe.Controls.Add(this.buttonNewRecipeSave);
            this.tabPageNewRecipe.Controls.Add(this.richTextBoxNewRecipeDescription);
            this.tabPageNewRecipe.Controls.Add(this.panelNewRecipeIngredients);
            this.tabPageNewRecipe.Controls.Add(this.labelNewRecipeDescripton);
            this.tabPageNewRecipe.Controls.Add(this.labelNewRecipeIngredients);
            this.tabPageNewRecipe.Controls.Add(this.labelNewRecipeTitle);
            this.tabPageNewRecipe.Controls.Add(this.textBoxNewRecipeTitle);
            this.tabPageNewRecipe.Location = new System.Drawing.Point(4, 22);
            this.tabPageNewRecipe.Name = "tabPageNewRecipe";
            this.tabPageNewRecipe.Size = new System.Drawing.Size(682, 585);
            this.tabPageNewRecipe.TabIndex = 5;
            this.tabPageNewRecipe.Text = "Nowy Przepis";
            // 
            // labelNewRecipeOriginalUrl
            // 
            this.labelNewRecipeOriginalUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelNewRecipeOriginalUrl.AutoSize = true;
            this.labelNewRecipeOriginalUrl.Location = new System.Drawing.Point(310, 557);
            this.labelNewRecipeOriginalUrl.Name = "labelNewRecipeOriginalUrl";
            this.labelNewRecipeOriginalUrl.Size = new System.Drawing.Size(42, 13);
            this.labelNewRecipeOriginalUrl.TabIndex = 12;
            this.labelNewRecipeOriginalUrl.Text = "Źródło:";
            // 
            // textBoxNewRecipeOriginalUrl
            // 
            this.textBoxNewRecipeOriginalUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNewRecipeOriginalUrl.Font = new System.Drawing.Font("Calibri", 10F);
            this.textBoxNewRecipeOriginalUrl.Location = new System.Drawing.Point(358, 553);
            this.textBoxNewRecipeOriginalUrl.Name = "textBoxNewRecipeOriginalUrl";
            this.textBoxNewRecipeOriginalUrl.Size = new System.Drawing.Size(304, 24);
            this.textBoxNewRecipeOriginalUrl.TabIndex = 11;
            // 
            // buttonNewRecipeSave
            // 
            this.buttonNewRecipeSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNewRecipeSave.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonNewRecipeSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonNewRecipeSave.Font = new System.Drawing.Font("Calibri", 12F);
            this.buttonNewRecipeSave.Location = new System.Drawing.Point(596, 44);
            this.buttonNewRecipeSave.Name = "buttonNewRecipeSave";
            this.buttonNewRecipeSave.Size = new System.Drawing.Size(66, 27);
            this.buttonNewRecipeSave.TabIndex = 10;
            this.buttonNewRecipeSave.Text = "Zapisz";
            this.buttonNewRecipeSave.UseVisualStyleBackColor = false;
            this.buttonNewRecipeSave.Click += new System.EventHandler(this.buttonNewRecipeSave_Click);
            // 
            // richTextBoxNewRecipeDescription
            // 
            this.richTextBoxNewRecipeDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxNewRecipeDescription.Location = new System.Drawing.Point(313, 124);
            this.richTextBoxNewRecipeDescription.Name = "richTextBoxNewRecipeDescription";
            this.richTextBoxNewRecipeDescription.Size = new System.Drawing.Size(349, 423);
            this.richTextBoxNewRecipeDescription.TabIndex = 5;
            this.richTextBoxNewRecipeDescription.Text = "";
            // 
            // panelNewRecipeIngredients
            // 
            this.panelNewRecipeIngredients.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelNewRecipeIngredients.Controls.Add(this.comboBoxNewRecipeIngredientUnit);
            this.panelNewRecipeIngredients.Controls.Add(this.buttonNewRecipeIngredientAdd);
            this.panelNewRecipeIngredients.Controls.Add(this.labelNewRecipeIngredient);
            this.panelNewRecipeIngredients.Controls.Add(this.comboBoxNewRecipeIngredient);
            this.panelNewRecipeIngredients.Controls.Add(this.textBoxNewRecipeIngredient);
            this.panelNewRecipeIngredients.Location = new System.Drawing.Point(17, 124);
            this.panelNewRecipeIngredients.Name = "panelNewRecipeIngredients";
            this.panelNewRecipeIngredients.Size = new System.Drawing.Size(235, 67);
            this.panelNewRecipeIngredients.TabIndex = 4;
            this.panelNewRecipeIngredients.Visible = false;
            // 
            // comboBoxNewRecipeIngredientUnit
            // 
            this.comboBoxNewRecipeIngredientUnit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxNewRecipeIngredientUnit.Font = new System.Drawing.Font("Calibri", 10F);
            this.comboBoxNewRecipeIngredientUnit.FormattingEnabled = true;
            this.comboBoxNewRecipeIngredientUnit.Location = new System.Drawing.Point(74, 36);
            this.comboBoxNewRecipeIngredientUnit.Name = "comboBoxNewRecipeIngredientUnit";
            this.comboBoxNewRecipeIngredientUnit.Size = new System.Drawing.Size(123, 23);
            this.comboBoxNewRecipeIngredientUnit.TabIndex = 11;
            this.comboBoxNewRecipeIngredientUnit.Tag = "unit";
            // 
            // buttonNewRecipeIngredientAdd
            // 
            this.buttonNewRecipeIngredientAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNewRecipeIngredientAdd.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonNewRecipeIngredientAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonNewRecipeIngredientAdd.Font = new System.Drawing.Font("Calibri", 12F);
            this.buttonNewRecipeIngredientAdd.Location = new System.Drawing.Point(203, 34);
            this.buttonNewRecipeIngredientAdd.Name = "buttonNewRecipeIngredientAdd";
            this.buttonNewRecipeIngredientAdd.Size = new System.Drawing.Size(27, 27);
            this.buttonNewRecipeIngredientAdd.TabIndex = 9;
            this.buttonNewRecipeIngredientAdd.Tag = "button";
            this.buttonNewRecipeIngredientAdd.Text = "+";
            this.buttonNewRecipeIngredientAdd.UseVisualStyleBackColor = false;
            this.buttonNewRecipeIngredientAdd.Click += new System.EventHandler(this.buttonNewRecipeIngredientAdd_Click);
            // 
            // labelNewRecipeIngredient
            // 
            this.labelNewRecipeIngredient.AutoSize = true;
            this.labelNewRecipeIngredient.Font = new System.Drawing.Font("Calibri", 12F);
            this.labelNewRecipeIngredient.Location = new System.Drawing.Point(52, 39);
            this.labelNewRecipeIngredient.Name = "labelNewRecipeIngredient";
            this.labelNewRecipeIngredient.Size = new System.Drawing.Size(16, 19);
            this.labelNewRecipeIngredient.TabIndex = 10;
            this.labelNewRecipeIngredient.Tag = "";
            this.labelNewRecipeIngredient.Text = "x";
            // 
            // comboBoxNewRecipeIngredient
            // 
            this.comboBoxNewRecipeIngredient.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxNewRecipeIngredient.Font = new System.Drawing.Font("Calibri", 10F);
            this.comboBoxNewRecipeIngredient.FormattingEnabled = true;
            this.comboBoxNewRecipeIngredient.Location = new System.Drawing.Point(3, 7);
            this.comboBoxNewRecipeIngredient.Name = "comboBoxNewRecipeIngredient";
            this.comboBoxNewRecipeIngredient.Size = new System.Drawing.Size(227, 23);
            this.comboBoxNewRecipeIngredient.TabIndex = 3;
            this.comboBoxNewRecipeIngredient.Tag = "name";
            this.comboBoxNewRecipeIngredient.SelectedIndexChanged += new System.EventHandler(this.comboBoxNewRecipeIngredient_SelectedIndexChanged);
            // 
            // textBoxNewRecipeIngredient
            // 
            this.textBoxNewRecipeIngredient.AllowDrop = true;
            this.textBoxNewRecipeIngredient.Font = new System.Drawing.Font("Calibri", 10F);
            this.textBoxNewRecipeIngredient.Location = new System.Drawing.Point(3, 36);
            this.textBoxNewRecipeIngredient.Name = "textBoxNewRecipeIngredient";
            this.textBoxNewRecipeIngredient.Size = new System.Drawing.Size(43, 24);
            this.textBoxNewRecipeIngredient.TabIndex = 8;
            this.textBoxNewRecipeIngredient.Tag = "ammount";
            this.textBoxNewRecipeIngredient.Text = "1";
            this.textBoxNewRecipeIngredient.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNewRecipeDescripton
            // 
            this.labelNewRecipeDescripton.AutoSize = true;
            this.labelNewRecipeDescripton.Location = new System.Drawing.Point(310, 87);
            this.labelNewRecipeDescripton.Name = "labelNewRecipeDescripton";
            this.labelNewRecipeDescripton.Size = new System.Drawing.Size(44, 13);
            this.labelNewRecipeDescripton.TabIndex = 3;
            this.labelNewRecipeDescripton.Text = "Przepis:";
            // 
            // labelNewRecipeIngredients
            // 
            this.labelNewRecipeIngredients.AutoSize = true;
            this.labelNewRecipeIngredients.Location = new System.Drawing.Point(36, 87);
            this.labelNewRecipeIngredients.Name = "labelNewRecipeIngredients";
            this.labelNewRecipeIngredients.Size = new System.Drawing.Size(52, 13);
            this.labelNewRecipeIngredients.TabIndex = 2;
            this.labelNewRecipeIngredients.Text = "Składniki";
            // 
            // labelNewRecipeTitle
            // 
            this.labelNewRecipeTitle.AutoSize = true;
            this.labelNewRecipeTitle.Location = new System.Drawing.Point(18, 50);
            this.labelNewRecipeTitle.Name = "labelNewRecipeTitle";
            this.labelNewRecipeTitle.Size = new System.Drawing.Size(35, 13);
            this.labelNewRecipeTitle.TabIndex = 1;
            this.labelNewRecipeTitle.Text = "Tytuł:";
            // 
            // textBoxNewRecipeTitle
            // 
            this.textBoxNewRecipeTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNewRecipeTitle.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxNewRecipeTitle.Location = new System.Drawing.Point(55, 40);
            this.textBoxNewRecipeTitle.Name = "textBoxNewRecipeTitle";
            this.textBoxNewRecipeTitle.Size = new System.Drawing.Size(535, 31);
            this.textBoxNewRecipeTitle.TabIndex = 0;
            // 
            // tabPageRecipe
            // 
            this.tabPageRecipe.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageRecipe.Controls.Add(this.richTextBoxRecipeSource);
            this.tabPageRecipe.Controls.Add(this.labelRecipeIngredientSource);
            this.tabPageRecipe.Controls.Add(this.richTextBoxRecipeDescription);
            this.tabPageRecipe.Controls.Add(this.panelRecipeIngredient);
            this.tabPageRecipe.Controls.Add(this.labelRecipeDescription);
            this.tabPageRecipe.Controls.Add(this.labelRecipeIngredients);
            this.tabPageRecipe.Controls.Add(this.labelRecipeTitle);
            this.tabPageRecipe.Location = new System.Drawing.Point(4, 22);
            this.tabPageRecipe.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageRecipe.Name = "tabPageRecipe";
            this.tabPageRecipe.Size = new System.Drawing.Size(682, 585);
            this.tabPageRecipe.TabIndex = 4;
            this.tabPageRecipe.Text = "Przepis";
            // 
            // richTextBoxRecipeSource
            // 
            this.richTextBoxRecipeSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxRecipeSource.BackColor = System.Drawing.Color.MintCream;
            this.richTextBoxRecipeSource.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxRecipeSource.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.richTextBoxRecipeSource.Enabled = false;
            this.richTextBoxRecipeSource.Font = new System.Drawing.Font("Calibri", 10F);
            this.richTextBoxRecipeSource.Location = new System.Drawing.Point(289, 556);
            this.richTextBoxRecipeSource.Multiline = false;
            this.richTextBoxRecipeSource.Name = "richTextBoxRecipeSource";
            this.richTextBoxRecipeSource.ReadOnly = true;
            this.richTextBoxRecipeSource.Size = new System.Drawing.Size(385, 17);
            this.richTextBoxRecipeSource.TabIndex = 24;
            this.richTextBoxRecipeSource.Text = "";
            // 
            // labelRecipeIngredientSource
            // 
            this.labelRecipeIngredientSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRecipeIngredientSource.AutoSize = true;
            this.labelRecipeIngredientSource.Location = new System.Drawing.Point(241, 557);
            this.labelRecipeIngredientSource.Name = "labelRecipeIngredientSource";
            this.labelRecipeIngredientSource.Size = new System.Drawing.Size(42, 13);
            this.labelRecipeIngredientSource.TabIndex = 23;
            this.labelRecipeIngredientSource.Text = "Źródło:";
            // 
            // richTextBoxRecipeDescription
            // 
            this.richTextBoxRecipeDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxRecipeDescription.BackColor = System.Drawing.Color.MintCream;
            this.richTextBoxRecipeDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxRecipeDescription.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.richTextBoxRecipeDescription.Enabled = false;
            this.richTextBoxRecipeDescription.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBoxRecipeDescription.Location = new System.Drawing.Point(244, 124);
            this.richTextBoxRecipeDescription.Name = "richTextBoxRecipeDescription";
            this.richTextBoxRecipeDescription.ReadOnly = true;
            this.richTextBoxRecipeDescription.Size = new System.Drawing.Size(430, 423);
            this.richTextBoxRecipeDescription.TabIndex = 20;
            this.richTextBoxRecipeDescription.Text = "";
            // 
            // panelRecipeIngredient
            // 
            this.panelRecipeIngredient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRecipeIngredient.Controls.Add(this.labelRecipeIngredientName);
            this.panelRecipeIngredient.Controls.Add(this.labelRecipeIngredientUnit);
            this.panelRecipeIngredient.Controls.Add(this.labelRecipeIngredientAmmmount);
            this.panelRecipeIngredient.Controls.Add(this.labelRecipeIngredientSeparator);
            this.panelRecipeIngredient.Location = new System.Drawing.Point(3, 124);
            this.panelRecipeIngredient.Name = "panelRecipeIngredient";
            this.panelRecipeIngredient.Size = new System.Drawing.Size(235, 67);
            this.panelRecipeIngredient.TabIndex = 19;
            this.panelRecipeIngredient.Visible = false;
            // 
            // labelRecipeIngredientName
            // 
            this.labelRecipeIngredientName.BackColor = System.Drawing.Color.MintCream;
            this.labelRecipeIngredientName.Font = new System.Drawing.Font("Calibri", 14F);
            this.labelRecipeIngredientName.Location = new System.Drawing.Point(3, 6);
            this.labelRecipeIngredientName.Name = "labelRecipeIngredientName";
            this.labelRecipeIngredientName.Size = new System.Drawing.Size(227, 19);
            this.labelRecipeIngredientName.TabIndex = 25;
            this.labelRecipeIngredientName.Tag = "title";
            this.labelRecipeIngredientName.Text = "Title";
            this.labelRecipeIngredientName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRecipeIngredientUnit
            // 
            this.labelRecipeIngredientUnit.BackColor = System.Drawing.Color.MintCream;
            this.labelRecipeIngredientUnit.Font = new System.Drawing.Font("Calibri", 12F);
            this.labelRecipeIngredientUnit.Location = new System.Drawing.Point(104, 40);
            this.labelRecipeIngredientUnit.Name = "labelRecipeIngredientUnit";
            this.labelRecipeIngredientUnit.Size = new System.Drawing.Size(126, 19);
            this.labelRecipeIngredientUnit.TabIndex = 25;
            this.labelRecipeIngredientUnit.Tag = "unit";
            this.labelRecipeIngredientUnit.Text = "Unit";
            this.labelRecipeIngredientUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRecipeIngredientAmmmount
            // 
            this.labelRecipeIngredientAmmmount.BackColor = System.Drawing.Color.MintCream;
            this.labelRecipeIngredientAmmmount.Font = new System.Drawing.Font("Calibri", 12F);
            this.labelRecipeIngredientAmmmount.Location = new System.Drawing.Point(3, 40);
            this.labelRecipeIngredientAmmmount.Name = "labelRecipeIngredientAmmmount";
            this.labelRecipeIngredientAmmmount.Size = new System.Drawing.Size(73, 19);
            this.labelRecipeIngredientAmmmount.TabIndex = 24;
            this.labelRecipeIngredientAmmmount.Tag = "ammount";
            this.labelRecipeIngredientAmmmount.Text = "12";
            this.labelRecipeIngredientAmmmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRecipeIngredientSeparator
            // 
            this.labelRecipeIngredientSeparator.AutoSize = true;
            this.labelRecipeIngredientSeparator.Font = new System.Drawing.Font("Calibri", 12F);
            this.labelRecipeIngredientSeparator.Location = new System.Drawing.Point(82, 40);
            this.labelRecipeIngredientSeparator.Name = "labelRecipeIngredientSeparator";
            this.labelRecipeIngredientSeparator.Size = new System.Drawing.Size(16, 19);
            this.labelRecipeIngredientSeparator.TabIndex = 10;
            this.labelRecipeIngredientSeparator.Tag = "";
            this.labelRecipeIngredientSeparator.Text = "x";
            // 
            // labelRecipeDescription
            // 
            this.labelRecipeDescription.AutoSize = true;
            this.labelRecipeDescription.Location = new System.Drawing.Point(241, 108);
            this.labelRecipeDescription.Name = "labelRecipeDescription";
            this.labelRecipeDescription.Size = new System.Drawing.Size(44, 13);
            this.labelRecipeDescription.TabIndex = 18;
            this.labelRecipeDescription.Text = "Przepis:";
            // 
            // labelRecipeIngredients
            // 
            this.labelRecipeIngredients.AutoSize = true;
            this.labelRecipeIngredients.Location = new System.Drawing.Point(4, 108);
            this.labelRecipeIngredients.Name = "labelRecipeIngredients";
            this.labelRecipeIngredients.Size = new System.Drawing.Size(55, 13);
            this.labelRecipeIngredients.TabIndex = 17;
            this.labelRecipeIngredients.Text = "Składniki:";
            // 
            // labelRecipeTitle
            // 
            this.labelRecipeTitle.AutoSize = true;
            this.labelRecipeTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.labelRecipeTitle.Location = new System.Drawing.Point(71, 35);
            this.labelRecipeTitle.Name = "labelRecipeTitle";
            this.labelRecipeTitle.Size = new System.Drawing.Size(106, 46);
            this.labelRecipeTitle.TabIndex = 14;
            this.labelRecipeTitle.Text = "Tytuł";
            // 
            // tabPageIngredients
            // 
            this.tabPageIngredients.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageIngredients.Controls.Add(this.panelIngredients);
            this.tabPageIngredients.Controls.Add(this.labelIngredientsPriceOwned);
            this.tabPageIngredients.Controls.Add(this.labelIngredientsNameOwned);
            this.tabPageIngredients.Controls.Add(this.buttonIngredientsAdd);
            this.tabPageIngredients.Controls.Add(this.textBoxIngredientsPrice);
            this.tabPageIngredients.Controls.Add(this.labelIngredientsPrice);
            this.tabPageIngredients.Controls.Add(this.textBoxIngredientsName);
            this.tabPageIngredients.Controls.Add(this.labelIngredietnsName);
            this.tabPageIngredients.Controls.Add(this.labelIngredients);
            this.tabPageIngredients.Location = new System.Drawing.Point(4, 22);
            this.tabPageIngredients.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageIngredients.Name = "tabPageIngredients";
            this.tabPageIngredients.Size = new System.Drawing.Size(682, 585);
            this.tabPageIngredients.TabIndex = 2;
            this.tabPageIngredients.Text = "Składniki";
            // 
            // panelIngredients
            // 
            this.panelIngredients.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelIngredients.Controls.Add(this.textBoxIngredientsPriceAdded);
            this.panelIngredients.Controls.Add(this.textBoxIngredientsNameAdded);
            this.panelIngredients.Location = new System.Drawing.Point(96, 220);
            this.panelIngredients.Name = "panelIngredients";
            this.panelIngredients.Size = new System.Drawing.Size(491, 26);
            this.panelIngredients.TabIndex = 41;
            this.panelIngredients.Visible = false;
            // 
            // textBoxIngredientsPriceAdded
            // 
            this.textBoxIngredientsPriceAdded.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxIngredientsPriceAdded.Location = new System.Drawing.Point(348, 3);
            this.textBoxIngredientsPriceAdded.Name = "textBoxIngredientsPriceAdded";
            this.textBoxIngredientsPriceAdded.Size = new System.Drawing.Size(138, 20);
            this.textBoxIngredientsPriceAdded.TabIndex = 25;
            this.textBoxIngredientsPriceAdded.Tag = "Price";
            // 
            // textBoxIngredientsNameAdded
            // 
            this.textBoxIngredientsNameAdded.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxIngredientsNameAdded.Enabled = false;
            this.textBoxIngredientsNameAdded.Location = new System.Drawing.Point(3, 3);
            this.textBoxIngredientsNameAdded.Name = "textBoxIngredientsNameAdded";
            this.textBoxIngredientsNameAdded.Size = new System.Drawing.Size(339, 20);
            this.textBoxIngredientsNameAdded.TabIndex = 24;
            this.textBoxIngredientsNameAdded.Tag = "Name";
            // 
            // labelIngredientsPriceOwned
            // 
            this.labelIngredientsPriceOwned.AutoSize = true;
            this.labelIngredientsPriceOwned.Location = new System.Drawing.Point(513, 192);
            this.labelIngredientsPriceOwned.Name = "labelIngredientsPriceOwned";
            this.labelIngredientsPriceOwned.Size = new System.Drawing.Size(35, 13);
            this.labelIngredientsPriceOwned.TabIndex = 39;
            this.labelIngredientsPriceOwned.Text = "Cena:";
            // 
            // labelIngredientsNameOwned
            // 
            this.labelIngredientsNameOwned.AutoSize = true;
            this.labelIngredientsNameOwned.Location = new System.Drawing.Point(182, 192);
            this.labelIngredientsNameOwned.Name = "labelIngredientsNameOwned";
            this.labelIngredientsNameOwned.Size = new System.Drawing.Size(43, 13);
            this.labelIngredientsNameOwned.TabIndex = 40;
            this.labelIngredientsNameOwned.Text = "Nazwa:";
            // 
            // buttonIngredientsAdd
            // 
            this.buttonIngredientsAdd.Location = new System.Drawing.Point(451, 130);
            this.buttonIngredientsAdd.Name = "buttonIngredientsAdd";
            this.buttonIngredientsAdd.Size = new System.Drawing.Size(97, 23);
            this.buttonIngredientsAdd.TabIndex = 38;
            this.buttonIngredientsAdd.Text = "Dodaj";
            this.buttonIngredientsAdd.UseVisualStyleBackColor = true;
            this.buttonIngredientsAdd.Click += new System.EventHandler(this.buttonIngredientsAdd_Click);
            // 
            // textBoxIngredientsPrice
            // 
            this.textBoxIngredientsPrice.Location = new System.Drawing.Point(205, 133);
            this.textBoxIngredientsPrice.Name = "textBoxIngredientsPrice";
            this.textBoxIngredientsPrice.Size = new System.Drawing.Size(220, 20);
            this.textBoxIngredientsPrice.TabIndex = 37;
            // 
            // labelIngredientsPrice
            // 
            this.labelIngredientsPrice.AutoSize = true;
            this.labelIngredientsPrice.Location = new System.Drawing.Point(154, 135);
            this.labelIngredientsPrice.Name = "labelIngredientsPrice";
            this.labelIngredientsPrice.Size = new System.Drawing.Size(35, 13);
            this.labelIngredientsPrice.TabIndex = 36;
            this.labelIngredientsPrice.Text = "Cena:";
            // 
            // textBoxIngredientsName
            // 
            this.textBoxIngredientsName.Location = new System.Drawing.Point(205, 108);
            this.textBoxIngredientsName.Name = "textBoxIngredientsName";
            this.textBoxIngredientsName.Size = new System.Drawing.Size(220, 20);
            this.textBoxIngredientsName.TabIndex = 35;
            // 
            // labelIngredietnsName
            // 
            this.labelIngredietnsName.AutoSize = true;
            this.labelIngredietnsName.Location = new System.Drawing.Point(146, 108);
            this.labelIngredietnsName.Name = "labelIngredietnsName";
            this.labelIngredietnsName.Size = new System.Drawing.Size(43, 13);
            this.labelIngredietnsName.TabIndex = 34;
            this.labelIngredietnsName.Text = "Nazwa:";
            // 
            // labelIngredients
            // 
            this.labelIngredients.AutoSize = true;
            this.labelIngredients.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F);
            this.labelIngredients.Location = new System.Drawing.Point(239, 31);
            this.labelIngredients.Name = "labelIngredients";
            this.labelIngredients.Size = new System.Drawing.Size(246, 63);
            this.labelIngredients.TabIndex = 33;
            this.labelIngredients.Text = "Składniki";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 611);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.buttonIngredients);
            this.Controls.Add(this.buttonRecipes);
            this.Controls.Add(this.buttonFridge);
            this.Controls.Add(this.splitterMenu);
            this.MinimumSize = new System.Drawing.Size(900, 650);
            this.Name = "FormMain";
            this.Text = "Studencki Pomocnik Kulinarny";
            this.tabControlMain.ResumeLayout(false);
            this.tabPageFridge.ResumeLayout(false);
            this.tabPageFridge.PerformLayout();
            this.panelOwnedIngredientAdd.ResumeLayout(false);
            this.panelOwnedIngredientAdd.PerformLayout();
            this.panelOwnedIngredient.ResumeLayout(false);
            this.panelOwnedIngredient.PerformLayout();
            this.tabPageRecipes.ResumeLayout(false);
            this.tabPageRecipes.PerformLayout();
            this.panelRecipeSearchResult.ResumeLayout(false);
            this.tabPageNewRecipe.ResumeLayout(false);
            this.tabPageNewRecipe.PerformLayout();
            this.panelNewRecipeIngredients.ResumeLayout(false);
            this.panelNewRecipeIngredients.PerformLayout();
            this.tabPageRecipe.ResumeLayout(false);
            this.tabPageRecipe.PerformLayout();
            this.panelRecipeIngredient.ResumeLayout(false);
            this.panelRecipeIngredient.PerformLayout();
            this.tabPageIngredients.ResumeLayout(false);
            this.tabPageIngredients.PerformLayout();
            this.panelIngredients.ResumeLayout(false);
            this.panelIngredients.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter splitterMenu;
        private System.Windows.Forms.Button buttonFridge;
        private System.Windows.Forms.Button buttonRecipes;
        private System.Windows.Forms.Button buttonIngredients;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageFridge;
        private System.Windows.Forms.TabPage tabPageRecipes;
        private System.Windows.Forms.TabPage tabPageIngredients;
        private System.Windows.Forms.Panel panelOwnedIngredientAdd;
        private System.Windows.Forms.Button buttonOwnedIngredientAdd;
        private System.Windows.Forms.Label labelOwnedIngredientAdd;
        private System.Windows.Forms.Panel panelOwnedIngredient;
        private System.Windows.Forms.TextBox textBoxOwnedIngredientUnitAmmount;
        private System.Windows.Forms.Label labelOwnedIngredientUnit;
        private System.Windows.Forms.Button buttonOwnedIngredientDelete;
        private System.Windows.Forms.Label labelOwnedIngredient;
        private System.Windows.Forms.Panel panelRecipeSearchResult;
        private System.Windows.Forms.TextBox textBoxRecipeSearch;
        private System.Windows.Forms.Label labelRecipeSearchResultTitle;
        private System.Windows.Forms.TabPage tabPageRecipe;
        private System.Windows.Forms.ComboBox comboBoxOwnedIngredientAdd;
        private System.Windows.Forms.TextBox textBoxOwnedIngredientAddUnitAmmount;
        private System.Windows.Forms.Label labelRecipeTitle;
        private System.Windows.Forms.Button buttonRecipesAdd;
        private System.Windows.Forms.TabPage tabPageNewRecipe;
        private System.Windows.Forms.TextBox textBoxNewRecipeIngredient;
        private System.Windows.Forms.Panel panelNewRecipeIngredients;
        private System.Windows.Forms.ComboBox comboBoxNewRecipeIngredient;
        private System.Windows.Forms.Label labelNewRecipeDescripton;
        private System.Windows.Forms.Label labelNewRecipeIngredients;
        private System.Windows.Forms.Label labelNewRecipeTitle;
        private System.Windows.Forms.TextBox textBoxNewRecipeTitle;
        private System.Windows.Forms.RichTextBox richTextBoxNewRecipeDescription;
        private System.Windows.Forms.ComboBox comboBoxNewRecipeIngredientUnit;
        private System.Windows.Forms.Button buttonNewRecipeIngredientAdd;
        private System.Windows.Forms.Label labelNewRecipeIngredient;
        private System.Windows.Forms.Button buttonNewRecipeSave;
        private System.Windows.Forms.ComboBox comboBoxOwnedIngredientAddUnit;
        private System.Windows.Forms.ComboBox comboBoxOwnedIngredientUnit;
        private System.Windows.Forms.Label labelNewRecipeOriginalUrl;
        private System.Windows.Forms.TextBox textBoxNewRecipeOriginalUrl;
        private System.Windows.Forms.Label labelRecipeIngredientSource;
        private System.Windows.Forms.RichTextBox richTextBoxRecipeDescription;
        private System.Windows.Forms.Panel panelRecipeIngredient;
        private System.Windows.Forms.Label labelRecipeIngredientSeparator;
        private System.Windows.Forms.Label labelRecipeDescription;
        private System.Windows.Forms.Label labelRecipeIngredients;
        private System.Windows.Forms.Label labelRecipeIngredientName;
        private System.Windows.Forms.Label labelRecipeIngredientUnit;
        private System.Windows.Forms.Label labelRecipeIngredientAmmmount;
        private System.Windows.Forms.RichTextBox richTextBoxRecipeSource;
        private System.Windows.Forms.Label labelFridge;
        private System.Windows.Forms.Panel panelIngredients;
        private System.Windows.Forms.TextBox textBoxIngredientsPriceAdded;
        private System.Windows.Forms.TextBox textBoxIngredientsNameAdded;
        private System.Windows.Forms.Label labelIngredientsPriceOwned;
        private System.Windows.Forms.Label labelIngredientsNameOwned;
        private System.Windows.Forms.Button buttonIngredientsAdd;
        private System.Windows.Forms.TextBox textBoxIngredientsPrice;
        private System.Windows.Forms.Label labelIngredientsPrice;
        private System.Windows.Forms.TextBox textBoxIngredientsName;
        private System.Windows.Forms.Label labelIngredietnsName;
        private System.Windows.Forms.Label labelIngredients;
    }
}

