﻿using System;

namespace SPKBeta
{
    [Serializable]
    public class UnitLiquid : Unit
    {
        public int UnitInMililiters { get; set; }
    }
}
