#pragma once
#include "Unit.h"

namespace SPK {

	using namespace System;

	public ref class UnitLiquid : public Unit
	{
	public:
		int UnitInMililiters;

	public:
		UnitLiquid();
		UnitLiquid(int id, String^ name, int unitInMililiters);
	};

}