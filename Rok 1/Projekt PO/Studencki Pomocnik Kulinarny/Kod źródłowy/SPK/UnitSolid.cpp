#include "UnitSolid.h"

using namespace SPK;

SPK::UnitSolid::UnitSolid()
{
}

SPK::UnitSolid::UnitSolid(int id, String ^ name, int unitInGrams)
{
	Id = id;
	Name = name;
	UnitInGrams = unitInGrams;
}
