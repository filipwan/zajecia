#pragma once
#include "Unit.h"

namespace SPK {

	using namespace System;

	public ref class Ingredient
	{
	public:
		int Id;
		String^ Name;
		Unit^ BaseUnit;
		double PricePerUnit;
		double UnitsOwned;

	public:
		Ingredient();
	};

}