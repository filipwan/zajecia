#pragma once

namespace SPK {

using namespace System;

public interface class ISortable
{

		void sort();
};

}

