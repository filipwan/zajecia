#include "MyForm.h"

using namespace std;
using namespace SPK;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Windows::Forms;

[STAThreadAttribute]

int main()
{
	FreeConsole();
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	SPK::MyForm form;
	Application::Run(%form);

	return 0;
}

//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd)
//{
//	Application::EnableVisualStyles();
//	Application::SetCompatibleTextRenderingDefault(false);
//	SPK::MyForm form;
//	Application::Run(%form);
//
//	return 0;
//}

/// <summary>
/// Inicjalizacja programu
/// </summary>
Void MyForm::MyForm_Load(System::Object^  sender, System::EventArgs^  e)
{
	ingredientList = gcnew List<Ingredient^>;
	recipeList = gcnew List<Recipe^>;
	OwnedIngredientPanels = gcnew List<Panel^>;
	RecipeSearchResultPanels = gcnew List<Panel^>;
	NewRecipeIngredientPanels = gcnew List<Panel^>;
	RecipeIngredientPanels = gcnew List<Panel^>;

	unitList = gcnew List<Unit^>;
#pragma region unitList

	unitList->Add( gcnew  UnitLiquid
	(
		0,
		"Mililitry",
		1
	));
	unitList->Add( gcnew  UnitLiquid
	(
		1,
		"Litry",
		1000
	));
	unitList->Add( gcnew  UnitLiquid
	(
		2,
		"Szklanka",
		250
	));
	unitList->Add( gcnew  UnitLiquid
	(
		3,
		"Decylitry",
		1
	));
	unitList->Add( gcnew  UnitLiquid
	(
		4,
		"�y�ka",
		15
	));
	unitList->Add( gcnew  UnitLiquid
	(
		5,
		"�y�eczka",
		5
	));
	unitList->Add( gcnew  UnitSolid
	(
		6,
		"Gram",
		1
	));
	unitList->Add( gcnew  UnitSolid
	(
		7,
		"Dekagram",
		10
	));
	unitList->Add( gcnew  UnitSolid
	(
		8,
		"Kilogram",
		1000
	));
	unitList->Add( gcnew  UnitIncalculable
	(
		9,
		"Sztuka"
	));
	unitList->Add( gcnew  UnitIncalculable
	(
		10,
		"Szklanka"
	));
	unitList->Add( gcnew  UnitIncalculable
	(
		11,
		"�y�ka"
	));
	unitList->Add(gcnew  UnitIncalculable
	(
		12,
		"�y�eczka"
	));

#pragma endregion UnitList

	DatabaseLogic::LoadList(ingredientList);
	DatabaseLogic::LoadList(recipeList);

	LoadIngredientsToFridge();
	LoadRecipesToRecipes();
	tabControlMain->SizeMode = TabSizeMode::Fixed;
	tabControlMain->Appearance = TabAppearance::FlatButtons;
}

#pragma region Fridge

/// <summary>
/// Wczytuje posiadane sk�adniki do zak�adki Lod�wka
/// </summary>
Void MyForm::LoadIngredientsToFridge()
{
	if (OwnedIngredientPanels != nullptr)
	{
		for each(Panel^ item in OwnedIngredientPanels)
		{
			delete item;
		}
		OwnedIngredientPanels->Clear();
	}
	else OwnedIngredientPanels = gcnew List<Panel^>;


	comboBoxOwnedIngredientAdd->DataSource = FormCreator::ChangeListToStringList(ingredientList);
	comboBoxOwnedIngredientAddUnit->DataSource = FormCreator::ChangeListToStringList(unitList);

	for each(Ingredient^ ingredient in ingredientList)
	{
		if (ingredient->UnitsOwned != 0)
		{
			Panel^ panel = FormCreator::ClonePanel(panelOwnedIngredient);
			panel->Tag = ingredient->Id.ToString();

			for each(Control^ control in panel->Controls)
			{
				if (control->Tag == labelOwnedIngredient->Tag)
				{
					control->Text = ingredient->Name;
				}
				else if (control->Tag == textBoxOwnedIngredientUnitAmmount->Tag)
				{
					control->Text = ingredient->UnitsOwned.ToString();
					control->TextChanged += gcnew System::EventHandler(this, &MyForm::textBoxOwnedIngredientUnitAmmount_TextChanged);
				}
				else if (control->Tag == comboBoxOwnedIngredientUnit->Tag)
				{
					ComboBox^ comboBox = dynamic_cast<ComboBox^>(control);
					comboBox->Text = ingredient->BaseUnit->Name;
				}
				else if (control->Tag == buttonOwnedIngredientDelete->Tag)
				{
					control->Click += gcnew System::EventHandler(this, &MyForm::buttonOwnedIngredientDelete_Click);
				}
			}

			if (OwnedIngredientPanels == nullptr || OwnedIngredientPanels->Count == 0)
			{
				OwnedIngredientPanels = gcnew List<Panel^>();
			}
			else
			{
				Point point = OwnedIngredientPanels[OwnedIngredientPanels->Count - 1]->Location;
				point.Y += OwnedIngredientPanels[OwnedIngredientPanels->Count - 1]->Height + PanelsMargin;
				panel->Location = point;
			}
			OwnedIngredientPanels->Add(panel);
			tabPageFridge->Controls->Add(panel);
		}
	}
}

Ingredient^ MyForm::AddNewIngredient(String^ Name)
{
	Ingredient^ i = gcnew Ingredient();
	if (ingredientList == nullptr || ingredientList->Count == 0)
		i->Id = 0;
	else
		i->Id = (int)(ingredientList[ingredientList->Count - 1]->Id) + 1;
	i->Name = Name;
	return i;
}

Void MyForm::buttonOwnedIngredientAdd_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (textBoxOwnedIngredientAddUnitAmmount->Text == String::Empty || comboBoxOwnedIngredientAdd->Text == String::Empty) return;

	if (comboBoxOwnedIngredientAdd->SelectedIndex != -1 && ingredientList[comboBoxOwnedIngredientAdd->SelectedIndex]->Name == comboBoxOwnedIngredientAdd->Text)
	{
		ingredientList[comboBoxOwnedIngredientAdd->SelectedIndex]->UnitsOwned += Double::Parse(textBoxOwnedIngredientAddUnitAmmount->Text);
	}
	else
	{
		for each(Ingredient^ ingredient in ingredientList)
		{
			if (ingredient->Name == comboBoxOwnedIngredientAdd->Text) {
				MessageBox::Show("SK�ADNIK JU� ISTNIEJE");
				return;
			}
		}

		Ingredient^ i = AddNewIngredient(comboBoxOwnedIngredientAdd->Text);
		i->UnitsOwned = Int32::Parse(textBoxOwnedIngredientAddUnitAmmount->Text);
		i->BaseUnit = unitList[comboBoxOwnedIngredientAddUnit->SelectedIndex];
		ingredientList->Add(i);
	}
	DatabaseLogic::SaveList(ingredientList);
	LoadIngredientsToFridge();
}

System::Void SPK::MyForm::buttonOwnedIngredientDelete_Click(System::Object ^ sender, System::EventArgs ^ e)
{
	Button^ button = dynamic_cast<Button^>(sender);
	int id = Int32::Parse(button->Parent->Tag->ToString());
	for each (Ingredient^ ingredient in ingredientList)
	{
		if (ingredient->Id == id)
		{
			ingredient->UnitsOwned = 0;
			break;
		}
	}
	DatabaseLogic::SaveList(ingredientList);
	LoadIngredientsToFridge();
}

System::Void SPK::MyForm::textBoxOwnedIngredientUnitAmmount_TextChanged(System::Object ^ sender, System::EventArgs ^ e)
{
	Control^ control = dynamic_cast<Control^>(sender);
	if (control->Text == String::Empty) return;

	int id = Int32::Parse(control->Parent->Tag->ToString());

	for each (Ingredient^ ingredient in ingredientList)
	{
		if (ingredient->Id == id)
		{
			ingredient->UnitsOwned = Double::Parse(control->Text);
			break;
		}
	}
	DatabaseLogic::SaveList(ingredientList);
	if(Double::Parse(control->Text) == 0)
		LoadIngredientsToFridge();
}

#pragma endregion Fridge

#pragma region Recipes

Void MyForm::LoadRecipesToRecipes()
{
	if (RecipeSearchResultPanels != nullptr)
	{
		for each(Panel^ item in RecipeSearchResultPanels)
		{
			delete item;
		}
		RecipeSearchResultPanels->Clear();
	}
	else RecipeSearchResultPanels = gcnew List<Panel^>;

	for each(Recipe^ recipe in recipeList)
	{
		Panel^ panel = FormCreator::ClonePanel(panelRecipeSearchResult);
		panel->Tag = recipe->Id.ToString();
		panel->Click += gcnew System::EventHandler(this, &MyForm::panelRecipeSearchResult_Click);

		for each(Control^ control in panel->Controls)
		{
			if (control->Tag == labelRecipeSearchResultTitle->Tag)
			{
				control->Text = recipe->Title;
				control->Click += gcnew System::EventHandler(this, &MyForm::panelRecipeSearchResult_Click);
			}
		}
		if (RecipeSearchResultPanels == nullptr || RecipeSearchResultPanels->Count == 0)
		{
			RecipeSearchResultPanels = gcnew List<Panel^>();
		}
		else
		{
			Point point = RecipeSearchResultPanels[RecipeSearchResultPanels->Count - 1]->Location;
			point.Y += RecipeSearchResultPanels[RecipeSearchResultPanels->Count - 1]->Height + PanelsMargin;
			panel->Location = point;
		}
		RecipeSearchResultPanels->Add(panel);
		tabPageRecipes->Controls->Add(panel);
	}
}

System::Void SPK::MyForm::panelRecipeSearchResult_Click(System::Object ^ sender, System::EventArgs ^ e)
{
	int id;
	if(dynamic_cast<Control^>(sender)->Tag != labelRecipeSearchResultTitle->Tag)
		id = Int32::Parse(dynamic_cast<Control^>(sender)->Tag->ToString());
	else
		id = Int32::Parse(dynamic_cast<Control^>(sender)->Parent->Tag->ToString());
	for each(Recipe^ r in recipeList)
	{
		if( r->Id == id)
		{
			LoadRecipeToRecipe(r);
			break;
		}
	}
}

#pragma endregion Recipes

#pragma region NewRecipe


Void MyForm::InitializeNewRecipe()
{
	newRecipeIngredients = gcnew List<IngredientInRecipe^>();
	comboBoxNewRecipeIngredient->DataSource = gcnew List<Ingredient^>(ingredientList);
	comboBoxNewRecipeIngredient->DisplayMember = "Name";
	comboBoxNewRecipeIngredientUnit->DataSource = gcnew List<Unit^>(unitList);
	if (NewRecipeIngredientPanels != nullptr)
	{
		for each(Panel^ item in NewRecipeIngredientPanels)
		{
			delete item;
		}
		NewRecipeIngredientPanels->Clear();
	}
	CreateNewRecipeIngredientPanel();
	
}

Panel^ MyForm::CreateNewRecipeIngredientPanel()
{
	Panel^ panel = FormCreator::ClonePanel(panelNewRecipeIngredients);
	for each(Control^ control in panel->Controls)
	{
		if (control->Tag == comboBoxNewRecipeIngredient->Tag)
		{
			dynamic_cast<ComboBox^>(control)->DataSource = FormCreator::ChangeListToStringList(ingredientList);
		}
		else if (control->Tag == comboBoxNewRecipeIngredientUnit->Tag)
		{
			dynamic_cast<ComboBox^>(control)->DataSource = FormCreator::ChangeListToStringList(unitList);
		}
		else if (control->Tag == buttonNewRecipeIngredientAdd->Tag)
		{
			control->Click += gcnew System::EventHandler(this, &MyForm::buttonNewRecipeIngredientAdd_Click);
		}
	}
	if (NewRecipeIngredientPanels == nullptr || NewRecipeIngredientPanels->Count == 0)
	{
		NewRecipeIngredientPanels = gcnew List<Panel^>();
	}
	else
	{
		Point point = NewRecipeIngredientPanels[NewRecipeIngredientPanels->Count - 1]->Location;
		point.Y += NewRecipeIngredientPanels[NewRecipeIngredientPanels->Count - 1]->Height + PanelsMargin;
		panel->Location = point;
	}
	NewRecipeIngredientPanels->Add(panel);
	tabPageNewRecipe->Controls->Add(panel);
	return panel;
}

Void MyForm::buttonNewRecipeIngredientAdd_Click(System::Object^  sender, System::EventArgs^  e)
{
	Button^ button = dynamic_cast<Button^>(sender);
	ComboBox^ comboBoxName;
	TextBox^ textBoxAmmount;
	ComboBox^ comboBoxUnit;
	for each(Control^ control in button->Parent->Controls)
	{
		if (control->Tag == comboBoxNewRecipeIngredient->Tag)
		{
			comboBoxName = dynamic_cast<ComboBox^>(control);
		}
		else if (control->Tag == textBoxNewRecipeIngredient->Tag)
		{
			textBoxAmmount = dynamic_cast<TextBox^>(control);
		}
		else if (control->Tag == comboBoxNewRecipeIngredientUnit->Tag)
		{
			comboBoxUnit = dynamic_cast<ComboBox^>(control);
		}
	}

	if (comboBoxName->SelectedItem == nullptr || textBoxAmmount == nullptr || comboBoxUnit->SelectedItem == nullptr)
		return;

	button->Parent->Tag = comboBoxName->SelectedIndex;

	Ingredient^ oryginalIngredient = ingredientList[comboBoxName->SelectedIndex];

	IngredientInRecipe^ ingredientInRecipe = gcnew IngredientInRecipe();
	ingredientInRecipe->Ingredient = oryginalIngredient;

	ingredientInRecipe->Ammount = Double::Parse(textBoxAmmount->Text);

	ingredientInRecipe->Unit = unitList[comboBoxNewRecipeIngredientUnit->SelectedIndex];

	newRecipeIngredients->Add(ingredientInRecipe);
	button->Text = "X";
	button->Click -= gcnew System::EventHandler(this, &MyForm::buttonNewRecipeIngredientAdd_Click);
	button->Click += gcnew System::EventHandler(this, &MyForm::buttonNewRecipeIngredientRemove_Click);

	CreateNewRecipeIngredientPanel();
}

Void MyForm::buttonNewRecipeIngredientRemove_Click(System::Object^  sender, System::EventArgs^  e)
{
	Button^ button = dynamic_cast<Button^>(sender);

	for each(IngredientInRecipe^ i in newRecipeIngredients)
	{
		if(i->Ingredient->Id == Int32::Parse(button->Parent->Tag->ToString()))
		{
			newRecipeIngredients->Remove(i);
			break;
		}
	}

	if (NewRecipeIngredientPanels != nullptr)
	{
		for each(Panel^ item in NewRecipeIngredientPanels)
		{
			delete item;
		}
		NewRecipeIngredientPanels->Clear();
	}

	for each(IngredientInRecipe^ ingredient in newRecipeIngredients)
	{
		if (ingredient->Ammount != 0)
		{
			Panel^ panel = CreateNewRecipeIngredientPanel();
			panel->Tag = ingredient->Ingredient->Id.ToString();

			for each(Control^ control in panel->Controls)
			{
				if (control->Tag == comboBoxNewRecipeIngredient->Tag)
				{
					control->Text = ingredient->Ingredient->Name;
				}
				else if (control->Tag == textBoxNewRecipeIngredient->Tag)
				{
					control->Text = ingredient->Ammount.ToString();
				}
				else if (control->Tag == comboBoxNewRecipeIngredientUnit->Tag)
				{
					control->Text = ingredient->Ingredient->BaseUnit->Name;
				}else if (control->Tag == buttonNewRecipeIngredientAdd->Tag)
				{
					control->Text = "X";
					control->Click -= gcnew System::EventHandler(this, &MyForm::buttonNewRecipeIngredientAdd_Click);
					control->Click += gcnew System::EventHandler(this, &MyForm::buttonNewRecipeIngredientRemove_Click);
				}
			}
		}
	}

	CreateNewRecipeIngredientPanel();
}

Void MyForm::buttonNewRecipeSave_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (textBoxNewRecipeTitle->Text == nullptr || richTextBoxNewRecipeDescription->Text == nullptr) return;

	Recipe^ recipe = AddNewRecipe();
	recipe->OriginalUrl = textBoxNewRecipeOriginalUrl->Text;
	recipe->Description = richTextBoxNewRecipeDescription->Text;
	recipe->ListOfIngredient = newRecipeIngredients;
	recipe->Title = textBoxNewRecipeTitle->Text;

	recipeList->Add(recipe);
	DatabaseLogic::SaveList(recipeList);

	LoadRecipesToRecipes();

	LoadRecipeToRecipe(recipe);
}

Recipe^ MyForm::AddNewRecipe()
{
	Recipe^ r = gcnew Recipe();
	if (recipeList == nullptr || recipeList->Count == 0)
		r->Id = 0;
	else
		r->Id = (int)(recipeList[recipeList->Count - 1]->Id) + 1;
	return r;
}

#pragma endregion NewRecipe

#pragma region Recipe

Void MyForm::LoadRecipeToRecipe(Recipe^ recipe)
{
	labelRecipeTitle->Text = recipe->Title;
	richTextBoxRecipeDescription->Text = recipe->Description;
	richTextBoxRecipeSource->Text = recipe->OriginalUrl;

	if (RecipeIngredientPanels != nullptr)
	{
		for each(Panel^ item in RecipeIngredientPanels)
		{
			delete item;
		}
		RecipeIngredientPanels->Clear();
	}
	else RecipeIngredientPanels = gcnew List<Panel^>;

	for each(IngredientInRecipe^ ingredient in recipe->ListOfIngredient)
	{
		Panel^ panel = FormCreator::ClonePanel(panelRecipeIngredient);
		panel->Tag = ingredient->Ingredient->Id.ToString();

		for each(Control^ control in panel->Controls)
		{
			if (control->Tag == labelRecipeIngredientName->Tag)
			{
				control->Text = ingredient->Ingredient->Name;
			} 
			else if(control->Tag == labelRecipeIngredientAmmmount->Tag)
			{
				control->Text = ingredient->Ammount.ToString();
			}
			else if (control->Tag == labelRecipeIngredientUnit->Tag)
			{
				control->Text = ingredient->Unit->Name;
			}
		}
		if (RecipeIngredientPanels == nullptr || RecipeIngredientPanels->Count == 0)
		{
			RecipeIngredientPanels = gcnew List<Panel^>();
		}
		else
		{
			Point point = RecipeIngredientPanels[RecipeIngredientPanels->Count - 1]->Location;
			point.Y += RecipeIngredientPanels[RecipeIngredientPanels->Count - 1]->Height + PanelsMargin;
			panel->Location = point;
		}
		RecipeIngredientPanels->Add(panel);
		tabPageRecipe->Controls->Add(panel);
	}

	tabControlMain->SelectedTab = tabPageRecipe;
}

#pragma endregion Recipe

#pragma region Ingredients

Void MyForm::buttonIngredientsAdd_Click(System::Object^  sender, System::EventArgs^  e)
{
	String^ lName = textBoxIngredientsName->Text;
	String^ lPrice = textBoxIngredientsPrice->Text;
	textBoxIngredientsName->Clear();
	textBoxIngredientsPrice->Clear();

	// sprawdzic czy Name istnieje
	for (int i = 0; i < ingredientList->Count; i++) {
		if (ingredientList[i]->Name == lName) {
			MessageBox::Show("SK�ADNIK JU� ISTNIEJE");
			return;
		}
	}
	// dodac nowy do listy
	Ingredient^ obj = gcnew Ingredient();
	obj->Name = lName;
	obj->PricePerUnit = Double::Parse(lPrice);
	ingredientList->Add(obj);
	if (ingredientList == nullptr || ingredientList->Count == 0)
		obj->Id = 0;
	else
		obj->Id = (int)(ingredientList[ingredientList->Count - 1]->Id) + 1;

	DatabaseLogic::SaveList(ingredientList);


	// wyswietlic nowa pozycje
	LoadIngredients();
}

Void MyForm::LoadIngredients()
{
	if (IngredientPanelsList != nullptr)
	{
		for each(Panel^ item in IngredientPanelsList)
		{
			delete item;
		}
		IngredientPanelsList->Clear();
	}
	else IngredientPanelsList = gcnew List<Panel^>;

	for each(Ingredient^ ingredient in ingredientList)
	{
		Panel^ panel = FormCreator::ClonePanel(panelIngredients);
		panel->Tag = ingredient->Id.ToString();

		for each(Control^ control in panel->Controls)
		{
			if (control->Tag == textBoxIngredientsNameAdded->Tag)
			{
				control->Text = ingredient->Name;
			}
			else if (control->Tag == textBoxIngredientsPriceAdded->Tag)
			{
				control->Text = ingredient->PricePerUnit.ToString();
				control->TextChanged += gcnew System::EventHandler(this, &MyForm::textBoxIngredientsPriceAdded_TextChanged);
			}

		}

		if (IngredientPanelsList == nullptr || IngredientPanelsList->Count == 0)
		{
			IngredientPanelsList = gcnew List<Panel^>();
		}
		else
		{
			Point point = IngredientPanelsList[IngredientPanelsList->Count - 1]->Location;
			point.Y += IngredientPanelsList[IngredientPanelsList->Count - 1]->Height + PanelsMargin;
			panel->Location = point;
		}
		IngredientPanelsList->Add(panel);
		tabPageIngredients->Controls->Add(panel);

	}
}

Void MyForm::textBoxIngredientsPriceAdded_TextChanged(System::Object ^ sender, System::EventArgs ^ e)
{
	Control^ control = dynamic_cast<Control^>(sender);
	if (control->Text == String::Empty) return;

	int id = Int32::Parse(control->Parent->Tag->ToString());

	for each (Ingredient^ ingredient in ingredientList)
	{
		if (ingredient->Id == id)
		{
			ingredient->PricePerUnit = Double::Parse(control->Text);
			break;
		}
	}
	DatabaseLogic::SaveList(ingredientList);
}

#pragma endregion Ingredients


/// <summary>
/// Przenosi do zak�adki Lod�wka
/// </summary>
Void MyForm::buttonFridge_Click(System::Object^  sender, System::EventArgs^  e)
{
	tabControlMain->SelectedTab = tabPageFridge;
}

/// <summary>
/// Przenosi do zak�adki
/// </summary>
Void MyForm::buttonRecipes_Click(System::Object^  sender, System::EventArgs^  e)
{
	tabControlMain->SelectedTab = tabPageRecipes;
}

/// <summary>
/// Przenosi do zak�adki
/// </summary>
Void MyForm::buttonIngredients_Click(System::Object^  sender, System::EventArgs^  e)
{
	LoadIngredients();
	tabControlMain->SelectedTab = tabPageIngredients;
}

/// <summary>
/// Przenosi do zak�adki
/// </summary>
Void MyForm::buttonRecipesAdd_Click(System::Object^  sender, System::EventArgs^  e)
{
	InitializeNewRecipe();

	tabControlMain->SelectedTab = tabPageNewRecipe;
}