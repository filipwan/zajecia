#include "FormCreator.h"

using namespace SPK;

FormCreator::FormCreator()
{
}

List<String^>^ FormCreator::ChangeListToStringList(List<Ingredient^>^ list)
{
	List<String^>^ newList = gcnew List<String^>;

	for each(Ingredient^ ingredient in list)
	{
		newList->Add(ingredient->Name);
	}

	return newList;
}

List<String^>^ FormCreator::ChangeListToStringList(List<Unit^>^ list)
{
	List<String^>^ newList = gcnew List<String^>;

	for each(Unit^ unit in list)
	{
		newList->Add(unit->Name);
	}

	return newList;
}

Panel^ FormCreator::ClonePanel(Panel^ panel)
{
	Panel^ newPanel = gcnew Panel();
	newPanel->Size = panel->Size;
	newPanel->Location = panel->Location;
	newPanel->Anchor = panel->Anchor;
	newPanel->BackColor = panel->BackColor;
	newPanel->BorderStyle = panel->BorderStyle;
	newPanel->Tag = panel->Tag;
	newPanel->Visible = true;

	for each(Control^ control in panel->Controls)
	{
		if (dynamic_cast<Label^>(control) != nullptr)
			newPanel->Controls->Add(CloneControl(dynamic_cast<Label^>(control)));
		else if (dynamic_cast<TextBox^>(control) != nullptr)
			newPanel->Controls->Add(CloneControl(dynamic_cast<TextBox^>(control)));
		else if(dynamic_cast<Button^>(control) != nullptr)
			newPanel->Controls->Add(CloneControl(dynamic_cast<Button^>(control)));
		else if(dynamic_cast<ComboBox^>(control) != nullptr)
			newPanel->Controls->Add(CloneControl(dynamic_cast<ComboBox^>(control)));
	}
	return newPanel;
}

Control ^ SPK::FormCreator::CloneControl(Label ^ control)
{
	Label^ newControl = gcnew Label();
	newControl->Size = control->Size;
	newControl->Location = control->Location;
	newControl->Anchor = control->Anchor;
	newControl->Text = control->Text;
	newControl->TextAlign = control->TextAlign;
	newControl->Font = control->Font;
	newControl->Tag = control->Tag;

	return newControl;
}

Control ^ SPK::FormCreator::CloneControl(TextBox ^ control)
{
	TextBox^ newControl = gcnew TextBox();
	newControl->Size = control->Size;
	newControl->Location = control->Location;
	newControl->Anchor = control->Anchor;
	newControl->Text = control->Text;
	newControl->TextAlign = control->TextAlign;
	newControl->Font = control->Font;
	newControl->Tag = control->Tag;

	return newControl;
}

Control ^ SPK::FormCreator::CloneControl(Button ^ control)
{
	Button^ newControl = gcnew Button();
	newControl->Size = control->Size;
	newControl->Location = control->Location;
	newControl->Anchor = control->Anchor;
	newControl->BackColor = control->BackColor;
	newControl->Text = control->Text;
	newControl->Font = control->Font;
	newControl->Tag = control->Tag;

	return newControl;
}

Control ^ SPK::FormCreator::CloneControl(ComboBox ^ control)
{
	ComboBox^ newControl = gcnew ComboBox();
	newControl->Size = control->Size;
	newControl->Location = control->Location;
	newControl->Anchor = control->Anchor;
	newControl->BackColor = control->BackColor;
	newControl->DisplayMember = control->DisplayMember;
	newControl->Enabled = control->Enabled;
	newControl->Font = control->Font;
	newControl->Tag = control->Tag;

	return newControl;
}
