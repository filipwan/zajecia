#pragma once

#using <System.Xml.dll>
#using <System.dll>

namespace SPK {

	using namespace System;
	using namespace System::IO;
	using namespace System::Xml::Serialization;

	ref class Unit;
	ref class UnitIncalculable;
	ref class UnitLiquid;
	ref class UnitSolid;

	[XmlInclude(UnitIncalculable::typeid)]
	[XmlInclude(UnitLiquid::typeid)]
	[XmlInclude(UnitSolid::typeid)]
	public ref class Unit
	{
	public:

		int Id;
		String^ Name;

	public:
		Unit();
	};

}
