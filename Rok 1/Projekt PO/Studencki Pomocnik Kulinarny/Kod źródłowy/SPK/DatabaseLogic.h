#pragma once
#include <msxml.h>
#include "Recipe.h"
#include "Ingredient.h"

using namespace System::Collections::Generic;

namespace SPK {

	

	public ref class DatabaseLogic
	{
	public:
		DatabaseLogic();
		static void SaveList(List<Recipe^>^);
		static void SaveList(List<Ingredient^>^);
		static void LoadList(List<Recipe^>^);
		static void LoadList(List<Ingredient^>^);
	};

}