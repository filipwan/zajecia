#include "DatabaseLogic.h"
#include <typeinfo>
#using <System.Dll>
#using <System.XML.Dll>
#using <System.XML.Linq.Dll>


using namespace SPK;
using namespace std;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::IO;
using namespace System::Runtime::Serialization;
using namespace System::Runtime::Serialization::Formatters::Binary;
using namespace System::Xml;
using namespace System::Xml::Serialization;
using namespace System::Xml::Linq;

DatabaseLogic::DatabaseLogic()
{

}

void DatabaseLogic::SaveList(List<Recipe^>^ recipes)
{
	if (recipes->Count == 0)
		return;

	XmlSerializer^ x = gcnew XmlSerializer(recipes[0]->GetType());
	for(int i = 0; i < recipes->Count; i++)
	{
		FileStream^ stream = gcnew FileStream("recipes/recipe" + i.ToString() + ".xml", FileMode::Create,
			FileAccess::ReadWrite);
		x->Serialize(stream, recipes[i]);
		stream->Close();
		delete stream;
	}
}

void DatabaseLogic::SaveList(List<Ingredient^>^ ingredients)
{
	if (ingredients->Count == 0)
		return;

	XmlSerializer^ x = gcnew XmlSerializer(ingredients[0]->GetType());
	for (int i = 0; i < ingredients->Count; i++)
	{
		FileStream^ stream = gcnew FileStream("ingredients/ingredient" + i.ToString() + ".xml", FileMode::Create,
			FileAccess::ReadWrite);
		x->Serialize(stream, ingredients[i]);
		stream->Close();
		delete stream;
	}
}

void DatabaseLogic::LoadList(List<Recipe^>^ recipes)
{
	recipes->Clear();
	XmlSerializer^ serializer = gcnew XmlSerializer((gcnew Recipe())->GetType());
	for(int i = 0; true; i++)
	{
		String^ path = "recipes/recipe" + i.ToString() + ".xml";
		if(!File::Exists(path)) break;

		FileStream^ stream = gcnew FileStream(path, FileMode::Open, FileAccess::ReadWrite);

		XmlReader^ reader = gcnew XmlTextReader(stream);

		if(serializer->CanDeserialize(reader) )
		{
			recipes->Add(dynamic_cast<Recipe^>(serializer->Deserialize(reader)));
		}
		stream->Close();
		delete stream;
	}

}

void DatabaseLogic::LoadList(List<Ingredient^>^ ingredients)
{
	ingredients->Clear();
	XmlSerializer^ serializer = gcnew XmlSerializer((gcnew Ingredient())->GetType());
	for (int i = 0; true; i++)
	{
		String^ path = "ingredients/ingredient" + i.ToString() + ".xml";
		if (!File::Exists(path)) break;

		FileStream^ stream = gcnew FileStream(path, FileMode::Open, FileAccess::ReadWrite);

		XmlReader^ reader = gcnew XmlTextReader(stream);

		if (serializer->CanDeserialize(reader))
		{
			ingredients->Add(dynamic_cast<Ingredient^>(serializer->Deserialize(reader)));
		}
		stream->Close();
		delete stream;
	}
}