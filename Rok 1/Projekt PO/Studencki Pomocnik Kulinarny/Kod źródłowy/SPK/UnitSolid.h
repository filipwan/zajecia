#pragma once
#include "Unit.h"

namespace SPK {

	using namespace System;

	public ref class UnitSolid : public Unit
	{

	public:
		int UnitInGrams;

	public:
		UnitSolid();
		UnitSolid(int id, String^ name, int unitInGrams);
	};

}