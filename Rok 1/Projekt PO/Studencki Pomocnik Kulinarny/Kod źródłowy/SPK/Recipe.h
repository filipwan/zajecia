#pragma once
#include "IngredientInRecipe.h"

namespace SPK {

	using namespace System;
	using namespace System::Collections::Generic;

	public ref class Recipe
	{
	public:

		int Id;
		String^ Title;
		String^ Description;
		List<IngredientInRecipe^>^ ListOfIngredient;
		String^ OriginalUrl;

	public:
		Recipe();
	};

}
