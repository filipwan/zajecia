#pragma once
#include "Ingredient.h"
#include "Unit.h"

namespace SPK {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	ref class FormCreator
	{
	public:
		FormCreator();
		static List<String^>^ ChangeListToStringList(List<Ingredient^>^ list);
		static List<String^>^ ChangeListToStringList(List<Unit^>^ list);
		static Panel^ ClonePanel(Panel^ panel);
	private:
		static Control^ CloneControl(Label^ control);
		static Control^ CloneControl(TextBox^ control);
		static Control^ CloneControl(Button^ control);
		static Control^ CloneControl(ComboBox^ control);
	};

}
