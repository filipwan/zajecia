#pragma once
#include "Unit.h"

namespace SPK {

	using namespace System;

	public ref class UnitIncalculable : public Unit
	{
	public:
		UnitIncalculable();
		UnitIncalculable(int id, String^ name);
	};

}