#pragma once
#include "DatabaseLogic.h"
#include "FormCreator.h"
#include "Recipe.h"
#include "UnitIncalculable.h"
#include "UnitLiquid.h"
#include "UnitSolid.h"

namespace SPK {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: List<Ingredient^>^ ingredientList;
	private: List<Recipe^>^ recipeList;
	private: List<IngredientInRecipe^>^ newRecipeIngredients;
	protected: List<Panel^>^ OwnedIngredientPanels;
	protected: List<Panel^>^ RecipeSearchResultPanels;
	protected: List<Panel^>^ NewRecipeIngredientPanels;
	protected: List<Panel^>^ RecipeIngredientPanels;
	protected: List<Panel^>^ IngredientPanelsList;
	private: List<Unit^>^ unitList = gcnew List<Unit^>();

	private: const int PanelsMargin = 6;

	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	private: System::Windows::Forms::TabControl^  tabControlMain;
	private: System::Windows::Forms::TabPage^  tabPageFridge;
	private: System::Windows::Forms::TabPage^  tabPageRecipes;
	private: System::Windows::Forms::TabPage^  tabPageNewRecipe;
	private: System::Windows::Forms::TabPage^  tabPageRecipe;
	private: System::Windows::Forms::TabPage^  tabPageIngredients;
	private: System::Windows::Forms::Button^  buttonIngredients;
	private: System::Windows::Forms::Button^  buttonRecipes;
	private: System::Windows::Forms::Button^  buttonFridge;
	private: System::Windows::Forms::Panel^  panelRecipeSearchResult;
	private: System::Windows::Forms::Label^  labelRecipeSearchResultTitle;
	private: System::Windows::Forms::TextBox^  textBoxRecipeSearch;
	private: System::Windows::Forms::Button^  buttonRecipesAdd;
	private: System::Windows::Forms::RichTextBox^  richTextBoxNewRecipeDescription;
	private: System::Windows::Forms::Label^  labelNewRecipeDescripton;
	private: System::Windows::Forms::Label^  labelNewRecipeIngredients;
	private: System::Windows::Forms::Panel^  panelNewRecipeIngredients;
	private: System::Windows::Forms::ComboBox^  comboBoxNewRecipeIngredientUnit;
	private: System::Windows::Forms::Button^  buttonNewRecipeIngredientAdd;
	private: System::Windows::Forms::Label^  labelNewRecipeIngredient;
	private: System::Windows::Forms::ComboBox^  comboBoxNewRecipeIngredient;
	private: System::Windows::Forms::TextBox^  textBoxNewRecipeIngredient;
	private: System::Windows::Forms::Button^  buttonNewRecipeSave;
	private: System::Windows::Forms::Label^  labelNewRecipeTitle;
	private: System::Windows::Forms::TextBox^  textBoxNewRecipeTitle;












	private: System::Windows::Forms::RichTextBox^  richTextBoxRecipeSource;

	private: System::Windows::Forms::Label^  labelRecipeIngredientSource;
	private: System::Windows::Forms::RichTextBox^  richTextBoxRecipeDescription;
	private: System::Windows::Forms::Panel^  panelRecipeIngredient;
	private: System::Windows::Forms::Label^  labelRecipeIngredientName;
	private: System::Windows::Forms::Label^  labelRecipeIngredientUnit;
	private: System::Windows::Forms::Label^  labelRecipeIngredientAmmmount;
	private: System::Windows::Forms::Label^  labelRecipeIngredientSeparator;
	private: System::Windows::Forms::Label^  labelRecipeDescription;
	private: System::Windows::Forms::Label^  labelRecipeIngredients;
	private: System::Windows::Forms::Label^  labelRecipeTitle;
	private: System::Windows::Forms::Panel^  panelOwnedIngredientAdd;
	private: System::Windows::Forms::ComboBox^  comboBoxOwnedIngredientAddUnit;
	private: System::Windows::Forms::TextBox^  textBoxOwnedIngredientAddUnitAmmount;
	private: System::Windows::Forms::ComboBox^  comboBoxOwnedIngredientAdd;
	private: System::Windows::Forms::Button^  buttonOwnedIngredientAdd;
	private: System::Windows::Forms::Label^  labelOwnedIngredientAdd;
	private: System::Windows::Forms::Panel^  panelOwnedIngredient;
	private: System::Windows::Forms::ComboBox^  comboBoxOwnedIngredientUnit;
	private: System::Windows::Forms::TextBox^  textBoxOwnedIngredientUnitAmmount;
	private: System::Windows::Forms::Label^  labelOwnedIngredientUnit;
	private: System::Windows::Forms::Button^  buttonOwnedIngredientDelete;
	private: System::Windows::Forms::Label^  labelOwnedIngredient;
	private: System::Windows::Forms::Label^  labelNewRecipeOriginalUrl;
	private: System::Windows::Forms::TextBox^  textBoxNewRecipeOriginalUrl;
	private: System::Windows::Forms::Label^  labelFridge;
	private: System::Windows::Forms::Panel^  panelIngredients;
	private: System::Windows::Forms::TextBox^  textBoxIngredientsPriceAdded;
	private: System::Windows::Forms::TextBox^  textBoxIngredientsNameAdded;
	private: System::Windows::Forms::Label^  labelIngredientsPriceOwned;
	private: System::Windows::Forms::Label^  labelIngredientsNameOwned;
	private: System::Windows::Forms::Button^  buttonIngredientsAdd;
	private: System::Windows::Forms::TextBox^  textBoxIngredientsPrice;
	private: System::Windows::Forms::Label^  labelIngredientsPrice;
	private: System::Windows::Forms::TextBox^  textBoxIngredientsName;
	private: System::Windows::Forms::Label^  labelIngredietnsName;
	private: System::Windows::Forms::Label^  labelIngredients;
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->buttonIngredients = (gcnew System::Windows::Forms::Button());
			this->buttonRecipes = (gcnew System::Windows::Forms::Button());
			this->buttonFridge = (gcnew System::Windows::Forms::Button());
			this->tabControlMain = (gcnew System::Windows::Forms::TabControl());
			this->tabPageFridge = (gcnew System::Windows::Forms::TabPage());
			this->labelFridge = (gcnew System::Windows::Forms::Label());
			this->panelOwnedIngredientAdd = (gcnew System::Windows::Forms::Panel());
			this->comboBoxOwnedIngredientAddUnit = (gcnew System::Windows::Forms::ComboBox());
			this->textBoxOwnedIngredientAddUnitAmmount = (gcnew System::Windows::Forms::TextBox());
			this->comboBoxOwnedIngredientAdd = (gcnew System::Windows::Forms::ComboBox());
			this->buttonOwnedIngredientAdd = (gcnew System::Windows::Forms::Button());
			this->labelOwnedIngredientAdd = (gcnew System::Windows::Forms::Label());
			this->panelOwnedIngredient = (gcnew System::Windows::Forms::Panel());
			this->comboBoxOwnedIngredientUnit = (gcnew System::Windows::Forms::ComboBox());
			this->textBoxOwnedIngredientUnitAmmount = (gcnew System::Windows::Forms::TextBox());
			this->labelOwnedIngredientUnit = (gcnew System::Windows::Forms::Label());
			this->buttonOwnedIngredientDelete = (gcnew System::Windows::Forms::Button());
			this->labelOwnedIngredient = (gcnew System::Windows::Forms::Label());
			this->tabPageRecipes = (gcnew System::Windows::Forms::TabPage());
			this->panelRecipeSearchResult = (gcnew System::Windows::Forms::Panel());
			this->labelRecipeSearchResultTitle = (gcnew System::Windows::Forms::Label());
			this->textBoxRecipeSearch = (gcnew System::Windows::Forms::TextBox());
			this->buttonRecipesAdd = (gcnew System::Windows::Forms::Button());
			this->tabPageNewRecipe = (gcnew System::Windows::Forms::TabPage());
			this->labelNewRecipeOriginalUrl = (gcnew System::Windows::Forms::Label());
			this->textBoxNewRecipeOriginalUrl = (gcnew System::Windows::Forms::TextBox());
			this->richTextBoxNewRecipeDescription = (gcnew System::Windows::Forms::RichTextBox());
			this->labelNewRecipeDescripton = (gcnew System::Windows::Forms::Label());
			this->labelNewRecipeIngredients = (gcnew System::Windows::Forms::Label());
			this->panelNewRecipeIngredients = (gcnew System::Windows::Forms::Panel());
			this->comboBoxNewRecipeIngredientUnit = (gcnew System::Windows::Forms::ComboBox());
			this->buttonNewRecipeIngredientAdd = (gcnew System::Windows::Forms::Button());
			this->labelNewRecipeIngredient = (gcnew System::Windows::Forms::Label());
			this->comboBoxNewRecipeIngredient = (gcnew System::Windows::Forms::ComboBox());
			this->textBoxNewRecipeIngredient = (gcnew System::Windows::Forms::TextBox());
			this->buttonNewRecipeSave = (gcnew System::Windows::Forms::Button());
			this->labelNewRecipeTitle = (gcnew System::Windows::Forms::Label());
			this->textBoxNewRecipeTitle = (gcnew System::Windows::Forms::TextBox());
			this->tabPageRecipe = (gcnew System::Windows::Forms::TabPage());
			this->richTextBoxRecipeSource = (gcnew System::Windows::Forms::RichTextBox());
			this->labelRecipeIngredientSource = (gcnew System::Windows::Forms::Label());
			this->richTextBoxRecipeDescription = (gcnew System::Windows::Forms::RichTextBox());
			this->panelRecipeIngredient = (gcnew System::Windows::Forms::Panel());
			this->labelRecipeIngredientName = (gcnew System::Windows::Forms::Label());
			this->labelRecipeIngredientUnit = (gcnew System::Windows::Forms::Label());
			this->labelRecipeIngredientAmmmount = (gcnew System::Windows::Forms::Label());
			this->labelRecipeIngredientSeparator = (gcnew System::Windows::Forms::Label());
			this->labelRecipeDescription = (gcnew System::Windows::Forms::Label());
			this->labelRecipeIngredients = (gcnew System::Windows::Forms::Label());
			this->labelRecipeTitle = (gcnew System::Windows::Forms::Label());
			this->tabPageIngredients = (gcnew System::Windows::Forms::TabPage());
			this->panelIngredients = (gcnew System::Windows::Forms::Panel());
			this->textBoxIngredientsPriceAdded = (gcnew System::Windows::Forms::TextBox());
			this->textBoxIngredientsNameAdded = (gcnew System::Windows::Forms::TextBox());
			this->labelIngredientsPriceOwned = (gcnew System::Windows::Forms::Label());
			this->labelIngredientsNameOwned = (gcnew System::Windows::Forms::Label());
			this->buttonIngredientsAdd = (gcnew System::Windows::Forms::Button());
			this->textBoxIngredientsPrice = (gcnew System::Windows::Forms::TextBox());
			this->labelIngredientsPrice = (gcnew System::Windows::Forms::Label());
			this->textBoxIngredientsName = (gcnew System::Windows::Forms::TextBox());
			this->labelIngredietnsName = (gcnew System::Windows::Forms::Label());
			this->labelIngredients = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->BeginInit();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->tabControlMain->SuspendLayout();
			this->tabPageFridge->SuspendLayout();
			this->panelOwnedIngredientAdd->SuspendLayout();
			this->panelOwnedIngredient->SuspendLayout();
			this->tabPageRecipes->SuspendLayout();
			this->panelRecipeSearchResult->SuspendLayout();
			this->tabPageNewRecipe->SuspendLayout();
			this->panelNewRecipeIngredients->SuspendLayout();
			this->tabPageRecipe->SuspendLayout();
			this->panelRecipeIngredient->SuspendLayout();
			this->tabPageIngredients->SuspendLayout();
			this->panelIngredients->SuspendLayout();
			this->SuspendLayout();
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->Location = System::Drawing::Point(0, 0);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->BackColor = System::Drawing::Color::LightSteelBlue;
			this->splitContainer1->Panel1->Controls->Add(this->buttonIngredients);
			this->splitContainer1->Panel1->Controls->Add(this->buttonRecipes);
			this->splitContainer1->Panel1->Controls->Add(this->buttonFridge);
			this->splitContainer1->Panel1->Margin = System::Windows::Forms::Padding(3);
			this->splitContainer1->Panel1MinSize = 200;
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->tabControlMain);
			this->splitContainer1->Size = System::Drawing::Size(884, 611);
			this->splitContainer1->SplitterDistance = 200;
			this->splitContainer1->TabIndex = 1;
			// 
			// buttonIngredients
			// 
			this->buttonIngredients->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->buttonIngredients->Font = (gcnew System::Drawing::Font(L"Calibri", 20));
			this->buttonIngredients->Location = System::Drawing::Point(12, 328);
			this->buttonIngredients->Name = L"buttonIngredients";
			this->buttonIngredients->Size = System::Drawing::Size(169, 52);
			this->buttonIngredients->TabIndex = 2;
			this->buttonIngredients->Text = L"Sk�adniki";
			this->buttonIngredients->UseVisualStyleBackColor = false;
			this->buttonIngredients->Click += gcnew System::EventHandler(this, &MyForm::buttonIngredients_Click);
			// 
			// buttonRecipes
			// 
			this->buttonRecipes->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->buttonRecipes->Font = (gcnew System::Drawing::Font(L"Calibri", 20));
			this->buttonRecipes->Location = System::Drawing::Point(12, 233);
			this->buttonRecipes->Name = L"buttonRecipes";
			this->buttonRecipes->Size = System::Drawing::Size(169, 52);
			this->buttonRecipes->TabIndex = 1;
			this->buttonRecipes->Text = L"Przepisy";
			this->buttonRecipes->UseVisualStyleBackColor = false;
			this->buttonRecipes->Click += gcnew System::EventHandler(this, &MyForm::buttonRecipes_Click);
			// 
			// buttonFridge
			// 
			this->buttonFridge->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->buttonFridge->Font = (gcnew System::Drawing::Font(L"Calibri", 20));
			this->buttonFridge->Location = System::Drawing::Point(12, 132);
			this->buttonFridge->Name = L"buttonFridge";
			this->buttonFridge->Size = System::Drawing::Size(169, 52);
			this->buttonFridge->TabIndex = 0;
			this->buttonFridge->Text = L"Lod�wka";
			this->buttonFridge->UseVisualStyleBackColor = false;
			this->buttonFridge->Click += gcnew System::EventHandler(this, &MyForm::buttonFridge_Click);
			// 
			// tabControlMain
			// 
			this->tabControlMain->Controls->Add(this->tabPageFridge);
			this->tabControlMain->Controls->Add(this->tabPageRecipes);
			this->tabControlMain->Controls->Add(this->tabPageNewRecipe);
			this->tabControlMain->Controls->Add(this->tabPageRecipe);
			this->tabControlMain->Controls->Add(this->tabPageIngredients);
			this->tabControlMain->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tabControlMain->ItemSize = System::Drawing::Size(0, 1);
			this->tabControlMain->Location = System::Drawing::Point(0, 0);
			this->tabControlMain->Margin = System::Windows::Forms::Padding(0);
			this->tabControlMain->Name = L"tabControlMain";
			this->tabControlMain->SelectedIndex = 0;
			this->tabControlMain->Size = System::Drawing::Size(680, 611);
			this->tabControlMain->TabIndex = 0;
			// 
			// tabPageFridge
			// 
			this->tabPageFridge->AutoScroll = true;
			this->tabPageFridge->BackColor = System::Drawing::Color::AliceBlue;
			this->tabPageFridge->Controls->Add(this->labelFridge);
			this->tabPageFridge->Controls->Add(this->panelOwnedIngredientAdd);
			this->tabPageFridge->Controls->Add(this->panelOwnedIngredient);
			this->tabPageFridge->Location = System::Drawing::Point(4, 5);
			this->tabPageFridge->Margin = System::Windows::Forms::Padding(0);
			this->tabPageFridge->Name = L"tabPageFridge";
			this->tabPageFridge->Size = System::Drawing::Size(672, 602);
			this->tabPageFridge->TabIndex = 0;
			this->tabPageFridge->Text = L"Lod�wka";
			// 
			// labelFridge
			// 
			this->labelFridge->AutoSize = true;
			this->labelFridge->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 30));
			this->labelFridge->Location = System::Drawing::Point(105, 28);
			this->labelFridge->Name = L"labelFridge";
			this->labelFridge->Size = System::Drawing::Size(181, 46);
			this->labelFridge->TabIndex = 26;
			this->labelFridge->Text = L"Lod�wka";
			// 
			// panelOwnedIngredientAdd
			// 
			this->panelOwnedIngredientAdd->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panelOwnedIngredientAdd->Controls->Add(this->comboBoxOwnedIngredientAddUnit);
			this->panelOwnedIngredientAdd->Controls->Add(this->textBoxOwnedIngredientAddUnitAmmount);
			this->panelOwnedIngredientAdd->Controls->Add(this->comboBoxOwnedIngredientAdd);
			this->panelOwnedIngredientAdd->Controls->Add(this->buttonOwnedIngredientAdd);
			this->panelOwnedIngredientAdd->Controls->Add(this->labelOwnedIngredientAdd);
			this->panelOwnedIngredientAdd->Location = System::Drawing::Point(26, 110);
			this->panelOwnedIngredientAdd->Name = L"panelOwnedIngredientAdd";
			this->panelOwnedIngredientAdd->Size = System::Drawing::Size(624, 52);
			this->panelOwnedIngredientAdd->TabIndex = 3;
			// 
			// comboBoxOwnedIngredientAddUnit
			// 
			this->comboBoxOwnedIngredientAddUnit->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxOwnedIngredientAddUnit->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->comboBoxOwnedIngredientAddUnit->FormattingEnabled = true;
			this->comboBoxOwnedIngredientAddUnit->Location = System::Drawing::Point(482, 14);
			this->comboBoxOwnedIngredientAddUnit->Name = L"comboBoxOwnedIngredientAddUnit";
			this->comboBoxOwnedIngredientAddUnit->Size = System::Drawing::Size(104, 27);
			this->comboBoxOwnedIngredientAddUnit->TabIndex = 7;
			// 
			// textBoxOwnedIngredientAddUnitAmmount
			// 
			this->textBoxOwnedIngredientAddUnitAmmount->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(238)));
			this->textBoxOwnedIngredientAddUnitAmmount->Location = System::Drawing::Point(391, 14);
			this->textBoxOwnedIngredientAddUnitAmmount->Name = L"textBoxOwnedIngredientAddUnitAmmount";
			this->textBoxOwnedIngredientAddUnitAmmount->Size = System::Drawing::Size(85, 27);
			this->textBoxOwnedIngredientAddUnitAmmount->TabIndex = 2;
			// 
			// comboBoxOwnedIngredientAdd
			// 
			this->comboBoxOwnedIngredientAdd->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
			this->comboBoxOwnedIngredientAdd->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->comboBoxOwnedIngredientAdd->FormattingEnabled = true;
			this->comboBoxOwnedIngredientAdd->Location = System::Drawing::Point(137, 14);
			this->comboBoxOwnedIngredientAdd->Name = L"comboBoxOwnedIngredientAdd";
			this->comboBoxOwnedIngredientAdd->Size = System::Drawing::Size(248, 27);
			this->comboBoxOwnedIngredientAdd->TabIndex = 2;
			// 
			// buttonOwnedIngredientAdd
			// 
			this->buttonOwnedIngredientAdd->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->buttonOwnedIngredientAdd->Cursor = System::Windows::Forms::Cursors::Hand;
			this->buttonOwnedIngredientAdd->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonOwnedIngredientAdd->Location = System::Drawing::Point(592, 13);
			this->buttonOwnedIngredientAdd->Name = L"buttonOwnedIngredientAdd";
			this->buttonOwnedIngredientAdd->Size = System::Drawing::Size(27, 27);
			this->buttonOwnedIngredientAdd->TabIndex = 6;
			this->buttonOwnedIngredientAdd->Text = L"+";
			this->buttonOwnedIngredientAdd->UseVisualStyleBackColor = false;
			this->buttonOwnedIngredientAdd->Click += gcnew System::EventHandler(this, &MyForm::buttonOwnedIngredientAdd_Click);
			// 
			// labelOwnedIngredientAdd
			// 
			this->labelOwnedIngredientAdd->AutoSize = true;
			this->labelOwnedIngredientAdd->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->labelOwnedIngredientAdd->Location = System::Drawing::Point(23, 16);
			this->labelOwnedIngredientAdd->Name = L"labelOwnedIngredientAdd";
			this->labelOwnedIngredientAdd->Size = System::Drawing::Size(108, 19);
			this->labelOwnedIngredientAdd->TabIndex = 0;
			this->labelOwnedIngredientAdd->Text = L"Dodaj sk�adnik:";
			// 
			// panelOwnedIngredient
			// 
			this->panelOwnedIngredient->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panelOwnedIngredient->Controls->Add(this->comboBoxOwnedIngredientUnit);
			this->panelOwnedIngredient->Controls->Add(this->textBoxOwnedIngredientUnitAmmount);
			this->panelOwnedIngredient->Controls->Add(this->labelOwnedIngredientUnit);
			this->panelOwnedIngredient->Controls->Add(this->buttonOwnedIngredientDelete);
			this->panelOwnedIngredient->Controls->Add(this->labelOwnedIngredient);
			this->panelOwnedIngredient->Location = System::Drawing::Point(26, 168);
			this->panelOwnedIngredient->Name = L"panelOwnedIngredient";
			this->panelOwnedIngredient->Size = System::Drawing::Size(477, 52);
			this->panelOwnedIngredient->TabIndex = 2;
			this->panelOwnedIngredient->Visible = false;
			// 
			// comboBoxOwnedIngredientUnit
			// 
			this->comboBoxOwnedIngredientUnit->Enabled = false;
			this->comboBoxOwnedIngredientUnit->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->comboBoxOwnedIngredientUnit->FormattingEnabled = true;
			this->comboBoxOwnedIngredientUnit->Location = System::Drawing::Point(335, 13);
			this->comboBoxOwnedIngredientUnit->Name = L"comboBoxOwnedIngredientUnit";
			this->comboBoxOwnedIngredientUnit->Size = System::Drawing::Size(104, 27);
			this->comboBoxOwnedIngredientUnit->TabIndex = 10;
			this->comboBoxOwnedIngredientUnit->Tag = L"unit";
			// 
			// textBoxOwnedIngredientUnitAmmount
			// 
			this->textBoxOwnedIngredientUnitAmmount->AllowDrop = true;
			this->textBoxOwnedIngredientUnitAmmount->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->textBoxOwnedIngredientUnitAmmount->Location = System::Drawing::Point(264, 13);
			this->textBoxOwnedIngredientUnitAmmount->Name = L"textBoxOwnedIngredientUnitAmmount";
			this->textBoxOwnedIngredientUnitAmmount->Size = System::Drawing::Size(43, 27);
			this->textBoxOwnedIngredientUnitAmmount->TabIndex = 7;
			this->textBoxOwnedIngredientUnitAmmount->Tag = L"ammount";
			this->textBoxOwnedIngredientUnitAmmount->Text = L"1";
			this->textBoxOwnedIngredientUnitAmmount->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->textBoxOwnedIngredientUnitAmmount->TextChanged += gcnew System::EventHandler(this, &MyForm::textBoxOwnedIngredientUnitAmmount_TextChanged);
			// 
			// labelOwnedIngredientUnit
			// 
			this->labelOwnedIngredientUnit->AutoSize = true;
			this->labelOwnedIngredientUnit->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->labelOwnedIngredientUnit->Location = System::Drawing::Point(313, 18);
			this->labelOwnedIngredientUnit->Name = L"labelOwnedIngredientUnit";
			this->labelOwnedIngredientUnit->Size = System::Drawing::Size(16, 19);
			this->labelOwnedIngredientUnit->TabIndex = 9;
			this->labelOwnedIngredientUnit->Tag = L"unitLabel";
			this->labelOwnedIngredientUnit->Text = L"x";
			// 
			// buttonOwnedIngredientDelete
			// 
			this->buttonOwnedIngredientDelete->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->buttonOwnedIngredientDelete->Cursor = System::Windows::Forms::Cursors::Hand;
			this->buttonOwnedIngredientDelete->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonOwnedIngredientDelete->Location = System::Drawing::Point(445, 13);
			this->buttonOwnedIngredientDelete->Name = L"buttonOwnedIngredientDelete";
			this->buttonOwnedIngredientDelete->Size = System::Drawing::Size(27, 27);
			this->buttonOwnedIngredientDelete->TabIndex = 8;
			this->buttonOwnedIngredientDelete->Text = L"X";
			this->buttonOwnedIngredientDelete->UseVisualStyleBackColor = false;
			this->buttonOwnedIngredientDelete->Click += gcnew System::EventHandler(this, &MyForm::buttonOwnedIngredientDelete_Click);
			// 
			// labelOwnedIngredient
			// 
			this->labelOwnedIngredient->AutoSize = true;
			this->labelOwnedIngredient->Font = (gcnew System::Drawing::Font(L"Calibri", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->labelOwnedIngredient->Location = System::Drawing::Point(23, 17);
			this->labelOwnedIngredient->Name = L"labelOwnedIngredient";
			this->labelOwnedIngredient->Size = System::Drawing::Size(62, 19);
			this->labelOwnedIngredient->TabIndex = 1;
			this->labelOwnedIngredient->Tag = L"name";
			this->labelOwnedIngredient->Text = L"Sk�adnik";
			// 
			// tabPageRecipes
			// 
			this->tabPageRecipes->AutoScroll = true;
			this->tabPageRecipes->BackColor = System::Drawing::Color::AliceBlue;
			this->tabPageRecipes->Controls->Add(this->panelRecipeSearchResult);
			this->tabPageRecipes->Controls->Add(this->textBoxRecipeSearch);
			this->tabPageRecipes->Controls->Add(this->buttonRecipesAdd);
			this->tabPageRecipes->Location = System::Drawing::Point(4, 5);
			this->tabPageRecipes->Name = L"tabPageRecipes";
			this->tabPageRecipes->Padding = System::Windows::Forms::Padding(3);
			this->tabPageRecipes->Size = System::Drawing::Size(672, 602);
			this->tabPageRecipes->TabIndex = 1;
			this->tabPageRecipes->Text = L"Przepisy";
			// 
			// panelRecipeSearchResult
			// 
			this->panelRecipeSearchResult->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->panelRecipeSearchResult->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panelRecipeSearchResult->Controls->Add(this->labelRecipeSearchResultTitle);
			this->panelRecipeSearchResult->Location = System::Drawing::Point(32, 110);
			this->panelRecipeSearchResult->Name = L"panelRecipeSearchResult";
			this->panelRecipeSearchResult->Size = System::Drawing::Size(594, 70);
			this->panelRecipeSearchResult->TabIndex = 12;
			this->panelRecipeSearchResult->Visible = false;
			this->panelRecipeSearchResult->Click += gcnew System::EventHandler(this, &MyForm::panelRecipeSearchResult_Click);
			// 
			// labelRecipeSearchResultTitle
			// 
			this->labelRecipeSearchResultTitle->Font = (gcnew System::Drawing::Font(L"Calibri", 24));
			this->labelRecipeSearchResultTitle->Location = System::Drawing::Point(71, 15);
			this->labelRecipeSearchResultTitle->Name = L"labelRecipeSearchResultTitle";
			this->labelRecipeSearchResultTitle->Size = System::Drawing::Size(494, 39);
			this->labelRecipeSearchResultTitle->TabIndex = 0;
			this->labelRecipeSearchResultTitle->Tag = L"title";
			this->labelRecipeSearchResultTitle->Text = L"Tytu�";
			this->labelRecipeSearchResultTitle->Click += gcnew System::EventHandler(this, &MyForm::panelRecipeSearchResult_Click);
			// 
			// textBoxRecipeSearch
			// 
			this->textBoxRecipeSearch->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->textBoxRecipeSearch->Location = System::Drawing::Point(32, 77);
			this->textBoxRecipeSearch->Name = L"textBoxRecipeSearch";
			this->textBoxRecipeSearch->Size = System::Drawing::Size(214, 27);
			this->textBoxRecipeSearch->TabIndex = 11;
			// 
			// buttonRecipesAdd
			// 
			this->buttonRecipesAdd->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->buttonRecipesAdd->Cursor = System::Windows::Forms::Cursors::Hand;
			this->buttonRecipesAdd->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonRecipesAdd->Location = System::Drawing::Point(32, 44);
			this->buttonRecipesAdd->Name = L"buttonRecipesAdd";
			this->buttonRecipesAdd->Size = System::Drawing::Size(214, 27);
			this->buttonRecipesAdd->TabIndex = 10;
			this->buttonRecipesAdd->Text = L"Dodaj nowy przepis";
			this->buttonRecipesAdd->UseVisualStyleBackColor = false;
			this->buttonRecipesAdd->Click += gcnew System::EventHandler(this, &MyForm::buttonRecipesAdd_Click);
			// 
			// tabPageNewRecipe
			// 
			this->tabPageNewRecipe->AutoScroll = true;
			this->tabPageNewRecipe->BackColor = System::Drawing::Color::AliceBlue;
			this->tabPageNewRecipe->Controls->Add(this->labelNewRecipeOriginalUrl);
			this->tabPageNewRecipe->Controls->Add(this->textBoxNewRecipeOriginalUrl);
			this->tabPageNewRecipe->Controls->Add(this->richTextBoxNewRecipeDescription);
			this->tabPageNewRecipe->Controls->Add(this->labelNewRecipeDescripton);
			this->tabPageNewRecipe->Controls->Add(this->labelNewRecipeIngredients);
			this->tabPageNewRecipe->Controls->Add(this->panelNewRecipeIngredients);
			this->tabPageNewRecipe->Controls->Add(this->buttonNewRecipeSave);
			this->tabPageNewRecipe->Controls->Add(this->labelNewRecipeTitle);
			this->tabPageNewRecipe->Controls->Add(this->textBoxNewRecipeTitle);
			this->tabPageNewRecipe->Location = System::Drawing::Point(4, 5);
			this->tabPageNewRecipe->Name = L"tabPageNewRecipe";
			this->tabPageNewRecipe->Padding = System::Windows::Forms::Padding(3);
			this->tabPageNewRecipe->Size = System::Drawing::Size(672, 602);
			this->tabPageNewRecipe->TabIndex = 2;
			this->tabPageNewRecipe->Text = L"Nowy Przepis";
			// 
			// labelNewRecipeOriginalUrl
			// 
			this->labelNewRecipeOriginalUrl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->labelNewRecipeOriginalUrl->AutoSize = true;
			this->labelNewRecipeOriginalUrl->Location = System::Drawing::Point(270, 557);
			this->labelNewRecipeOriginalUrl->Name = L"labelNewRecipeOriginalUrl";
			this->labelNewRecipeOriginalUrl->Size = System::Drawing::Size(42, 13);
			this->labelNewRecipeOriginalUrl->TabIndex = 17;
			this->labelNewRecipeOriginalUrl->Text = L"�r�d�o:";
			// 
			// textBoxNewRecipeOriginalUrl
			// 
			this->textBoxNewRecipeOriginalUrl->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxNewRecipeOriginalUrl->Font = (gcnew System::Drawing::Font(L"Calibri", 10));
			this->textBoxNewRecipeOriginalUrl->Location = System::Drawing::Point(318, 553);
			this->textBoxNewRecipeOriginalUrl->Name = L"textBoxNewRecipeOriginalUrl";
			this->textBoxNewRecipeOriginalUrl->Size = System::Drawing::Size(346, 24);
			this->textBoxNewRecipeOriginalUrl->TabIndex = 16;
			// 
			// richTextBoxNewRecipeDescription
			// 
			this->richTextBoxNewRecipeDescription->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->richTextBoxNewRecipeDescription->Location = System::Drawing::Point(271, 124);
			this->richTextBoxNewRecipeDescription->Name = L"richTextBoxNewRecipeDescription";
			this->richTextBoxNewRecipeDescription->Size = System::Drawing::Size(393, 412);
			this->richTextBoxNewRecipeDescription->TabIndex = 15;
			this->richTextBoxNewRecipeDescription->Text = L"";
			// 
			// labelNewRecipeDescripton
			// 
			this->labelNewRecipeDescripton->AutoSize = true;
			this->labelNewRecipeDescripton->Location = System::Drawing::Point(268, 98);
			this->labelNewRecipeDescripton->Name = L"labelNewRecipeDescripton";
			this->labelNewRecipeDescripton->Size = System::Drawing::Size(44, 13);
			this->labelNewRecipeDescripton->TabIndex = 14;
			this->labelNewRecipeDescripton->Text = L"Przepis:";
			// 
			// labelNewRecipeIngredients
			// 
			this->labelNewRecipeIngredients->AutoSize = true;
			this->labelNewRecipeIngredients->Location = System::Drawing::Point(18, 98);
			this->labelNewRecipeIngredients->Name = L"labelNewRecipeIngredients";
			this->labelNewRecipeIngredients->Size = System::Drawing::Size(55, 13);
			this->labelNewRecipeIngredients->TabIndex = 13;
			this->labelNewRecipeIngredients->Text = L"Sk�adniki:";
			// 
			// panelNewRecipeIngredients
			// 
			this->panelNewRecipeIngredients->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panelNewRecipeIngredients->Controls->Add(this->comboBoxNewRecipeIngredientUnit);
			this->panelNewRecipeIngredients->Controls->Add(this->buttonNewRecipeIngredientAdd);
			this->panelNewRecipeIngredients->Controls->Add(this->labelNewRecipeIngredient);
			this->panelNewRecipeIngredients->Controls->Add(this->comboBoxNewRecipeIngredient);
			this->panelNewRecipeIngredients->Controls->Add(this->textBoxNewRecipeIngredient);
			this->panelNewRecipeIngredients->Location = System::Drawing::Point(17, 124);
			this->panelNewRecipeIngredients->Name = L"panelNewRecipeIngredients";
			this->panelNewRecipeIngredients->Size = System::Drawing::Size(235, 67);
			this->panelNewRecipeIngredients->TabIndex = 12;
			this->panelNewRecipeIngredients->Visible = false;
			// 
			// comboBoxNewRecipeIngredientUnit
			// 
			this->comboBoxNewRecipeIngredientUnit->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->comboBoxNewRecipeIngredientUnit->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxNewRecipeIngredientUnit->Font = (gcnew System::Drawing::Font(L"Calibri", 10));
			this->comboBoxNewRecipeIngredientUnit->FormattingEnabled = true;
			this->comboBoxNewRecipeIngredientUnit->Location = System::Drawing::Point(74, 36);
			this->comboBoxNewRecipeIngredientUnit->Name = L"comboBoxNewRecipeIngredientUnit";
			this->comboBoxNewRecipeIngredientUnit->Size = System::Drawing::Size(123, 23);
			this->comboBoxNewRecipeIngredientUnit->TabIndex = 11;
			this->comboBoxNewRecipeIngredientUnit->Tag = L"unit";
			// 
			// buttonNewRecipeIngredientAdd
			// 
			this->buttonNewRecipeIngredientAdd->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->buttonNewRecipeIngredientAdd->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->buttonNewRecipeIngredientAdd->Cursor = System::Windows::Forms::Cursors::Hand;
			this->buttonNewRecipeIngredientAdd->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonNewRecipeIngredientAdd->Location = System::Drawing::Point(203, 34);
			this->buttonNewRecipeIngredientAdd->Name = L"buttonNewRecipeIngredientAdd";
			this->buttonNewRecipeIngredientAdd->Size = System::Drawing::Size(27, 27);
			this->buttonNewRecipeIngredientAdd->TabIndex = 9;
			this->buttonNewRecipeIngredientAdd->Text = L"+";
			this->buttonNewRecipeIngredientAdd->UseVisualStyleBackColor = false;
			this->buttonNewRecipeIngredientAdd->Click += gcnew System::EventHandler(this, &MyForm::buttonNewRecipeIngredientAdd_Click);
			// 
			// labelNewRecipeIngredient
			// 
			this->labelNewRecipeIngredient->AutoSize = true;
			this->labelNewRecipeIngredient->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->labelNewRecipeIngredient->Location = System::Drawing::Point(52, 39);
			this->labelNewRecipeIngredient->Name = L"labelNewRecipeIngredient";
			this->labelNewRecipeIngredient->Size = System::Drawing::Size(16, 19);
			this->labelNewRecipeIngredient->TabIndex = 10;
			this->labelNewRecipeIngredient->Tag = L"unitLabel";
			this->labelNewRecipeIngredient->Text = L"x";
			// 
			// comboBoxNewRecipeIngredient
			// 
			this->comboBoxNewRecipeIngredient->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->comboBoxNewRecipeIngredient->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxNewRecipeIngredient->Font = (gcnew System::Drawing::Font(L"Calibri", 10));
			this->comboBoxNewRecipeIngredient->FormattingEnabled = true;
			this->comboBoxNewRecipeIngredient->Location = System::Drawing::Point(3, 7);
			this->comboBoxNewRecipeIngredient->Name = L"comboBoxNewRecipeIngredient";
			this->comboBoxNewRecipeIngredient->Size = System::Drawing::Size(227, 23);
			this->comboBoxNewRecipeIngredient->TabIndex = 3;
			this->comboBoxNewRecipeIngredient->Tag = L"title";
			// 
			// textBoxNewRecipeIngredient
			// 
			this->textBoxNewRecipeIngredient->AllowDrop = true;
			this->textBoxNewRecipeIngredient->Font = (gcnew System::Drawing::Font(L"Calibri", 10));
			this->textBoxNewRecipeIngredient->Location = System::Drawing::Point(3, 36);
			this->textBoxNewRecipeIngredient->Name = L"textBoxNewRecipeIngredient";
			this->textBoxNewRecipeIngredient->Size = System::Drawing::Size(43, 24);
			this->textBoxNewRecipeIngredient->TabIndex = 8;
			this->textBoxNewRecipeIngredient->Tag = L"ammount";
			this->textBoxNewRecipeIngredient->Text = L"1";
			this->textBoxNewRecipeIngredient->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// buttonNewRecipeSave
			// 
			this->buttonNewRecipeSave->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->buttonNewRecipeSave->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->buttonNewRecipeSave->Cursor = System::Windows::Forms::Cursors::Hand;
			this->buttonNewRecipeSave->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->buttonNewRecipeSave->Location = System::Drawing::Point(598, 42);
			this->buttonNewRecipeSave->Name = L"buttonNewRecipeSave";
			this->buttonNewRecipeSave->Size = System::Drawing::Size(66, 27);
			this->buttonNewRecipeSave->TabIndex = 11;
			this->buttonNewRecipeSave->Text = L"Zapisz";
			this->buttonNewRecipeSave->UseVisualStyleBackColor = false;
			this->buttonNewRecipeSave->Click += gcnew System::EventHandler(this, &MyForm::buttonNewRecipeSave_Click);
			// 
			// labelNewRecipeTitle
			// 
			this->labelNewRecipeTitle->AutoSize = true;
			this->labelNewRecipeTitle->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->labelNewRecipeTitle->Location = System::Drawing::Point(10, 44);
			this->labelNewRecipeTitle->Name = L"labelNewRecipeTitle";
			this->labelNewRecipeTitle->Size = System::Drawing::Size(47, 20);
			this->labelNewRecipeTitle->TabIndex = 2;
			this->labelNewRecipeTitle->Text = L"Tytu�:";
			// 
			// textBoxNewRecipeTitle
			// 
			this->textBoxNewRecipeTitle->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxNewRecipeTitle->Font = (gcnew System::Drawing::Font(L"Calibri", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->textBoxNewRecipeTitle->Location = System::Drawing::Point(65, 40);
			this->textBoxNewRecipeTitle->Name = L"textBoxNewRecipeTitle";
			this->textBoxNewRecipeTitle->Size = System::Drawing::Size(527, 31);
			this->textBoxNewRecipeTitle->TabIndex = 1;
			// 
			// tabPageRecipe
			// 
			this->tabPageRecipe->AutoScroll = true;
			this->tabPageRecipe->BackColor = System::Drawing::Color::AliceBlue;
			this->tabPageRecipe->Controls->Add(this->richTextBoxRecipeSource);
			this->tabPageRecipe->Controls->Add(this->labelRecipeIngredientSource);
			this->tabPageRecipe->Controls->Add(this->richTextBoxRecipeDescription);
			this->tabPageRecipe->Controls->Add(this->panelRecipeIngredient);
			this->tabPageRecipe->Controls->Add(this->labelRecipeDescription);
			this->tabPageRecipe->Controls->Add(this->labelRecipeIngredients);
			this->tabPageRecipe->Controls->Add(this->labelRecipeTitle);
			this->tabPageRecipe->Location = System::Drawing::Point(4, 5);
			this->tabPageRecipe->Name = L"tabPageRecipe";
			this->tabPageRecipe->Padding = System::Windows::Forms::Padding(3);
			this->tabPageRecipe->Size = System::Drawing::Size(672, 602);
			this->tabPageRecipe->TabIndex = 3;
			this->tabPageRecipe->Text = L"Przepis";
			// 
			// richTextBoxRecipeSource
			// 
			this->richTextBoxRecipeSource->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->richTextBoxRecipeSource->BackColor = System::Drawing::Color::MintCream;
			this->richTextBoxRecipeSource->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->richTextBoxRecipeSource->Cursor = System::Windows::Forms::Cursors::IBeam;
			this->richTextBoxRecipeSource->Font = (gcnew System::Drawing::Font(L"Calibri", 10));
			this->richTextBoxRecipeSource->Location = System::Drawing::Point(332, 534);
			this->richTextBoxRecipeSource->Multiline = false;
			this->richTextBoxRecipeSource->Name = L"richTextBoxRecipeSource";
			this->richTextBoxRecipeSource->ReadOnly = true;
			this->richTextBoxRecipeSource->Size = System::Drawing::Size(266, 17);
			this->richTextBoxRecipeSource->TabIndex = 31;
			this->richTextBoxRecipeSource->Text = L"";
			// 
			// labelRecipeIngredientSource
			// 
			this->labelRecipeIngredientSource->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->labelRecipeIngredientSource->AutoSize = true;
			this->labelRecipeIngredientSource->Location = System::Drawing::Point(284, 535);
			this->labelRecipeIngredientSource->Name = L"labelRecipeIngredientSource";
			this->labelRecipeIngredientSource->Size = System::Drawing::Size(42, 13);
			this->labelRecipeIngredientSource->TabIndex = 30;
			this->labelRecipeIngredientSource->Text = L"�r�d�o:";
			// 
			// richTextBoxRecipeDescription
			// 
			this->richTextBoxRecipeDescription->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->richTextBoxRecipeDescription->BackColor = System::Drawing::Color::MintCream;
			this->richTextBoxRecipeDescription->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->richTextBoxRecipeDescription->Cursor = System::Windows::Forms::Cursors::IBeam;
			this->richTextBoxRecipeDescription->Font = (gcnew System::Drawing::Font(L"Calibri", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->richTextBoxRecipeDescription->Location = System::Drawing::Point(287, 112);
			this->richTextBoxRecipeDescription->Name = L"richTextBoxRecipeDescription";
			this->richTextBoxRecipeDescription->ReadOnly = true;
			this->richTextBoxRecipeDescription->Size = System::Drawing::Size(311, 413);
			this->richTextBoxRecipeDescription->TabIndex = 29;
			this->richTextBoxRecipeDescription->Text = L"";
			// 
			// panelRecipeIngredient
			// 
			this->panelRecipeIngredient->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panelRecipeIngredient->Controls->Add(this->labelRecipeIngredientName);
			this->panelRecipeIngredient->Controls->Add(this->labelRecipeIngredientUnit);
			this->panelRecipeIngredient->Controls->Add(this->labelRecipeIngredientAmmmount);
			this->panelRecipeIngredient->Controls->Add(this->labelRecipeIngredientSeparator);
			this->panelRecipeIngredient->Location = System::Drawing::Point(28, 112);
			this->panelRecipeIngredient->Name = L"panelRecipeIngredient";
			this->panelRecipeIngredient->Size = System::Drawing::Size(235, 67);
			this->panelRecipeIngredient->TabIndex = 28;
			this->panelRecipeIngredient->Visible = false;
			// 
			// labelRecipeIngredientName
			// 
			this->labelRecipeIngredientName->BackColor = System::Drawing::Color::MintCream;
			this->labelRecipeIngredientName->Font = (gcnew System::Drawing::Font(L"Calibri", 14));
			this->labelRecipeIngredientName->Location = System::Drawing::Point(3, 6);
			this->labelRecipeIngredientName->Name = L"labelRecipeIngredientName";
			this->labelRecipeIngredientName->Size = System::Drawing::Size(227, 19);
			this->labelRecipeIngredientName->TabIndex = 25;
			this->labelRecipeIngredientName->Tag = L"name";
			this->labelRecipeIngredientName->Text = L"Name";
			this->labelRecipeIngredientName->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelRecipeIngredientUnit
			// 
			this->labelRecipeIngredientUnit->BackColor = System::Drawing::Color::MintCream;
			this->labelRecipeIngredientUnit->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->labelRecipeIngredientUnit->Location = System::Drawing::Point(104, 40);
			this->labelRecipeIngredientUnit->Name = L"labelRecipeIngredientUnit";
			this->labelRecipeIngredientUnit->Size = System::Drawing::Size(126, 19);
			this->labelRecipeIngredientUnit->TabIndex = 25;
			this->labelRecipeIngredientUnit->Tag = L"unit";
			this->labelRecipeIngredientUnit->Text = L"Unit";
			this->labelRecipeIngredientUnit->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelRecipeIngredientAmmmount
			// 
			this->labelRecipeIngredientAmmmount->BackColor = System::Drawing::Color::MintCream;
			this->labelRecipeIngredientAmmmount->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->labelRecipeIngredientAmmmount->Location = System::Drawing::Point(3, 40);
			this->labelRecipeIngredientAmmmount->Name = L"labelRecipeIngredientAmmmount";
			this->labelRecipeIngredientAmmmount->Size = System::Drawing::Size(73, 19);
			this->labelRecipeIngredientAmmmount->TabIndex = 24;
			this->labelRecipeIngredientAmmmount->Tag = L"ammount";
			this->labelRecipeIngredientAmmmount->Text = L"12";
			this->labelRecipeIngredientAmmmount->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelRecipeIngredientSeparator
			// 
			this->labelRecipeIngredientSeparator->AutoSize = true;
			this->labelRecipeIngredientSeparator->Font = (gcnew System::Drawing::Font(L"Calibri", 12));
			this->labelRecipeIngredientSeparator->Location = System::Drawing::Point(82, 40);
			this->labelRecipeIngredientSeparator->Name = L"labelRecipeIngredientSeparator";
			this->labelRecipeIngredientSeparator->Size = System::Drawing::Size(16, 19);
			this->labelRecipeIngredientSeparator->TabIndex = 10;
			this->labelRecipeIngredientSeparator->Tag = L"unitLabel";
			this->labelRecipeIngredientSeparator->Text = L"x";
			// 
			// labelRecipeDescription
			// 
			this->labelRecipeDescription->AutoSize = true;
			this->labelRecipeDescription->Location = System::Drawing::Point(284, 96);
			this->labelRecipeDescription->Name = L"labelRecipeDescription";
			this->labelRecipeDescription->Size = System::Drawing::Size(44, 13);
			this->labelRecipeDescription->TabIndex = 27;
			this->labelRecipeDescription->Text = L"Przepis:";
			// 
			// labelRecipeIngredients
			// 
			this->labelRecipeIngredients->AutoSize = true;
			this->labelRecipeIngredients->Location = System::Drawing::Point(29, 96);
			this->labelRecipeIngredients->Name = L"labelRecipeIngredients";
			this->labelRecipeIngredients->Size = System::Drawing::Size(55, 13);
			this->labelRecipeIngredients->TabIndex = 26;
			this->labelRecipeIngredients->Text = L"Sk�adniki:";
			// 
			// labelRecipeTitle
			// 
			this->labelRecipeTitle->AutoSize = true;
			this->labelRecipeTitle->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 30));
			this->labelRecipeTitle->Location = System::Drawing::Point(109, 23);
			this->labelRecipeTitle->Name = L"labelRecipeTitle";
			this->labelRecipeTitle->Size = System::Drawing::Size(106, 46);
			this->labelRecipeTitle->TabIndex = 25;
			this->labelRecipeTitle->Text = L"Tytu�";
			// 
			// tabPageIngredients
			// 
			this->tabPageIngredients->AutoScroll = true;
			this->tabPageIngredients->BackColor = System::Drawing::Color::AliceBlue;
			this->tabPageIngredients->Controls->Add(this->panelIngredients);
			this->tabPageIngredients->Controls->Add(this->labelIngredientsPriceOwned);
			this->tabPageIngredients->Controls->Add(this->labelIngredientsNameOwned);
			this->tabPageIngredients->Controls->Add(this->buttonIngredientsAdd);
			this->tabPageIngredients->Controls->Add(this->textBoxIngredientsPrice);
			this->tabPageIngredients->Controls->Add(this->labelIngredientsPrice);
			this->tabPageIngredients->Controls->Add(this->textBoxIngredientsName);
			this->tabPageIngredients->Controls->Add(this->labelIngredietnsName);
			this->tabPageIngredients->Controls->Add(this->labelIngredients);
			this->tabPageIngredients->Location = System::Drawing::Point(4, 5);
			this->tabPageIngredients->Name = L"tabPageIngredients";
			this->tabPageIngredients->Padding = System::Windows::Forms::Padding(3);
			this->tabPageIngredients->Size = System::Drawing::Size(672, 602);
			this->tabPageIngredients->TabIndex = 4;
			this->tabPageIngredients->Text = L"Sk�adniki";
			// 
			// panelIngredients
			// 
			this->panelIngredients->Controls->Add(this->textBoxIngredientsPriceAdded);
			this->panelIngredients->Controls->Add(this->textBoxIngredientsNameAdded);
			this->panelIngredients->Location = System::Drawing::Point(80, 208);
			this->panelIngredients->Name = L"panelIngredients";
			this->panelIngredients->Size = System::Drawing::Size(491, 26);
			this->panelIngredients->TabIndex = 32;
			this->panelIngredients->Visible = false;
			// 
			// textBoxIngredientsPriceAdded
			// 
			this->textBoxIngredientsPriceAdded->Location = System::Drawing::Point(348, 3);
			this->textBoxIngredientsPriceAdded->Name = L"textBoxIngredientsPriceAdded";
			this->textBoxIngredientsPriceAdded->Size = System::Drawing::Size(138, 20);
			this->textBoxIngredientsPriceAdded->TabIndex = 25;
			this->textBoxIngredientsPriceAdded->Tag = L"Price";
			this->textBoxIngredientsPriceAdded->TextChanged += gcnew System::EventHandler(this, &MyForm::textBoxIngredientsPriceAdded_TextChanged);
			// 
			// textBoxIngredientsNameAdded
			// 
			this->textBoxIngredientsNameAdded->Enabled = false;
			this->textBoxIngredientsNameAdded->Location = System::Drawing::Point(3, 3);
			this->textBoxIngredientsNameAdded->Name = L"textBoxIngredientsNameAdded";
			this->textBoxIngredientsNameAdded->Size = System::Drawing::Size(339, 20);
			this->textBoxIngredientsNameAdded->TabIndex = 24;
			this->textBoxIngredientsNameAdded->Tag = L"Name";
			// 
			// labelIngredientsPriceOwned
			// 
			this->labelIngredientsPriceOwned->AutoSize = true;
			this->labelIngredientsPriceOwned->Location = System::Drawing::Point(497, 180);
			this->labelIngredientsPriceOwned->Name = L"labelIngredientsPriceOwned";
			this->labelIngredientsPriceOwned->Size = System::Drawing::Size(35, 13);
			this->labelIngredientsPriceOwned->TabIndex = 30;
			this->labelIngredientsPriceOwned->Text = L"Cena:";
			// 
			// labelIngredientsNameOwned
			// 
			this->labelIngredientsNameOwned->AutoSize = true;
			this->labelIngredientsNameOwned->Location = System::Drawing::Point(166, 180);
			this->labelIngredientsNameOwned->Name = L"labelIngredientsNameOwned";
			this->labelIngredientsNameOwned->Size = System::Drawing::Size(43, 13);
			this->labelIngredientsNameOwned->TabIndex = 31;
			this->labelIngredientsNameOwned->Text = L"Nazwa:";
			// 
			// buttonIngredientsAdd
			// 
			this->buttonIngredientsAdd->Location = System::Drawing::Point(435, 118);
			this->buttonIngredientsAdd->Name = L"buttonIngredientsAdd";
			this->buttonIngredientsAdd->Size = System::Drawing::Size(97, 23);
			this->buttonIngredientsAdd->TabIndex = 29;
			this->buttonIngredientsAdd->Text = L"Dodaj";
			this->buttonIngredientsAdd->UseVisualStyleBackColor = true;
			this->buttonIngredientsAdd->Click += gcnew System::EventHandler(this, &MyForm::buttonIngredientsAdd_Click);
			// 
			// textBoxIngredientsPrice
			// 
			this->textBoxIngredientsPrice->Location = System::Drawing::Point(189, 121);
			this->textBoxIngredientsPrice->Name = L"textBoxIngredientsPrice";
			this->textBoxIngredientsPrice->Size = System::Drawing::Size(220, 20);
			this->textBoxIngredientsPrice->TabIndex = 28;
			// 
			// labelIngredientsPrice
			// 
			this->labelIngredientsPrice->AutoSize = true;
			this->labelIngredientsPrice->Location = System::Drawing::Point(138, 123);
			this->labelIngredientsPrice->Name = L"labelIngredientsPrice";
			this->labelIngredientsPrice->Size = System::Drawing::Size(35, 13);
			this->labelIngredientsPrice->TabIndex = 27;
			this->labelIngredientsPrice->Text = L"Cena:";
			// 
			// textBoxIngredientsName
			// 
			this->textBoxIngredientsName->Location = System::Drawing::Point(189, 96);
			this->textBoxIngredientsName->Name = L"textBoxIngredientsName";
			this->textBoxIngredientsName->Size = System::Drawing::Size(220, 20);
			this->textBoxIngredientsName->TabIndex = 26;
			// 
			// labelIngredietnsName
			// 
			this->labelIngredietnsName->AutoSize = true;
			this->labelIngredietnsName->Location = System::Drawing::Point(130, 96);
			this->labelIngredietnsName->Name = L"labelIngredietnsName";
			this->labelIngredietnsName->Size = System::Drawing::Size(43, 13);
			this->labelIngredietnsName->TabIndex = 25;
			this->labelIngredietnsName->Text = L"Nazwa:";
			// 
			// labelIngredients
			// 
			this->labelIngredients->AutoSize = true;
			this->labelIngredients->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 40));
			this->labelIngredients->Location = System::Drawing::Point(223, 19);
			this->labelIngredients->Name = L"labelIngredients";
			this->labelIngredients->Size = System::Drawing::Size(246, 63);
			this->labelIngredients->TabIndex = 24;
			this->labelIngredients->Text = L"Sk�adniki";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(884, 611);
			this->Controls->Add(this->splitContainer1);
			this->MinimumSize = System::Drawing::Size(900, 650);
			this->Name = L"MyForm";
			this->Text = L"Studencki Pomocnik Kulinarny";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->EndInit();
			this->splitContainer1->ResumeLayout(false);
			this->tabControlMain->ResumeLayout(false);
			this->tabPageFridge->ResumeLayout(false);
			this->tabPageFridge->PerformLayout();
			this->panelOwnedIngredientAdd->ResumeLayout(false);
			this->panelOwnedIngredientAdd->PerformLayout();
			this->panelOwnedIngredient->ResumeLayout(false);
			this->panelOwnedIngredient->PerformLayout();
			this->tabPageRecipes->ResumeLayout(false);
			this->tabPageRecipes->PerformLayout();
			this->panelRecipeSearchResult->ResumeLayout(false);
			this->tabPageNewRecipe->ResumeLayout(false);
			this->tabPageNewRecipe->PerformLayout();
			this->panelNewRecipeIngredients->ResumeLayout(false);
			this->panelNewRecipeIngredients->PerformLayout();
			this->tabPageRecipe->ResumeLayout(false);
			this->tabPageRecipe->PerformLayout();
			this->panelRecipeIngredient->ResumeLayout(false);
			this->panelRecipeIngredient->PerformLayout();
			this->tabPageIngredients->ResumeLayout(false);
			this->tabPageIngredients->PerformLayout();
			this->panelIngredients->ResumeLayout(false);
			this->panelIngredients->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
		
	private: Void LoadIngredientsToFridge();
	private: Ingredient^ AddNewIngredient(String^ Name);
			 
	private: Void InitializeNewRecipe();
	private: Panel^ CreateNewRecipeIngredientPanel();
	private: Recipe ^ AddNewRecipe();

	private: Void LoadRecipeToRecipe(Recipe ^ recipe);

	private: Void LoadRecipesToRecipes();

	private: Void LoadIngredients();

	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonFridge_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonRecipes_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonIngredients_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonRecipesAdd_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonIngredientsAdd_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonOwnedIngredientAdd_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonNewRecipeIngredientAdd_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonNewRecipeIngredientRemove_Click(System::Object ^ sender, System::EventArgs ^ e);
	private: System::Void buttonNewRecipeSave_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void panelRecipeSearchResult_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonOwnedIngredientDelete_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxOwnedIngredientUnitAmmount_TextChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxIngredientsPriceAdded_TextChanged(System::Object^  sender, System::EventArgs^  e);
};

}
