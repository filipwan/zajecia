#pragma once
#include "Ingredient.h"
#include "Unit.h"

namespace SPK {

	public ref class IngredientInRecipe
	{
	public:

		Ingredient^ Ingredient;
		double Ammount;
		Unit^ Unit;


	public:
		IngredientInRecipe();
	};

}
