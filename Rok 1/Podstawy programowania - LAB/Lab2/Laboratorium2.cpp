/*
Autor:  Filip Wanat
Grupa:  WT/P 7:30     (Wtorek parzysty godz 7:30)
Temat:  Laboratorium nr 2
Data:   17 pazdziernika 2017 r.
*/

#include <iostream>
#include <iomanip> 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>


using namespace std;

int main()
{
	cout << "Autor: Filip Wanat (WT/P 7:30) \n\n" ;
	srand(time(NULL));
	
 //Zadanie 1
	double a, b, c, delta;
	
    cout << "Podaj a, b i c dla wzoru ax^2 + bx + c: ";
    cin >> a >> b >> c;
    
    
    if(a == 0)
    	cout << "\n to nie funkcja kwadratowa\n";
    else
    {
		delta = (b*b) - (4*a*c);
		if(delta < 0)
			cout << "\nBrak pierwiastkow rzeczywistych\n";
		else if(delta == 0)
			cout << "Pierwiastkiem rownania jest x1 = x2 = " << (-b) / (2 * a) << endl;
		else
		{
			cout << "Pierwiastkami rownania sa x1 = " << ((-b - sqrt(delta)) / (2 * a)) 
				 << " i x2 = " << ((-b + sqrt(delta)) / (2 * a)) << endl;
		}
	}
	
	system("PAUSE");
    
    
 //Zadanie 2
 	int dzien, mies, rok;
 	cout << "\n\nPodaj dzien, miesiac i rok: ";
 	cin >> dzien >> mies >> rok;
 	if((mies > 12) || (mies < 1) || (dzien <= 0) || (dzien > 31)) cout << "\nPodana data jest niepoprawna\n";
 	else if((mies == 1 || mies == 3 || mies == 5 || mies == 7 || mies == 8 || mies == 10 || mies == 12) && (dzien > 0) && (dzien <= 31))
		cout << "\nPodana data jest poprawna\n";
 	else if((mies == 4 || mies == 6 || mies == 9 || mies == 11) && (dzien > 0) && (dzien <= 30) )
		cout << "\nPodana data jest poprawna\n";
 	else if(mies == 2)
 		if((rok%4 == 0) && !((rok % 100 == 0) && (rok % 400)) && (dzien <= 29))
			cout  << "\nPodana data jest poprawna\n";
 		else if(dzien <= 28)
			cout  << "\nPodana data jest poprawna\n";
 	else cout << "\nPodana data jest niepoprawna\n";
 	
 	system("PAUSE");
 
 //Zadanie 3 
	double rmin, rmax, wier;
	
	cout << "\n\nPodaj minimalny promien: ";
	cin >> rmin;
	cout << "Podaj maksymalny promien: ";
	cin >> rmax;
	cout << "Podaj ilosc wierszy: ";
	cin >> wier;
	
	cout << endl << endl
	 	 << "==========================================\n"
 		 << "| Lp | promien | obwod kola | pole kola  |\n"
 		 << "==========================================\n";
 	if(rmin <= rmax)	 
	for(int i = 0; i < wier; i++)
	{
		double r = rmin + (i*((rmax-rmin)/(wier-1)));
		cout << "| ";
		cout << setw(2) << right << i+1 << " | ";
		cout.setf( ios::showpoint );
		cout.precision( 2 );
		cout << fixed << setw(7) << right <<  r << " | ";
		cout << fixed << setw(10) << right << 2 * M_PI * r << " | ";
		cout << fixed << setw(10) << right << M_PI * r * r << " |\n";
		cout.unsetf( ios::showpoint );
	}
	cout << "==========================================\n";
		
	system("PAUSE");
	
 //Zadanie 4
	int n0;
	cout << "\n\nPodaj dodatnia liczbe calkowita n0: ";
	cin >> n0;
	
	if(n0>0)
	for(int i = 0; n0 != 1; i++)
	{
		if(n0 % 2 == 0)
		{
			cout << i + 1 << ", " << n0 << ", parzyste, ";
			n0 /= 2;
			cout << n0 << endl;
		}else
		{
			cout << i + 1 << ", " << n0 << ", nieparzyste, ";
			n0 = 3 * n0 + 1;
			cout << n0 << endl;
		}
		
	}
    
    system("PAUSE");
	
 //Zadanie 5 a)
	int mina, maxa, suma = 0, najmna, najwa;
	cout << "\n\nPodaj liczbe min: ";
	cin >> mina;
	cout << "Podaj liczbe max: ";
	cin >> maxa;
	
	najmna = maxa;
	najwa = mina;
	
	if(mina<maxa)
	for(int i = 0, wyl; i < 20; i++)
	{
		wyl = (rand() % (maxa-mina+1)) + mina;
		if(wyl > najwa) najwa = wyl;
		if(wyl < najmna) najmna = wyl;
		suma += wyl;
		cout << "\n" << wyl;
	}
	cout << "\nNajmniejsza wylosowana to: " << najmna << "\nNajwieksza wylosowana to: "  << najwa << "\nSrednia wynosi: " << (float)suma / 20 << endl;

 //Zadanie 5 b)
	double minb, maxb, sumb = 0, najmnb, najwb, wylb;
	cout << "\n\nPodaj liczbe min: ";
	cin >> minb;
	cout << "Podaj liczbe max: ";
	cin >> maxb;
	
	najmnb = maxb;
	najwb = minb;
	
	if(minb<maxb)
	for(int i = 0; i < 20; i++)
	{
		wylb = minb + (maxb - minb) * rand() / ((double)RAND_MAX);
		if(wylb > najwb) najwb = wylb;
		if(wylb < najmnb) najmnb = wylb;
		sumb += wylb;
		cout << "\n" << wylb;
	}
	cout << "\nNajmniejsza wylosowana to: " << najmnb << "\nNajwieksza wylosowana to: "  << najwb << "\nSrednia wynosi: " << (float)sumb / 20 << endl;
	
	
	
	return 0;
}



