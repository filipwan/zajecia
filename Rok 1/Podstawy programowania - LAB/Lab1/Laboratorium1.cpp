/*
Autor:  Filip Wanat
Grupa:  WT/P 7:30     (Wtorek parzysty godz 7:30)
Temat:  Laboratorium nr 1.
Data:   3 pazdziernika 2017 r.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


using namespace std;

int main()
{
	cout << "Autor: Filip Wanat (WP 7:30) \n\n" ;
	
 //Zadanie 1
   // a)
	
	cout << "____________________________________\n"
		 << "|                                  |\n"
		 << "|           Filip Wanat            |\n"
		 << "|                                  |\n"
		 << "|        ul. Minkowskiego 3        |\n"
		 << "|             Wroclaw              |\n"
		 << "|                                  |\n"
		 << "|       tel.: 664 610 423          |\n"
		 << "|     e-mail: fwan24@gmail.com     |\n"
		 << "|__________________________________|\n\n\n";
		 
	system("PAUSE");
	
   //b)
	
	printf("\n\n\n____________________________________\n");
	printf("|                                  |\n");
	printf("|          Janusz Nielot           |\n");
	printf("|                                  |\n");
	printf("|          ul. Wiejska 5           |\n");
	printf("|             Wroclaw              |\n");
	printf("|                                  |\n");
	printf("|       tel.: 404 404 404          |\n");
	printf("|     e-mail: jnielot@gmail.com    |\n");
	printf("|__________________________________|\n\n\n");
	
	system("PAUSE");
	
 //Zadanie 2
   //Wariant iostream
	
	int a, b, c;
	cout << "\n\n\nPodaj 3 zmienne: ";
	cin >> a >> b >> c;
	float srednia = (float)(a+b+c)/3;
	cout << "Suma: " << a + b + c
		 << "\nIloczyn: " << a * b * c
		 << "\nSrednia arytmetyczna: " << srednia << "\n\n\n";
	
	system("PAUSE");
	
	//Wariant stdio.h
	
	printf("\n\n\nPodaj 3 zmienne: ");
	scanf("%d", &a);
	scanf("%d", &b);
	scanf("%d", &c);
	int suma_abc = a + b + c;
	int iloczyn = a * b * c;
	srednia = (float)suma_abc / 3;
	printf("\nSuma: %d \nIloczyn: %d \nSrednia arytmetyczna: %f\n\n", suma_abc, iloczyn, srednia);
	
	system("PAUSE");
	
 //Zadanie 3
   //Wariant stdio.h
	
	float r, l, p;
	printf("\n\n\nPodaj promien kola: ");
	scanf("%f", &r);
	l = 2 * M_PI * r;
	p = M_PI * r * r;
	printf("\nObwod kola o podanym promieniu wynosi %f \n", l);
	printf("Pole kola o podanym promieniu wynosi %f \n\n", p);
	
	system("PAUSE");
	
   //Wariant iostream
	cout << "\n\n\nPodaj promien kola: ";
	cin >> r;
	cout << "\nObwod kola o podanym promieniu wynosi " << 2 * M_PI * r
		 << "\nPole kola o podanym promieniu wynosi " << M_PI * r * r << endl;
	
	
	system("PAUSE");
	
	return 0;
}



